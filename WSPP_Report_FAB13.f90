!  WSPP_Report.f90 
!
!  FUNCTIONS:
!  WSPP_Report - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: WSPP_Report
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************
MODULE PARAM
    INTEGER, PARAMETER ::   StartYr = 2013
    INTEGER, PARAMETER ::   EndYr = 2041
    INTEGER, PARAMETER ::   rozones = 37
    INTEGER, PARAMETER ::   cozones = 5
    INTEGER, PARAMETER ::   ncells = 49725
    
    DATA INDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\'/
    DATA OUTDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\Report\'/
    DATA RAWDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\'/
    DATA CntyID /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\CountID.txt'/
    DATA CntyCell /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\Cell_county.txt'/
    DATA WBPDIR /'W:\CNEB\CNEB\Gridded_CropSim\Run005\Full_Grid\'/
    
    CHARACTER*100    OUTDIR, RAWDIR, INDIR, WBPDIR
    CHARACTER*25    County, nCounty(0 : 93)
    CHARACTER*100    CntyID, CntyCell
    CHARACTER*12    CROPS(12)
    CHARACTER*3     Irrig(4)
    CHARACTER*4     YRT, cYRT
    
    INTEGER     Cell
    INTEGER     Yr, iyr, cyr(startyr : endyr)
    INTEGER     Mon
    INTEGER     crop
    INTEGER     pflag
    INTEGER     CoefZ
    INTEGER     CoefZone(ncells)
    INTEGER     ROZ
    INTEGER     Cntyflag(0 : 93)
    INTEGER     CntNum
    INTEGER     CellCnty(ncells)
    INTEGER     h, i, j, k, l, m
    INTEGER     iyear
    INTEGER     ROZONE(ncells)
    INTEGER     Zone
    
    REAL    acs
    REAL    AP
    REAL    AppSW
    REAL    DAP
    REAL    DET
    REAL    DP
    REAL    DP1
    REAL    DP2
    REAL    ET
    REAL    ETb
    REAL    ETG
    REAL    EWAT
    REAL    PSL
    REAL    RO
    REAL    RO1
    REAL    RO2
    REAL    SL
    REAL    SF
    REAL    RO2DP
    REAL    RO2ET
    REAL    LossFactor
    REAL    LPM(0 : rozones)
    REAL    MiToGauge(ncells)
    REAL    Precip(ncells, 0 : 12)
    REAL    ETtrans
    REAL    NIRmET
    
    ! Regional Totals
    DOUBLE PRECISION    RegionPrecip(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionAc(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionAP(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionSW(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionET(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionRO(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionDP(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionSL(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionPSL(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionDAP(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionETG(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionDET(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionETB(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionDP1(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionDP2(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionRO1(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionRO2(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionEWAT(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionSF(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionRO2DP(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionRO2ET(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionETtrans(StartYr : EndYr, 0 : 12)
    DOUBLE PRECISION    RegionNIRmET(StartYr : EndYr, 0 : 12)

    ! Regional Irrigation Totals
    DOUBLE PRECISION    RegIrrPrecip(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrAc(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrAP(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrSW(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrET(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrRO(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrDP(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrSL(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrPSL(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrDAP(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrETG(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrDET(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrETB(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrDP1(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrDP2(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrRO1(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrRO2(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrEWAT(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrSF(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrRO2DP(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrRO2ET(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrETtrans(StartYr : EndYr, 0 : 12, 4)
    DOUBLE PRECISION    RegIrrNIRmET(StartYr : EndYr, 0 : 12, 4)
    
    ! Regional Crop Totals
    DOUBLE PRECISION    RegCropPrecip(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropAc(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropAP(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropSW(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropET(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropRO(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropDP(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropSL(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropPSL(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropDAP(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropETG(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropDET(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropETB(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropDP1(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropDP2(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropRO1(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropRO2(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropEWAT(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropSF(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropRO2DP(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropRO2ET(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropETtrans(StartYr : EndYr, 0 : 12, 12)
    DOUBLE PRECISION    RegCropNIRmET(StartYr : EndYr, 0 : 12, 12)
    
    !Regional crop and irrigation Totals
    DOUBLE PRECISION    RICPrecip(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICAc(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICAP(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICSW(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICET(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICRO(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICDP(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICSL(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICPSL(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICDAP(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICETG(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICDET(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICETB(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICDP1(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICDP2(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICRO1(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICRO2(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICEWAT(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICSF(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICRO2DP(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICRO2ET(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICETtrans(StartYr : EndYr, 0 : 12, 4, 12)
    DOUBLE PRECISION    RICNIRmET(StartYr : EndYr, 0 : 12, 4, 12)
     
    ! County Totals
    DOUBLE PRECISION    cntyPrecip(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyAc(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyAP(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntySW(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyET(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyRO(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyDP(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntySL(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyPSL(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyDAP(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyETG(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyDET(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyETB(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyDP1(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyDP2(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyRO1(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyRO2(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyEWAT(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntySF(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyRO2DP(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyRO2ET(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyETtrans(StartYr : EndYr, 0 : 12, 0 : 93)
    DOUBLE PRECISION    cntyNIRmET(StartYr : EndYr, 0 : 12, 0 : 93)
    
    ! County Irrigation Totals
    DOUBLE PRECISION    cntyirrPrecip(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrAc(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrAP(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrSW(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrET(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrRO(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrDP(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrSL(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrPSL(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrDAP(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrETG(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrDET(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrETB(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrDP1(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrDP2(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrRO1(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrRO2(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrEWAT(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrSF(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrRO2DP(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrRO2ET(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrETtrans(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    DOUBLE PRECISION    cntyirrNIRmET(StartYr : EndYr, 0 : 12, 0 : 93, 4)
    
    ! County Crop Totals
    DOUBLE PRECISION    cntycrpPrecip(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpAc(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpAP(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpSW(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpET(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpRO(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpDP(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpSL(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpPSL(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpDAP(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpETG(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpDET(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpETB(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpDP1(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpDP2(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpRO1(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpRO2(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpEWAT(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpSF(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpRO2DP(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpRO2ET(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpETtrans(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    DOUBLE PRECISION    cntycrpNIRmET(StartYr : EndYr, 0 : 12, 0 : 93, 12)
    
    ! County Irrigation and Crop Totals
    DOUBLE PRECISION    CICPrecip(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICAc(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICAP(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICSW(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICET(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICRO(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICDP(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICSL(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICPSL(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICDAP(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICETG(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICDET(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICETB(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICDP1(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICDP2(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICRO1(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICRO2(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICEWAT(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICSF(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICRO2DP(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICRO2ET(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICETtrans(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    DOUBLE PRECISION    CICNIRmET(StartYr : EndYr, 0 : 12, 0 : 93, 4, 12)
    
    !Runoff Zone totals
    DOUBLE PRECISION    ROZPrecip(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZAc(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZAP(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZSW(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZET(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZRO(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZDP(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZSL(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZPSL(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZDAP(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZETG(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZDET(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZETB(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZDP1(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZDP2(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZRO1(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZRO2(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZEWAT(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZSF(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZRO2DP(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZRO2ET(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZETtrans(StartYr : EndYr, 0 : 12, 0 : rozones)
    DOUBLE PRECISION    ROZNIRmET(StartYr : EndYr, 0 : 12, 0 : rozones)
    
    !Runoff Zone Irrigation totals
    DOUBLE PRECISION    ROZirrPrecip(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrAc(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrAP(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrSW(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrET(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrRO(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrDP(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrSL(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrPSL(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrDAP(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrETG(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrDET(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrETB(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrDP1(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrDP2(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrRO1(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrRO2(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrEWAT(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrSF(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrRO2DP(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrRO2ET(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrETtrans(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    DOUBLE PRECISION    ROZirrNIRmET(StartYr : EndYr, 0 : 12, 0 : rozones, 4)
    
    !Runoff Zone Crop Totals
    DOUBLE PRECISION    ROZcrpPrecip(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpAc(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpAP(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpSW(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpET(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpRO(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpDP(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpSL(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpPSL(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpDAP(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpETG(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpDET(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpETB(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpDP1(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpDP2(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpRO1(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpRO2(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpEWAT(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpSF(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpRO2DP(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpRO2ET(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpETtrans(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    DOUBLE PRECISION    ROZcrpNIRmET(StartYr : EndYr, 0 : 12, 0 : rozones, 12)
    
    !Runoff Zone Irrigation and crop Totals
    DOUBLE PRECISION    ROZiCPrecip(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCAc(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCAP(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCSW(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCET(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCRO(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCDP(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCSL(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCPSL(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCDAP(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCETG(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCDET(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCETB(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCDP1(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCDP2(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCRO1(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCRO2(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCEWAT(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCSF(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCRO2DP(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCRO2ET(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCETtrans(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    DOUBLE PRECISION    ROZiCNIRmET(StartYr : EndYr, 0 : 12, 0 : rozones, 4, 12)
    
    !Coefficient Zone Totals
    DOUBLE PRECISION    CoEZPrecip(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZAc(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZAP(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZSW(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZET(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZRO(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZDP(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZSL(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZPSL(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZDAP(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZETG(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZDET(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZETB(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZDP1(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZDP2(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZRO1(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZRO2(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZEWAT(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZSF(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZRO2DP(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZRO2ET(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZETtrans(StartYr : EndYr, 0 : 12, 0 : cozones)
    DOUBLE PRECISION    CoEZNIRmET(StartYr : EndYr, 0 : 12, 0 : cozones)
    
    !Coefficient Zone Irrgation Totals
    DOUBLE PRECISION    CoEZirrPrecip(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrAc(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrAP(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrSW(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrET(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrRO(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrDP(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrSL(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrPSL(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrDAP(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrETG(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrDET(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrETB(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrDP1(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrDP2(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrRO1(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrRO2(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrEWAT(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrSF(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrRO2DP(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrRO2ET(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrETtrans(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    DOUBLE PRECISION    CoEZirrNIRmET(StartYr : EndYr, 0 : 12, 0 : cozones, 4)
    
    !Coefficient Zone Crop Totals
    DOUBLE PRECISION    CoEZcrpPrecip(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpAc(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpAP(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpSW(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpET(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpRO(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpDP(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpSL(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpPSL(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpDAP(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpETG(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpDET(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpETB(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpDP1(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpDP2(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpRO1(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpRO2(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpEWAT(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpSF(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpRO2DP(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpRO2ET(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpETtrans(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    DOUBLE PRECISION    CoEZcrpNIRmET(StartYr : EndYr, 0 : 12, 0 : cozones, 12)
    
    !Coefficient Zone Irrigaiton and Crop Totals
    DOUBLE PRECISION    CoEZICPrecip(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICAc(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICAP(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICSW(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICET(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICRO(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICDP(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICSL(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICPSL(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICDAP(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICETG(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICDET(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICETB(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICDP1(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICDP2(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICRO1(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICRO2(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICEWAT(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICSF(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICRO2DP(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICRO2ET(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICETtrans(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    DOUBLE PRECISION    CoEZICNIRmET(StartYr : EndYr, 0 : 12, 0 : cozones, 4, 12)
    
    REAL    DryETadj, IrrETadj, NIRadjFactor, FSLgw, DryETtoRO, Fslsw, PerToRchg(cozones)
    REAL    AEgadj, AEsadj, DPadj, ROadj
    
    DATA CROPS /'Corn', 'Sugar_Beet', 'Edible_Bean', 'Alfafa', 'Winter_Wheat', &
                'Potato', 'Sorghum', 'Sunflower', 'Soybean', 'Small_Grain', &
                'Fallow', 'Pasture'/
    
    DATA Irrig /'Dry', 'GW', 'SW', 'CO'/

END MODULE PARAM
!*************************************************************************

PROGRAM WSPP_Report
    USE PARAM
    IMPLICIT NONE
    
    Call init
    
    nCounty(0) = "Not_Nebraska"
    Cntyflag = 0
    
    !Load Cell Coefficients
    WRITE (6, *) 'Reading Coefficient File Inofrmation'
    OPEN (11, FILE = TRIM(INDIR)//'CoefTest.txt', STATUS = 'OLD')
    READ (11, *) !Read header
    DO WHILE (.NOT. EOF(11))
        READ (11, *) M, DryETAdj, IrrETadj, NIRadjFactor, AEgadj, Fslgw, &
                        DryETtoRO, AEsadj, Fslsw, PerToRchg(M), DPadj, ROadj
    END DO
    CLOSE (11)
    
    WRITE (6, *) 'Populating County ID Numbers (FIP)'
    OPEN (1, FILE = TRIM(CntyID), STATUS = 'OLD')
    READ (1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) County, CntNum
        nCounty ((CntNum + 1) / 2) = TRIM(County)
    END DO
    CLOSE (1)
    
    WRITE (6, *) 'Populating Cell-County Relationship'
    OPEN (1, FILE = TRIM(CntyCell), STATUS = 'OLD')
    READ (1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) Cell, Cntnum
        CellCnty (Cell) = (cntnum + 1) / 2
        Cntyflag ((cntnum + 1) / 2) = 1
    END DO
    CLOSE (1)
    
    WRITE (6,*) 'READING CELL LOCATION'
    OPEN (8, FILE = TRIM(INDIR)//'CellLoc.txt', STATUS = 'OLD')
    READ(8, *)
    DO WHILE (.NOT. EOF(8))
        READ (8, *, END = 15) Cell, CoefZone(cell), ROZone(cell), MiToGauge(cell)
        IF (Cell .GE. ncells) GOTO 15
    END DO
15  CLOSE (8)
    
    WRITE (6, *) 'POPULATING RUNOFF ZONE COEFS'
	OPEN(9, File = TRIM(INDIR)//'ROZoneCoef.txt', STATUS = 'OLD')
    READ(9, *)
	DO WHILE (.NOT. EOF(9))
		READ (9, *) Zone, LPM(Zone)
	END DO  
	Close (9)
    
    !Get call years
    WRITE (6, *) 'Get WBP relationship years'
    OPEN (11, FILE = TRIM(INDIR)//'CallYear.csv', STATUS = 'OLD')
    READ (11, *)
    DO WHILE (.NOT. EOF(11))
        READ (11, *) yr, iyr
        IF ((yr .LT. startyr) .OR. (yr .GT. endyr)) GOTO 3
        cyr(yr) = iyr
3   END DO
    CLOSE (11)
    
    DO iyear = startyr, endyr
        WRITE (6, *) iyear
        WRITE (YRT, '(I4)') iyear
        WRITE (cYRT, '(I4)') cyr(iyear)
        
        Precip = 0.
        OPEN (10, FILE = TRIM(WBPDIR)//'Precip\Precip'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
        READ (10, *)
        DO WHILE (.NOT. EOF(10))
            READ (10, *, END = 1) Cell, YR, crop, Precip(Cell, 0 : 12)
1        END DO
        CLOSE (10)
        
        OPEN (1, FILE = TRIM(RAWDIR)//'RAW\RAW_WSPP'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
    
        DO WHILE (.NOT. EOF(1))
            READ (1, *, END = 2) Cell, Yr, Mon, crop, Pflag, CoefZ, ROZ, acs, AP, AppSW, ET, RO, & !12
                                    DP, SL, PSL, DAP, ETG, DET, ETB, DP1, & !8
                                    DP2, RO1, RO2, EWat, ETtrans, NIRmET !5
            
2           CONTINUE
            
            IF (MiToGauge(Cell) .EQ. 0.) THEN
                LossFactor = 0.5
            ELSE
                LossFactor = MIN(1 - EXP(-LPM(ROZone(Cell)) * MiToGauge(Cell)), 1.0)
            END IF
            
            SF = RO * (1 - LossFactor)
            RO2DP = RO * LossFactor * PerToRchg(coefZone(Cell))
            RO2ET = RO * LossFactor * (1 - PerToRchg(CoefZone(Cell)))
            
            IF (ABS(RO - SF - RO2DP - RO2ET) .GT. 0.0001) THEN
                CONTINUE
            END IF
                
           !Region Totals
            RegionPrecip(Yr, Mon) = RegionPrecip(Yr, Mon) + Precip(cell, Mon) * acs / 12
            RegionAc(Yr, Mon) = RegionAc(Yr, Mon) + acs
            RegionAP(Yr, Mon) = RegionAP(Yr, Mon) + AP
            RegionSW(Yr, Mon) = RegionSW(Yr, Mon) + AppSW
            RegionET(Yr, Mon) = RegionET(Yr, Mon) + ET
            RegionRO(Yr, Mon) = RegionRO(Yr, Mon) + RO
            RegionDP(Yr, Mon) = RegionDP(Yr, Mon) + DP
            RegionSL(Yr, Mon) = RegionSL(Yr, Mon) + SL
            RegionPSL(Yr, Mon) = RegionPSL(Yr, Mon) + PSL
            RegionDAP(Yr, Mon) = RegionDAP(Yr, Mon) + DAP
            RegionETG(Yr, Mon) = RegionETG(Yr, Mon) + ETG
            RegionDET(Yr, Mon) = RegionDET(Yr, Mon) + DET
            RegionETB(Yr, Mon) = RegionETB(Yr, Mon) + ETB
            RegionDP1(Yr, Mon) = RegionDP1(Yr, Mon) + DP1
            RegionDP2(Yr, Mon) = RegionDP2(Yr, Mon) + DP2
            RegionRO1(Yr, Mon) = RegionRO1(Yr, Mon) + RO1
            RegionRO2(Yr, Mon) = RegionRO2(Yr, Mon) + RO2
            RegionEWAT(Yr, Mon) = RegionEWAT(Yr, Mon) + EWat
            RegionSF(Yr, Mon) = RegionSF(Yr, Mon) + SF
            RegionRO2DP(Yr, Mon) = RegionRO2DP(Yr, Mon) + RO2DP 
            RegionRO2ET(Yr, Mon) = RegionRO2ET(Yr, Mon) + RO2ET
            RegionETtrans(Yr, Mon) = RegionETtrans(Yr, Mon) + ETtrans
            RegionNIRmET(Yr, Mon) = RegionNIRmET(Yr, Mon) + NIRmET
        
            !Regional Irrigation Totals
            RegIrrPrecip(Yr, Mon, Pflag) = RegIrrPrecip(Yr, Mon, Pflag) + Precip(cell, Mon) * acs / 12
            RegIrrAc(Yr, Mon, Pflag) = RegIrrAc(Yr, Mon, Pflag) + acs
            RegIrrAP(Yr, Mon, Pflag) = RegIrrAP(Yr, Mon, Pflag) + AP
            RegIrrSW(Yr, Mon, Pflag) = RegIrrSW(Yr, Mon, Pflag) + AppSW
            RegIrrET(Yr, Mon, Pflag) = RegIrrET(Yr, Mon, Pflag) + ET
            RegIrrRO(Yr, Mon, Pflag) = RegIrrRO(Yr, Mon, Pflag) + RO
            RegIrrDP(Yr, Mon, Pflag) = RegIrrDP(Yr, Mon, Pflag) + DP
            RegIrrSL(Yr, Mon, Pflag) = RegIrrSL(Yr, Mon, Pflag) + SL
            RegIrrPSL(Yr, Mon, Pflag) = RegIrrPSL(Yr, Mon, Pflag) + PSL
            RegIrrDAP(Yr, Mon, Pflag) = RegIrrDAP(Yr, Mon, Pflag) + DAP
            RegIrrETG(Yr, Mon, Pflag) = RegIrrETG(Yr, Mon, Pflag) + ETG
            RegIrrDET(Yr, Mon, Pflag) = RegIrrDET(Yr, Mon, Pflag) + DET
            RegIrrETB(Yr, Mon, Pflag) = RegIrrETB(Yr, Mon, Pflag) + ETB
            RegIrrDP1(Yr, Mon, Pflag) = RegIrrDP1(Yr, Mon, Pflag) + DP1
            RegIrrDP2(Yr, Mon, Pflag) = RegIrrDP2(Yr, Mon, Pflag) + DP2
            RegIrrRO1(Yr, Mon, Pflag) = RegIrrRO1(Yr, Mon, Pflag) + RO1
            RegIrrRO2(Yr, Mon, Pflag) = RegIrrRO2(Yr, Mon, Pflag) + RO2
            RegIrrEWAT(Yr, Mon, Pflag) = RegIrrEWAT(Yr, Mon, Pflag) + EWat
            RegIrrSF(Yr, Mon, Pflag) = RegIrrSF(Yr, Mon, Pflag) + SF
            RegIrrRO2DP(Yr, Mon, Pflag) = RegIrrRO2DP(Yr, Mon, Pflag) + RO2DP
            RegIrrRO2ET(Yr, Mon, Pflag) = RegIrrRO2ET(Yr, Mon, Pflag) + RO2ET
            RegIrrETtrans(Yr, Mon, Pflag) = RegIrrETtrans(Yr, Mon, Pflag) + ETtrans
            RegIrrNIRmET(Yr, Mon, Pflag) = RegIrrNIRmET(Yr, Mon, Pflag) + NIRmET
        
            !Regional Crop Totals
            RegCropPrecip(Yr, Mon, crop) = RegCropPrecip(Yr, Mon, crop) + Precip(cell, Mon) * acs / 12
            RegCropAc(Yr, Mon, crop) = RegCropAc(Yr, Mon, crop) + acs
            RegCropAP(Yr, Mon, crop) = RegCropAP(Yr, Mon, crop) + AP
            RegCropSW(Yr, Mon, crop) = RegCropSW(Yr, Mon, crop) + AppSW
            RegCropET(Yr, Mon, crop) = RegCropET(Yr, Mon, crop) + ET
            RegCropRO(Yr, Mon, crop) = RegCropRO(Yr, Mon, crop) + RO
            RegCropDP(Yr, Mon, crop) = RegCropDP(Yr, Mon, crop) + DP
            RegCropSL(Yr, Mon, crop) = RegCropSL(Yr, Mon, crop) + SL
            RegCropPSL(Yr, Mon, crop) = RegCropPSL(Yr, Mon, crop) + PSL
            RegCropDAP(Yr, Mon, crop) = RegCropDAP(Yr, Mon, crop) + DAP
            RegCropETG(Yr, Mon, crop) = RegCropETG(Yr, Mon, crop) + ETG
            RegCropDET(Yr, Mon, crop) = RegCropDET(Yr, Mon, crop) + DET
            RegCropETB(Yr, Mon, crop) = RegCropETB(Yr, Mon, crop) + ETB
            RegCropDP1(Yr, Mon, crop) = RegCropDP1(Yr, Mon, crop) + DP1
            RegCropDP2(Yr, Mon, crop) = RegCropDP2(Yr, Mon, crop) + DP2
            RegCropRO1(Yr, Mon, crop) = RegCropRO1(Yr, Mon, crop) + RO1
            RegCropRO2(Yr, Mon, crop) = RegCropRO2(Yr, Mon, crop) + RO2
            RegCropEWAT(Yr, Mon, crop) = RegCropEWAT(Yr, Mon, crop) + EWat 
            RegCropSF(Yr, Mon, crop) = RegCropSF(Yr, Mon, crop) + SF
            RegCropRO2DP(Yr, Mon, crop) = RegCropRO2DP(Yr, Mon, crop) + RO2DP
            RegCropRO2ET(Yr, Mon, crop) = RegCropRO2ET(Yr, Mon, crop) + RO2ET
            RegCropETtrans(Yr, Mon, crop) = RegCropETtrans(Yr, Mon, crop) + ETtrans
            RegCropNIRmET(Yr, Mon, crop) = RegCropNIRmET(Yr, Mon, crop) + NIRmET

            !Region crop and Irrigation Totals
            RICPrecip(Yr, Mon, pflag, crop) = RICPrecip(Yr, Mon, pflag, crop) + Precip(cell, Mon) * acs / 12
            RICAc(Yr, Mon, pflag, crop) = RICAc(Yr, Mon, pflag, crop) + acs
            RICAP(Yr, Mon, pflag, crop) = RICAP(Yr, Mon, pflag, crop) + AP
            RICSW(Yr, Mon, pflag, crop) = RICSW(Yr, Mon, pflag, crop) + AppSW
            RICET(Yr, Mon, pflag, crop) = RICET(Yr, Mon, pflag, crop) + ET
            RICRO(Yr, Mon, pflag, crop) = RICRO(Yr, Mon, pflag, crop) + RO
            RICDP(Yr, Mon, pflag, crop) = RICDP(Yr, Mon, pflag, crop) + DP
            RICSL(Yr, Mon, pflag, crop) = RICSL(Yr, Mon, pflag, crop) + SL
            RICPSL(Yr, Mon, pflag, crop) = RICPSL(Yr, Mon, pflag, crop) + PSL
            RICDAP(Yr, Mon, pflag, crop) = RICDAP(Yr, Mon, pflag, crop) + DAP
            RICETG(Yr, Mon, pflag, crop) = RICETG(Yr, Mon, pflag, crop) + ETG
            RICDET(Yr, Mon, pflag, crop) = RICDET(Yr, Mon, pflag, crop) + DET
            RICETB(Yr, Mon, pflag, crop) = RICETB(Yr, Mon, pflag, crop) + ETB
            RICDP1(Yr, Mon, pflag, crop) = RICDP1(Yr, Mon, pflag, crop) + DP1
            RICDP2(Yr, Mon, pflag, crop) = RICDP2(Yr, Mon, pflag, crop) + DP2
            RICRO1(Yr, Mon, pflag, crop) = RICRO1(Yr, Mon, pflag, crop) + RO1
            RICRO2(Yr, Mon, pflag, crop) = RICRO2(Yr, Mon, pflag, crop) + RO2
            RICEWAT(Yr, Mon, pflag, crop) = RICEWAT(Yr, Mon, pflag, crop) + EWat
            RICSF(Yr, Mon, pflag, crop) = RICSF(Yr, Mon, pflag, crop) + SF
            RICRO2DP(Yr, Mon, pflag, crop) = RICRO2DP(Yr, Mon, pflag, crop) + RO2DP
            RICRO2ET(Yr, Mon, pflag, crop) = RICRO2ET(Yr, Mon, pflag, crop) + RO2ET
            RICETtrans(Yr, Mon, pflag, crop) = RICETtrans(Yr, Mon, pflag, crop) + ETtrans
            RICNIRmET(Yr, Mon, pflag, crop) = RICNIRmET(Yr, Mon, pflag, crop) + NIRmET
        
            !County Totals
            cntyPrecip(Yr, Mon, CellCnty(Cell)) = cntyPrecip(Yr, Mon, CellCnty(Cell)) + Precip(cell, Mon) * acs / 12
            cntyAc(Yr, Mon, CellCnty(Cell)) = cntyAc(Yr, Mon, CellCnty(Cell)) + acs
            cntyAP(Yr, Mon, CellCnty(Cell)) = cntyAP(Yr, Mon, CellCnty(Cell)) + AP
            cntySW(Yr, Mon, CellCnty(Cell)) = cntySW(Yr, Mon, CellCnty(Cell)) + AppSW
            cntyET(Yr, Mon, CellCnty(Cell)) = cntyET(Yr, Mon, CellCnty(Cell)) + ET
            cntyRO(Yr, Mon, CellCnty(Cell)) = cntyRO(Yr, Mon, CellCnty(Cell)) + RO
            cntyDP(Yr, Mon, CellCnty(Cell)) = cntyDP(Yr, Mon, CellCnty(Cell)) + DP
            cntySL(Yr, Mon, CellCnty(Cell)) = cntySL(Yr, Mon, CellCnty(Cell)) + SL
            cntyPSL(Yr, Mon, CellCnty(Cell)) = cntyPSL(Yr, Mon, CellCnty(Cell)) + PSL
            cntyDAP(Yr, Mon, CellCnty(Cell)) = cntyDAP(Yr, Mon, CellCnty(Cell)) + DAP
            cntyETG(Yr, Mon, CellCnty(Cell)) = cntyETG(Yr, Mon, CellCnty(Cell)) + ETG
            cntyDET(Yr, Mon, CellCnty(Cell)) = cntyDET(Yr, Mon, CellCnty(Cell)) + DET
            cntyETB(Yr, Mon, CellCnty(Cell)) = cntyETB(Yr, Mon, CellCnty(Cell)) + ETB
            cntyDP1(Yr, Mon, CellCnty(Cell)) = cntyDP1(Yr, Mon, CellCnty(Cell)) + DP1
            cntyDP2(Yr, Mon, CellCnty(Cell)) = cntyDP2(Yr, Mon, CellCnty(Cell)) + DP2
            cntyRO1(Yr, Mon, CellCnty(Cell)) = cntyRO1(Yr, Mon, CellCnty(Cell)) + RO1
            cntyRO2(Yr, Mon, CellCnty(Cell)) = cntyRO2(Yr, Mon, CellCnty(Cell)) + RO2
            cntyEWAT(Yr, Mon, CellCnty(Cell)) = cntyEWAT(Yr, Mon, CellCnty(Cell)) + EWat
            cntySF(Yr, Mon, CellCnty(Cell)) = cntySF(Yr, Mon, CellCnty(Cell)) + SF
            cntyRO2DP(Yr, Mon, CellCnty(Cell)) = cntyRO2DP(Yr, Mon, CellCnty(Cell)) + RO2DP
            cntyRO2ET(Yr, Mon, CellCnty(Cell)) = cntyRO2ET(Yr, Mon, CellCnty(Cell)) + RO2ET
            cntyETtrans(Yr, Mon, CellCnty(Cell)) = cntyETtrans(Yr, Mon, CellCnty(Cell)) + ETtrans
            cntyNIRmET(Yr, Mon, CellCnty(Cell)) = cntyNIRmET(Yr, Mon, CellCnty(Cell)) + NIRmET
        
            !County Irrigation Totals   
            cntyirrPrecip(Yr, Mon, CellCnty(Cell), pflag) = cntyirrPrecip(Yr, Mon, CellCnty(Cell), pflag) + Precip(cell, Mon) * acs / 12
            cntyirrAc(Yr, Mon, CellCnty(Cell), pflag) = cntyirrAc(Yr, Mon, CellCnty(Cell), pflag) + acs
            cntyirrAP(Yr, Mon, CellCnty(Cell), pflag) = cntyirrAP(Yr, Mon, CellCnty(Cell), pflag) + AP
            cntyirrSW(Yr, Mon, CellCnty(Cell), pflag) = cntyirrSW(Yr, Mon, CellCnty(Cell), pflag) + AppSW
            cntyirrET(Yr, Mon, CellCnty(Cell), pflag) = cntyirrET(Yr, Mon, CellCnty(Cell), pflag) + ET
            cntyirrRO(Yr, Mon, CellCnty(Cell), pflag) = cntyirrRO(Yr, Mon, CellCnty(Cell), pflag) + RO
            cntyirrDP(Yr, Mon, CellCnty(Cell), pflag) = cntyirrDP(Yr, Mon, CellCnty(Cell), pflag) + DP
            cntyirrSL(Yr, Mon, CellCnty(Cell), pflag) = cntyirrSL(Yr, Mon, CellCnty(Cell), pflag) + SL
            cntyirrPSL(Yr, Mon, CellCnty(Cell), pflag) = cntyirrPSL(Yr, Mon, CellCnty(Cell), pflag) + PSL
            cntyirrDAP(Yr, Mon, CellCnty(Cell), pflag) = cntyirrDAP(Yr, Mon, CellCnty(Cell), pflag) + DAP
            cntyirrETG(Yr, Mon, CellCnty(Cell), pflag) = cntyirrETG(Yr, Mon, CellCnty(Cell), pflag) + ETG
            cntyirrDET(Yr, Mon, CellCnty(Cell), pflag) = cntyirrDET(Yr, Mon, CellCnty(Cell), pflag) + DET
            cntyirrETB(Yr, Mon, CellCnty(Cell), pflag) = cntyirrETB(Yr, Mon, CellCnty(Cell), pflag) + ETB
            cntyirrDP1(Yr, Mon, CellCnty(Cell), pflag) = cntyirrDP1(Yr, Mon, CellCnty(Cell), pflag) + DP1
            cntyirrDP2(Yr, Mon, CellCnty(Cell), pflag) = cntyirrDP2(Yr, Mon, CellCnty(Cell), pflag) + DP2
            cntyirrRO1(Yr, Mon, CellCnty(Cell), pflag) = cntyirrRO1(Yr, Mon, CellCnty(Cell), pflag) + RO1
            cntyirrRO2(Yr, Mon, CellCnty(Cell), pflag) = cntyirrRO2(Yr, Mon, CellCnty(Cell), pflag) + RO2
            cntyirrEWAT(Yr, Mon, CellCnty(Cell), pflag) = cntyirrEWAT(Yr, Mon, CellCnty(Cell), pflag) + EWat        
            cntyirrSF(Yr, Mon, CellCnty(Cell), pflag) = cntyirrSF(Yr, Mon, CellCnty(Cell), pflag) + SF
            cntyirrRO2DP(Yr, Mon, CellCnty(Cell), pflag) = cntyirrRO2DP(Yr, Mon, CellCnty(Cell), pflag) + RO2DP
            cntyirrRO2ET(Yr, Mon, CellCnty(Cell), pflag) = cntyirrRO2ET(Yr, Mon, CellCnty(Cell), pflag) + RO2ET
            cntyirrETtrans(Yr, Mon, CellCnty(Cell), pflag) = cntyirrETtrans(Yr, Mon, CellCnty(Cell), pflag) + ETtrans
            cntyirrNIRmET(Yr, Mon, CellCnty(Cell), pflag) = cntyirrNIRmET(Yr, Mon, CellCnty(Cell), pflag) + NIRmET
        
            !County Crop Totals        
            cntycrpPrecip(Yr, Mon, CellCnty(Cell), crop) = cntycrpPrecip(Yr, Mon, CellCnty(Cell), crop) + Precip(cell, Mon) * acs / 12
            cntycrpAc(Yr, Mon, CellCnty(Cell), crop) = cntycrpAc(Yr, Mon, CellCnty(Cell), crop) + acs
            cntycrpAP(Yr, Mon, CellCnty(Cell), crop) = cntycrpAP(Yr, Mon, CellCnty(Cell), crop) + AP
            cntycrpSW(Yr, Mon, CellCnty(Cell), crop) = cntycrpSW(Yr, Mon, CellCnty(Cell), crop) + AppSW
            cntycrpET(Yr, Mon, CellCnty(Cell), crop) = cntycrpET(Yr, Mon, CellCnty(Cell), crop) + ET
            cntycrpRO(Yr, Mon, CellCnty(Cell), crop) = cntycrpRO(Yr, Mon, CellCnty(Cell), crop) + RO
            cntycrpDP(Yr, Mon, CellCnty(Cell), crop) = cntycrpDP(Yr, Mon, CellCnty(Cell), crop) + DP
            cntycrpSL(Yr, Mon, CellCnty(Cell), crop) = cntycrpSL(Yr, Mon, CellCnty(Cell), crop) + SL
            cntycrpPSL(Yr, Mon, CellCnty(Cell), crop) = cntycrpPSL(Yr, Mon, CellCnty(Cell), crop) + PSL
            cntycrpDAP(Yr, Mon, CellCnty(Cell), crop) = cntycrpDAP(Yr, Mon, CellCnty(Cell), crop) + DAP
            cntycrpETG(Yr, Mon, CellCnty(Cell), crop) = cntycrpETG(Yr, Mon, CellCnty(Cell), crop) + ETG
            cntycrpDET(Yr, Mon, CellCnty(Cell), crop) = cntycrpDET(Yr, Mon, CellCnty(Cell), crop) + DET
            cntycrpETB(Yr, Mon, CellCnty(Cell), crop) = cntycrpETB(Yr, Mon, CellCnty(Cell), crop) + ETB
            cntycrpDP1(Yr, Mon, CellCnty(Cell), crop) = cntycrpDP1(Yr, Mon, CellCnty(Cell), crop) + DP1
            cntycrpDP2(Yr, Mon, CellCnty(Cell), crop) = cntycrpDP2(Yr, Mon, CellCnty(Cell), crop) + DP2
            cntycrpRO1(Yr, Mon, CellCnty(Cell), crop) = cntycrpRO1(Yr, Mon, CellCnty(Cell), crop) + RO1
            cntycrpRO2(Yr, Mon, CellCnty(Cell), crop) = cntycrpRO2(Yr, Mon, CellCnty(Cell), crop) + RO2
            cntycrpEWAT(Yr, Mon, CellCnty(Cell), crop) = cntycrpEWAT(Yr, Mon, CellCnty(Cell), crop) + EWat        
            cntycrpSF(Yr, Mon, CellCnty(Cell), crop) = cntycrpSF(Yr, Mon, CellCnty(Cell), crop) + SF
            cntycrpRO2DP(Yr, Mon, CellCnty(Cell), crop) = cntycrpRO2DP(Yr, Mon, CellCnty(Cell), crop) + RO2DP 
            cntycrpRO2ET(Yr, Mon, CellCnty(Cell), crop) = cntycrpRO2ET(Yr, Mon, CellCnty(Cell), crop) + RO2ET
            cntycrpETtrans(Yr, Mon, CellCnty(Cell), crop) = cntycrpETtrans(Yr, Mon, CellCnty(Cell), crop) + ETtrans
            cntycrpNIRmET(Yr, Mon, CellCnty(Cell), crop) = cntycrpNIRmET(Yr, Mon, CellCnty(Cell), crop) + NIRmET
        
            !County Irrigation and Crop Totals        
            CICPrecip(Yr, Mon, CellCnty(Cell), pflag, crop) = CICPrecip(Yr, Mon, CellCnty(Cell), pflag, crop) + Precip(cell, Mon) * acs / 12
            CICAc(Yr, Mon, CellCnty(Cell), pflag, crop) = CICAc(Yr, Mon, CellCnty(Cell), pflag, crop) + acs
            CICAP(Yr, Mon, CellCnty(Cell), pflag, crop) = CICAP(Yr, Mon, CellCnty(Cell), pflag, crop) + AP
            CICSW(Yr, Mon, CellCnty(Cell), pflag, crop) = CICSW(Yr, Mon, CellCnty(Cell), pflag, crop) + AppSW
            CICET(Yr, Mon, CellCnty(Cell), pflag, crop) = CICET(Yr, Mon, CellCnty(Cell), pflag, crop) + ET
            CICRO(Yr, Mon, CellCnty(Cell), pflag, crop) = CICRO(Yr, Mon, CellCnty(Cell), pflag, crop) + RO
            CICDP(Yr, Mon, CellCnty(Cell), pflag, crop) = CICDP(Yr, Mon, CellCnty(Cell), pflag, crop) + DP
            CICSL(Yr, Mon, CellCnty(Cell), pflag, crop) = CICSL(Yr, Mon, CellCnty(Cell), pflag, crop) + SL
            CICPSL(Yr, Mon, CellCnty(Cell), pflag, crop) = CICPSL(Yr, Mon, CellCnty(Cell), pflag, crop) + PSL
            CICDAP(Yr, Mon, CellCnty(Cell), pflag, crop) = CICDAP(Yr, Mon, CellCnty(Cell), pflag, crop) + DAP
            CICETG(Yr, Mon, CellCnty(Cell), pflag, crop) = CICETG(Yr, Mon, CellCnty(Cell), pflag, crop) + ETG
            CICDET(Yr, Mon, CellCnty(Cell), pflag, crop) = CICDET(Yr, Mon, CellCnty(Cell), pflag, crop) + DET
            CICETB(Yr, Mon, CellCnty(Cell), pflag, crop) = CICETB(Yr, Mon, CellCnty(Cell), pflag, crop) + ETB
            CICDP1(Yr, Mon, CellCnty(Cell), pflag, crop) = CICDP1(Yr, Mon, CellCnty(Cell), pflag, crop) + DP1
            CICDP2(Yr, Mon, CellCnty(Cell), pflag, crop) = CICDP2(Yr, Mon, CellCnty(Cell), pflag, crop) + DP2
            CICRO1(Yr, Mon, CellCnty(Cell), pflag, crop) = CICRO1(Yr, Mon, CellCnty(Cell), pflag, crop) + RO1
            CICRO2(Yr, Mon, CellCnty(Cell), pflag, crop) = CICRO2(Yr, Mon, CellCnty(Cell), pflag, crop) + RO2
            CICEWAT(Yr, Mon, CellCnty(Cell), pflag, crop) = CICEWAT(Yr, Mon, CellCnty(Cell), pflag, crop) + EWat
            CICSF(Yr, Mon, CellCnty(Cell), pflag, crop) = CICSF(Yr, Mon, CellCnty(Cell), pflag, crop) + SF
            CICRO2DP(Yr, Mon, CellCnty(Cell), pflag, crop) = CICRO2DP(Yr, Mon, CellCnty(Cell), pflag, crop) + RO2DP 
            CICRO2ET(Yr, Mon, CellCnty(Cell), pflag, crop) = CICRO2ET(Yr, Mon, CellCnty(Cell), pflag, crop) + RO2ET
            CICETtrans(Yr, Mon, CellCnty(Cell), pflag, crop) = CICETtrans(Yr, Mon, CellCnty(Cell), pflag, crop) + ETtrans
            CICNIRmET(Yr, Mon, CellCnty(Cell), pflag, crop) = CICNIRmET(Yr, Mon, CellCnty(Cell), pflag, crop) + NIRmET
        
            !Runoff Zone Totals
            ROZPrecip(Yr, Mon, ROZ) = ROZPrecip(Yr, Mon, ROZ) + Precip(cell, Mon) * acs / 12
            ROZAc(Yr, Mon, ROZ) = ROZAc(Yr, Mon, ROZ) + acs
            ROZAP(Yr, Mon, ROZ) = ROZAP(Yr, Mon, ROZ) + AP
            ROZSW(Yr, Mon, ROZ) = ROZSW(Yr, Mon, ROZ) + AppSW
            ROZET(Yr, Mon, ROZ) = ROZET(Yr, Mon, ROZ) + ET
            ROZRO(Yr, Mon, ROZ) = ROZRO(Yr, Mon, ROZ) + RO
            ROZDP(Yr, Mon, ROZ) = ROZDP(Yr, Mon, ROZ) + DP
            ROZSL(Yr, Mon, ROZ) = ROZSL(Yr, Mon, ROZ) + SL
            ROZPSL(Yr, Mon, ROZ) = ROZPSL(Yr, Mon, ROZ) + PSL
            ROZDAP(Yr, Mon, ROZ) = ROZDAP(Yr, Mon, ROZ) + DAP
            ROZETG(Yr, Mon, ROZ) = ROZETG(Yr, Mon, ROZ) + ETG
            ROZDET(Yr, Mon, ROZ) = ROZDET(Yr, Mon, ROZ) + DET
            ROZETB(Yr, Mon, ROZ) = ROZETB(Yr, Mon, ROZ) + ETB
            ROZDP1(Yr, Mon, ROZ) = ROZDP1(Yr, Mon, ROZ) + DP1
            ROZDP2(Yr, Mon, ROZ) = ROZDP2(Yr, Mon, ROZ) + DP2
            ROZRO1(Yr, Mon, ROZ) = ROZRO1(Yr, Mon, ROZ) + RO1
            ROZRO2(Yr, Mon, ROZ) = ROZRO2(Yr, Mon, ROZ) + RO2
            ROZEWAT(Yr, Mon, ROZ) = ROZEWAT(Yr, Mon, ROZ) + EWat
            ROZSF(Yr, Mon, ROZ) = ROZSF(Yr, Mon, ROZ) + SF
            ROZRO2DP(Yr, Mon, ROZ) = ROZRO2DP(Yr, Mon, ROZ) + RO2DP
            ROZRO2ET(Yr, Mon, ROZ) = ROZRO2ET(Yr, Mon, ROZ) + RO2ET
            ROZETtrans(Yr, Mon, ROZ) = ROZETtrans(Yr, Mon, ROZ) + ETtrans
            ROZNIRmET(Yr, Mon, ROZ) = ROZNIRmET(Yr, Mon, ROZ) + NIRmET
            
            !Runoff Zone Irrigation Totals
            ROZirrPrecip(Yr, Mon, ROZ, pflag) = ROZirrPrecip(Yr, Mon, ROZ, pflag) + Precip(cell, Mon) * acs / 12
            ROZirrAc(Yr, Mon, ROZ, pflag) = ROZirrAc(Yr, Mon, ROZ, pflag) + acs
            ROZirrAP(Yr, Mon, ROZ, pflag) = ROZirrAP(Yr, Mon, ROZ, pflag) + AP
            ROZirrSW(Yr, Mon, ROZ, pflag) = ROZirrSW(Yr, Mon, ROZ, pflag) + AppSW
            ROZirrET(Yr, Mon, ROZ, pflag) = ROZirrET(Yr, Mon, ROZ, pflag) + ET
            ROZirrRO(Yr, Mon, ROZ, pflag) = ROZirrRO(Yr, Mon, ROZ, pflag) + RO
            ROZirrDP(Yr, Mon, ROZ, pflag) = ROZirrDP(Yr, Mon, ROZ, pflag) + DP
            ROZirrSL(Yr, Mon, ROZ, pflag) = ROZirrSL(Yr, Mon, ROZ, pflag) + SL
            ROZirrPSL(Yr, Mon, ROZ, pflag) = ROZirrPSL(Yr, Mon, ROZ, pflag) + PSL
            ROZirrDAP(Yr, Mon, ROZ, pflag) = ROZirrDAP(Yr, Mon, ROZ, pflag) + DAP
            ROZirrETG(Yr, Mon, ROZ, pflag) = ROZirrETG(Yr, Mon, ROZ, pflag) + ETG
            ROZirrDET(Yr, Mon, ROZ, pflag) = ROZirrDET(Yr, Mon, ROZ, pflag) + DET
            ROZirrETB(Yr, Mon, ROZ, pflag) = ROZirrETB(Yr, Mon, ROZ, pflag) + ETB
            ROZirrDP1(Yr, Mon, ROZ, pflag) = ROZirrDP1(Yr, Mon, ROZ, pflag) + DP1
            ROZirrDP2(Yr, Mon, ROZ, pflag) = ROZirrDP2(Yr, Mon, ROZ, pflag) + DP2
            ROZirrRO1(Yr, Mon, ROZ, pflag) = ROZirrRO1(Yr, Mon, ROZ, pflag) + RO1
            ROZirrRO2(Yr, Mon, ROZ, pflag) = ROZirrRO2(Yr, Mon, ROZ, pflag) + RO2
            ROZirrEWAT(Yr, Mon, ROZ, pflag) = ROZirrEWAT(Yr, Mon, ROZ, pflag) + EWat
            ROZirrSF(Yr, Mon, ROZ, pflag) = ROZirrSF(Yr, Mon, ROZ, pflag) + SF
            ROZirrRO2DP(Yr, Mon, ROZ, pflag) = ROZirrRO2DP(Yr, Mon, ROZ, pflag) + RO2DP
            ROZirrRO2ET(Yr, Mon, ROZ, pflag) = ROZirrRO2ET(Yr, Mon, ROZ, pflag) + RO2ET
            ROZirrETtrans(Yr, Mon, ROZ, pflag) = ROZirrETtrans(Yr, Mon, ROZ, pflag) + ETtrans
            ROZirrNIRmET(Yr, Mon, ROZ, pflag) = ROZirrNIRmET(Yr, Mon, ROZ, pflag) + NIRmET
        
            !Runoff Zone Crop Totals
            ROZcrpPrecip(Yr, Mon, ROZ, crop) = ROZcrpPrecip(Yr, Mon, ROZ, crop) + Precip(cell, Mon) * acs / 12
            ROZcrpAc(Yr, Mon, ROZ, crop) = ROZcrpAc(Yr, Mon, ROZ, crop) + acs
            ROZcrpAP(Yr, Mon, ROZ, crop) = ROZcrpAP(Yr, Mon, ROZ, crop) + AP
            ROZcrpSW(Yr, Mon, ROZ, crop) = ROZcrpSW(Yr, Mon, ROZ, crop) + AppSW
            ROZcrpET(Yr, Mon, ROZ, crop) = ROZcrpET(Yr, Mon, ROZ, crop) + ET
            ROZcrpRO(Yr, Mon, ROZ, crop) = ROZcrpRO(Yr, Mon, ROZ, crop) + RO
            ROZcrpDP(Yr, Mon, ROZ, crop) = ROZcrpDP(Yr, Mon, ROZ, crop) + DP
            ROZcrpSL(Yr, Mon, ROZ, crop) = ROZcrpSL(Yr, Mon, ROZ, crop) + SL
            ROZcrpPSL(Yr, Mon, ROZ, crop) = ROZcrpPSL(Yr, Mon, ROZ, crop) + PSL
            ROZcrpDAP(Yr, Mon, ROZ, crop) = ROZcrpDAP(Yr, Mon, ROZ, crop) + DAP
            ROZcrpETG(Yr, Mon, ROZ, crop) = ROZcrpETG(Yr, Mon, ROZ, crop) + ETG
            ROZcrpDET(Yr, Mon, ROZ, crop) = ROZcrpDET(Yr, Mon, ROZ, crop) + DET
            ROZcrpETB(Yr, Mon, ROZ, crop) = ROZcrpETB(Yr, Mon, ROZ, crop) + ETB
            ROZcrpDP1(Yr, Mon, ROZ, crop) = ROZcrpDP1(Yr, Mon, ROZ, crop) + DP1
            ROZcrpDP2(Yr, Mon, ROZ, crop) = ROZcrpDP2(Yr, Mon, ROZ, crop) + DP2
            ROZcrpRO1(Yr, Mon, ROZ, crop) = ROZcrpRO1(Yr, Mon, ROZ, crop) + RO1
            ROZcrpRO2(Yr, Mon, ROZ, crop) = ROZcrpRO2(Yr, Mon, ROZ, crop) + RO2
            ROZcrpEWAT(Yr, Mon, ROZ, crop) = ROZcrpEWAT(Yr, Mon, ROZ, crop) + EWat
            ROZcrpSF(Yr, Mon, ROZ, crop) = ROZcrpSF(Yr, Mon, ROZ, crop) + SF
            ROZcrpRO2DP(Yr, Mon, ROZ, crop) = ROZcrpRO2DP(Yr, Mon, ROZ, crop) + RO2DP
            ROZcrpRO2ET(Yr, Mon, ROZ, crop) = ROZcrpRO2ET(Yr, Mon, ROZ, crop) + RO2ET
            ROZcrpETtrans(Yr, Mon, ROZ, crop) = ROZcrpETtrans(Yr, Mon, ROZ, crop) + ETtrans
            ROZcrpNIRmET(Yr, Mon, ROZ, crop) = ROZcrpNIRmET(Yr, Mon, ROZ, crop) + NIRmET
            
            !Runoff Zone Irrigation and Crop Totals
            ROZICPrecip(Yr, Mon, ROZ, pflag, crop) = ROZICPrecip(Yr, Mon, ROZ, pflag, crop) + Precip(cell, Mon) * acs / 12
            ROZICAc(Yr, Mon, ROZ, pflag, crop) = ROZICAc(Yr, Mon, ROZ, pflag, crop) + acs
            ROZICAP(Yr, Mon, ROZ, pflag, crop) = ROZICAP(Yr, Mon, ROZ, pflag, crop) + AP
            ROZICSW(Yr, Mon, ROZ, pflag, crop) = ROZICSW(Yr, Mon, ROZ, pflag, crop) + AppSW
            ROZICET(Yr, Mon, ROZ, pflag, crop) = ROZICET(Yr, Mon, ROZ, pflag, crop) + ET
            ROZICRO(Yr, Mon, ROZ, pflag, crop) = ROZICRO(Yr, Mon, ROZ, pflag, crop) + RO
            ROZICDP(Yr, Mon, ROZ, pflag, crop) = ROZICDP(Yr, Mon, ROZ, pflag, crop) + DP
            ROZICSL(Yr, Mon, ROZ, pflag, crop) = ROZICSL(Yr, Mon, ROZ, pflag, crop) + SL
            ROZICPSL(Yr, Mon, ROZ, pflag, crop) = ROZICPSL(Yr, Mon, ROZ, pflag, crop) + PSL
            ROZICDAP(Yr, Mon, ROZ, pflag, crop) = ROZICDAP(Yr, Mon, ROZ, pflag, crop) + DAP
            ROZICETG(Yr, Mon, ROZ, pflag, crop) = ROZICETG(Yr, Mon, ROZ, pflag, crop) + ETG
            ROZICDET(Yr, Mon, ROZ, pflag, crop) = ROZICDET(Yr, Mon, ROZ, pflag, crop) + DET
            ROZICETB(Yr, Mon, ROZ, pflag, crop) = ROZICETB(Yr, Mon, ROZ, pflag, crop) + ETB
            ROZICDP1(Yr, Mon, ROZ, pflag, crop) = ROZICDP1(Yr, Mon, ROZ, pflag, crop) + DP1
            ROZICDP2(Yr, Mon, ROZ, pflag, crop) = ROZICDP2(Yr, Mon, ROZ, pflag, crop) + DP2
            ROZICRO1(Yr, Mon, ROZ, pflag, crop) = ROZICRO1(Yr, Mon, ROZ, pflag, crop) + RO1
            ROZICRO2(Yr, Mon, ROZ, pflag, crop) = ROZICRO2(Yr, Mon, ROZ, pflag, crop) + RO2
            ROZICEWAT(Yr, Mon, ROZ, pflag, crop) = ROZICEWAT(Yr, Mon, ROZ, pflag, crop) + EWat
            ROZICSF(Yr, Mon, ROZ, pflag, crop) = ROZICSF(Yr, Mon, ROZ, pflag, crop) + SF
            ROZICRO2DP(Yr, Mon, ROZ, pflag, crop) = ROZICRO2DP(Yr, Mon, ROZ, pflag, crop) + RO2DP
            ROZICRO2ET(Yr, Mon, ROZ, pflag, crop) = ROZICRO2ET(Yr, Mon, ROZ, pflag, crop) + RO2ET
            ROZICETtrans(Yr, Mon, ROZ, pflag, crop) = ROZICETtrans(Yr, Mon, ROZ, pflag, crop) + ETtrans
            ROZICNIRmET(Yr, Mon, ROZ, pflag, crop) = ROZICNIRmET(Yr, Mon, ROZ, pflag, crop) + NIRmET
        
            !Coefficient Zone Totals
            CoEZPrecip(Yr, Mon, coefZ) = CoEZPrecip(Yr, Mon, coefZ) + Precip(cell, Mon) * acs / 12
            CoEZAc(Yr, Mon, coefZ) = CoEZAc(Yr, Mon, coefZ) + acs
            CoEZAP(Yr, Mon, coefZ) = CoEZAP(Yr, Mon, coefZ) + AP
            CoEZSW(Yr, Mon, coefZ) = CoEZSW(Yr, Mon, coefZ) + AppSW
            CoEZET(Yr, Mon, coefZ) = CoEZET(Yr, Mon, coefZ) + ET
            CoEZRO(Yr, Mon, coefZ) = CoEZRO(Yr, Mon, coefZ) + RO
            CoEZDP(Yr, Mon, coefZ) = CoEZDP(Yr, Mon, coefZ) + DP
            CoEZSL(Yr, Mon, coefZ) = CoEZSL(Yr, Mon, coefZ) + SL
            CoEZPSL(Yr, Mon, coefZ) = CoEZPSL(Yr, Mon, coefZ) + PSL
            CoEZDAP(Yr, Mon, coefZ) = CoEZDAP(Yr, Mon, coefZ) + DAP
            CoEZETG(Yr, Mon, coefZ) = CoEZETG(Yr, Mon, coefZ) + ETG
            CoEZDET(Yr, Mon, coefZ) = CoEZDET(Yr, Mon, coefZ) + DET
            CoEZETB(Yr, Mon, coefZ) = CoEZETB(Yr, Mon, coefZ) + ETB
            CoEZDP1(Yr, Mon, coefZ) = CoEZDP1(Yr, Mon, coefZ) + DP1
            CoEZDP2(Yr, Mon, coefZ) = CoEZDP2(Yr, Mon, coefZ) + DP2
            CoEZRO1(Yr, Mon, coefZ) = CoEZRO1(Yr, Mon, coefZ) + RO1
            CoEZRO2(Yr, Mon, coefZ) = CoEZRO2(Yr, Mon, coefZ) + RO2
            CoEZEWAT(Yr, Mon, coefZ) = CoEZEWAT(Yr, Mon, coefZ) + EWat
            CoEZSF(Yr, Mon, coefZ) = CoEZSF(Yr, Mon, coefZ) + SF
            CoEZRO2DP(Yr, Mon, coefZ) = CoEZRO2DP(Yr, Mon, coefZ) + RO2DP
            CoEZRO2ET(Yr, Mon, coefZ) = CoEZRO2ET(Yr, Mon, coefZ) + RO2ET
            CoEZETtrans(Yr, Mon, coefZ) = CoEZETtrans(Yr, Mon, coefZ) + ETtrans
            CoEZNIRmET(Yr, Mon, coefZ) = CoEZNIRmET(Yr, Mon, coefZ) + NIRmET

            !Coefficient Zone Irrigation Totals
            CoEZirrPrecip(Yr, Mon, coefZ, pflag) = CoEZirrPrecip(Yr, Mon, coefZ, pflag) + Precip(cell, Mon) * acs / 12
            CoEZirrAc(Yr, Mon, coefZ, pflag) = CoEZirrAc(Yr, Mon, coefZ, pflag) + acs
            CoEZirrAP(Yr, Mon, coefZ, pflag) = CoEZirrAP(Yr, Mon, coefZ, pflag) + AP
            CoEZirrSW(Yr, Mon, coefZ, pflag) = CoEZirrSW(Yr, Mon, coefZ, pflag) + AppSW
            CoEZirrET(Yr, Mon, coefZ, pflag) = CoEZirrET(Yr, Mon, coefZ, pflag) + ET
            CoEZirrRO(Yr, Mon, coefZ, pflag) = CoEZirrRO(Yr, Mon, coefZ, pflag) + RO
            CoEZirrDP(Yr, Mon, coefZ, pflag) = CoEZirrDP(Yr, Mon, coefZ, pflag) + DP
            CoEZirrSL(Yr, Mon, coefZ, pflag) = CoEZirrSL(Yr, Mon, coefZ, pflag) + SL
            CoEZirrPSL(Yr, Mon, coefZ, pflag) = CoEZirrPSL(Yr, Mon, coefZ, pflag) + PSL
            CoEZirrDAP(Yr, Mon, coefZ, pflag) = CoEZirrDAP(Yr, Mon, coefZ, pflag) + DAP
            CoEZirrETG(Yr, Mon, coefZ, pflag) = CoEZirrETG(Yr, Mon, coefZ, pflag) + ETG
            CoEZirrDET(Yr, Mon, coefZ, pflag) = CoEZirrDET(Yr, Mon, coefZ, pflag) + DET
            CoEZirrETB(Yr, Mon, coefZ, pflag) = CoEZirrETB(Yr, Mon, coefZ, pflag) + ETB
            CoEZirrDP1(Yr, Mon, coefZ, pflag) = CoEZirrDP1(Yr, Mon, coefZ, pflag) + DP1
            CoEZirrDP2(Yr, Mon, coefZ, pflag) = CoEZirrDP2(Yr, Mon, coefZ, pflag) + DP2
            CoEZirrRO1(Yr, Mon, coefZ, pflag) = CoEZirrRO1(Yr, Mon, coefZ, pflag) + RO1
            CoEZirrRO2(Yr, Mon, coefZ, pflag) = CoEZirrRO2(Yr, Mon, coefZ, pflag) + RO2
            CoEZirrEWAT(Yr, Mon, coefZ, pflag) = CoEZirrEWAT(Yr, Mon, coefZ, pflag) + EWat
            CoEZirrSF(Yr, Mon, coefZ, pflag) = CoEZirrSF(Yr, Mon, coefZ, pflag) + SF
            CoEZirrRO2DP(Yr, Mon, coefZ, pflag) = CoEZirrRO2DP(Yr, Mon, coefZ, pflag) + RO2DP 
            CoEZirrRO2ET(Yr, Mon, coefZ, pflag) = CoEZirrRO2ET(Yr, Mon, coefZ, pflag) + RO2ET
            CoEZirrETtrans(Yr, Mon, coefZ, pflag) = CoEZirrETtrans(Yr, Mon, coefZ, pflag) + ETtrans
            CoEZirrNIRmET(Yr, Mon, coefZ, pflag) = CoEZirrNIRmET(Yr, Mon, coefZ, pflag) + NIRmET
        
            !Coefficient Zone Crop Totals
            CoEZcrpPrecip(Yr, Mon, coefZ, crop) = CoEZcrpPrecip(Yr, Mon, coefZ, crop) + Precip(cell, Mon) * acs / 12
            CoEZcrpAc(Yr, Mon, coefZ, crop) = CoEZcrpAc(Yr, Mon, coefZ, crop) + acs
            CoEZcrpAP(Yr, Mon, coefZ, crop) = CoEZcrpAP(Yr, Mon, coefZ, crop) + AP
            CoEZcrpSW(Yr, Mon, coefZ, crop) = CoEZcrpSW(Yr, Mon, coefZ, crop) + AppSW
            CoEZcrpET(Yr, Mon, coefZ, crop) = CoEZcrpET(Yr, Mon, coefZ, crop) + ET
            CoEZcrpRO(Yr, Mon, coefZ, crop) = CoEZcrpRO(Yr, Mon, coefZ, crop) + RO
            CoEZcrpDP(Yr, Mon, coefZ, crop) = CoEZcrpDP(Yr, Mon, coefZ, crop) + DP
            CoEZcrpSL(Yr, Mon, coefZ, crop) = CoEZcrpSL(Yr, Mon, coefZ, crop) + SL
            CoEZcrpPSL(Yr, Mon, coefZ, crop) = CoEZcrpPSL(Yr, Mon, coefZ, crop) + PSL
            CoEZcrpDAP(Yr, Mon, coefZ, crop) = CoEZcrpDAP(Yr, Mon, coefZ, crop) + DAP
            CoEZcrpETG(Yr, Mon, coefZ, crop) = CoEZcrpETG(Yr, Mon, coefZ, crop) + ETG
            CoEZcrpDET(Yr, Mon, coefZ, crop) = CoEZcrpDET(Yr, Mon, coefZ, crop) + DET
            CoEZcrpETB(Yr, Mon, coefZ, crop) = CoEZcrpETB(Yr, Mon, coefZ, crop) + ETB
            CoEZcrpDP1(Yr, Mon, coefZ, crop) = CoEZcrpDP1(Yr, Mon, coefZ, crop) + DP1
            CoEZcrpDP2(Yr, Mon, coefZ, crop) = CoEZcrpDP2(Yr, Mon, coefZ, crop) + DP2
            CoEZcrpRO1(Yr, Mon, coefZ, crop) = CoEZcrpRO1(Yr, Mon, coefZ, crop) + RO1
            CoEZcrpRO2(Yr, Mon, coefZ, crop) = CoEZcrpRO2(Yr, Mon, coefZ, crop) + RO2
            CoEZcrpEWAT(Yr, Mon, coefZ, crop) = CoEZcrpEWAT(Yr, Mon, coefZ, crop) + EWat
            CoEZcrpSF(Yr, Mon, coefZ, crop) = CoEZcrpSF(Yr, Mon, coefZ, crop) + SF
            CoEZcrpRO2DP(Yr, Mon, coefZ, crop) = CoEZcrpRO2DP(Yr, Mon, coefZ, crop) + RO2DP
            CoEZcrpRO2ET(Yr, Mon, coefZ, crop) = CoEZcrpRO2ET(Yr, Mon, coefZ, crop) + RO2ET
            CoEZcrpETtrans(Yr, Mon, coefZ, crop) = CoEZcrpETtrans(Yr, Mon, coefZ, crop) + ETtrans
            CoEZcrpNIRmET(Yr, Mon, coefZ, crop) = CoEZcrpNIRmET(Yr, Mon, coefZ, crop) + NIRmET
        
            !Coefficients Zone Irrigation and Crop Totals
            CoEZICPrecip(Yr, Mon, coefZ, pflag, crop) = CoEZICPrecip(Yr, Mon, coefZ, pflag, crop) + Precip(cell, Mon) * acs / 12
            CoEZICAc(Yr, Mon, coefZ, pflag, crop) = CoEZICAc(Yr, Mon, coefZ, pflag, crop) + acs
            CoEZICAP(Yr, Mon, coefZ, pflag, crop) = CoEZICAP(Yr, Mon, coefZ, pflag, crop) + AP
            CoEZICSW(Yr, Mon, coefZ, pflag, crop) = CoEZICSW(Yr, Mon, coefZ, pflag, crop) + AppSW
            CoEZICET(Yr, Mon, coefZ, pflag, crop) = CoEZICET(Yr, Mon, coefZ, pflag, crop) + ET
            CoEZICRO(Yr, Mon, coefZ, pflag, crop) = CoEZICRO(Yr, Mon, coefZ, pflag, crop) + RO
            CoEZICDP(Yr, Mon, coefZ, pflag, crop) = CoEZICDP(Yr, Mon, coefZ, pflag, crop) + DP
            CoEZICSL(Yr, Mon, coefZ, pflag, crop) = CoEZICSL(Yr, Mon, coefZ, pflag, crop) + SL
            CoEZICPSL(Yr, Mon, coefZ, pflag, crop) = CoEZICPSL(Yr, Mon, coefZ, pflag, crop) + PSL
            CoEZICDAP(Yr, Mon, coefZ, pflag, crop) = CoEZICDAP(Yr, Mon, coefZ, pflag, crop) + DAP
            CoEZICETG(Yr, Mon, coefZ, pflag, crop) = CoEZICETG(Yr, Mon, coefZ, pflag, crop) + ETG
            CoEZICDET(Yr, Mon, coefZ, pflag, crop) = CoEZICDET(Yr, Mon, coefZ, pflag, crop) + DET
            CoEZICETB(Yr, Mon, coefZ, pflag, crop) = CoEZICETB(Yr, Mon, coefZ, pflag, crop) + ETB
            CoEZICDP1(Yr, Mon, coefZ, pflag, crop) = CoEZICDP1(Yr, Mon, coefZ, pflag, crop) + DP1
            CoEZICDP2(Yr, Mon, coefZ, pflag, crop) = CoEZICDP2(Yr, Mon, coefZ, pflag, crop) + DP2
            CoEZICRO1(Yr, Mon, coefZ, pflag, crop) = CoEZICRO1(Yr, Mon, coefZ, pflag, crop) + RO1
            CoEZICRO2(Yr, Mon, coefZ, pflag, crop) = CoEZICRO2(Yr, Mon, coefZ, pflag, crop) + RO2
            CoEZICEWAT(Yr, Mon, coefZ, pflag, crop) = CoEZICEWAT(Yr, Mon, coefZ, pflag, crop) + EWat
            CoEZICSF(Yr, Mon, coefZ, pflag, crop) = CoEZICSF(Yr, Mon, coefZ, pflag, crop) + SF
            CoEZICRO2DP(Yr, Mon, coefZ, pflag, crop) = CoEZICRO2DP(Yr, Mon, coefZ, pflag, crop) + RO2DP
            CoEZICRO2ET(Yr, Mon, coefZ, pflag, crop) = CoEZICRO2ET(Yr, Mon, coefZ, pflag, crop) + RO2ET
            CoEZICETtrans(Yr, Mon, coefZ, pflag, crop) = CoEZICETtrans(Yr, Mon, coefZ, pflag, crop) + ETtrans
            CoEZICNIRmET(Yr, Mon, coefZ, pflag, crop) = CoEZICNIRmET(Yr, Mon, coefZ, pflag, crop) + NIRmET
            
        END DO
        CLOSE (1)
    END DO
    
    OPEN (100, File = TRIM(OUTDIR)//'Regional\Regional_ann_totals.txt', STATUS = 'UNKNOWN')
    OPEN (300, File = TRIM(OUTDIR)//'County\County_ann_totals.txt', STATUS = 'UNKNOWN')
    OPEN (500, File = TRIM(OUTDIR)//'RoZone\RoZone_ann_totals.txt', STATUS = 'UNKNOWN')
    OPEN (700, File = TRIM(OUTDIR)//'CoeffZone\Coeff_ann_totals.txt', STATUS = 'UNKNOWN')
    
    OPEN (101, File = TRIM(OUTDIR)//'Regional\Regional_mon_totals.txt', STATUS = 'UNKNOWN')
    OPEN (301, File = TRIM(OUTDIR)//'County\County_mon_totals.txt', STATUS = 'UNKNOWN')
    OPEN (501, File = TRIM(OUTDIR)//'RoZone\RoZone_mon_totals.txt', STATUS = 'UNKNOWN')
    OPEN (701, File = TRIM(OUTDIR)//'CoeffZone\Coeff_mon_totals.txt', STATUS = 'UNKNOWN')
    
    WRITE(100, 110)
    WRITE(300, 120)
    WRITE(500, 130)
    WRITE(700, 140)
    
    WRITE(101, 110)
    WRITE(301, 120)
    WRITE(501, 130)
    WRITE(701, 140)
    
    DO i = 1, 4
        OPEN (100 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_ann_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (300 + 2 * i, File = TRIM(OUTDIR)//'County\County_ann_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (500 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_ann_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (700 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_ann_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
    
        OPEN (101 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_mon_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (301 + 2 * i, File = TRIM(OUTDIR)//'County\County_mon_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (501 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_mon_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (701 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_mon_'//TRIM(Irrig(i))//'_tot.txt', STATUS = 'UNKNOWN')
        
        WRITE(100 + 2 * i, 112)
        WRITE(300 + 2 * i, 122)
        WRITE(500 + 2 * i, 132)
        WRITE(700 + 2 * i, 142)
    
        WRITE(101 + 2 * i, 112)
        WRITE(301 + 2 * i, 122)
        WRITE(501 + 2 * i, 132)
        WRITE(701 + 2 * i, 142)
    
    END DO
    
    DO i = 1, 12
        OPEN (109 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_ann_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (309 + 2 * i, File = TRIM(OUTDIR)//'County\County_ann_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (509 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_ann_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (709 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_ann_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
    
        OPEN (110 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_mon_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (310 + 2 * i, File = TRIM(OUTDIR)//'County\County_mon_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (510 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_mon_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        OPEN (710 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_mon_'//TRIM(CROPS(i))//'_tot.txt', STATUS = 'UNKNOWN')
        
        WRITE(109 + 2 * i, 114)
        WRITE(309 + 2 * i, 124)
        WRITE(509 + 2 * i, 134)
        WRITE(709 + 2 * i, 144)
    
        WRITE(110 + 2 * i, 114)
        WRITE(310 + 2 * i, 124)
        WRITE(510 + 2 * i, 134)
        WRITE(710 + 2 * i, 144)
    
    END DO
    
    i = 1
    DO j = 1, 4
        DO k = 1, 12
            OPEN (199 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_ann_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (399 + 2 * i, File = TRIM(OUTDIR)//'County\County_ann_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (599 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_ann_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (799 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_ann_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
    
            OPEN (200 + 2 * i, File = TRIM(OUTDIR)//'Regional\Regional_mon_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (400 + 2 * i, File = TRIM(OUTDIR)//'County\County_mon_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (600 + 2 * i, File = TRIM(OUTDIR)//'RoZone\RoZone_mon_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            OPEN (800 + 2 * i, File = TRIM(OUTDIR)//'CoeffZone\Coeff_mon_'//TRIM(Irrig(j))//'_'//TRIM(CROPS(k))//'_tot.txt', STATUS = 'UNKNOWN')
            
            WRITE(199 + 2 * i, 116)
            WRITE(399 + 2 * i, 126)
            WRITE(599 + 2 * i, 136)
            WRITE(799 + 2 * i, 146)
    
            WRITE(200 + 2 * i, 116)
            WRITE(400 + 2 * i, 126)
            WRITE(600 + 2 * i, 136)
            WRITE(800 + 2 * i, 146)
            
            i = i + 1
        END DO
    END DO
!*******************************************************************************
    !Regional Totals
    DO i = StartYr, EndYr
        DO j = 0, 12
            IF (j .EQ. 0) THEN
                WRITE(100, 111) i, j, RegionAc(i, j), RegionPrecip(i, j), RegionAP(i, j), RegionSW(i, j), RegionET(i, j) + RegionSL(i, j) + RegionRO2ET(i, j), &
                                RegionDP(i, j) + RegionRO2DP(i, J), RegionET(i, j), RegionRO(i, j), &
                                RegionDP(i, j), RegionSL(i, j), RegionPSL(i, j), RegionDAP(i, j), RegionETG(i, j), &
                                RegionDET(i, j), RegionETB(i, j), RegionDP1(i, j), RegionDP2(i, j), &
                                RegionRO1(i, j), RegionRO2(i, j), RegionEWat(i, j), &
                                RegionSF(i, j), RegionRO2DP(i, j), RegionRO2ET(i, j), RegionETtrans(i, j), RegionNIRmET(i, j)
            ELSE
                WRITE(101, 111) i, j, RegionAc(i, j), RegionPrecip(i, j), RegionAP(i, j), RegionSW(i, j), RegionET(i, j) + RegionSL(i, j) + RegionRO2ET(i, j), &
                                RegionDP(i, j) + RegionRO2DP(i, J), RegionET(i, j), RegionRO(i, j), &
                                RegionDP(i, j), RegionSL(i, j), RegionPSL(i, j), RegionDAP(i, j), RegionETG(i, j), &
                                RegionDET(i, j), RegionETB(i, j), RegionDP1(i, j), RegionDP2(i, j), &
                                RegionRO1(i, j), RegionRO2(i, j), RegionEWat(i, j), &
                                RegionSF(i, j), RegionRO2DP(i, j), RegionRO2ET(i, j), RegionETtrans(i, j), RegionNIRmET(i, j)
            END IF
        END DO
    END DO
    
    !Regional Irrigation Totals
    DO k = 1, 4
        DO i = StartYr, EndYr
            DO j = 0, 12
                IF (j .EQ. 0) THEN
                    WRITE(100 + 2 * k, 113) i, j, k, RegIrrAc(i, j, k), RegIrrPrecip(i, j, k), RegIrrAP(i, j, k), RegIrrSW(i, j, k), RegIrrET(i, j, k) + RegIrrSL(i, j, k) + RegIrrRO2ET(i, j, k), &
                                    RegIrrDP(i, j, k) + RegIrrRO2DP(i, j, k), RegIrrET(i, j, k), RegIrrRO(i, j, k), &
                                    RegIrrDP(i, j, k), RegIrrSL(i, j, k), RegIrrPSL(i, j, k), RegIrrDAP(i, j, k), RegIrrETG(i, j, k), &
                                    RegIrrDET(i, j, k), RegIrrETB(i, j, k), RegIrrDP1(i, j, k), RegIrrDP2(i, j, k), &
                                    RegIrrRO1(i, j, k), RegIrrRO2(i, j, k), RegIrrEWat(i, j, k), &
                                    RegIrrSF(i, j, k), RegIrrRO2DP(i, j, k), RegIrrRO2ET(i, j, k), RegIrrETtrans(i, j, k), RegIrrNIRmET(i, j, k)
                ELSE
                    WRITE(101 + 2 * k, 113) i, j, k, RegIrrAc(i, j, k), RegIrrPrecip(i, j, k), RegIrrAP(i, j, k), RegIrrSW(i, j, k), RegIrrET(i, j, k) + RegIrrSL(i, j, k) + RegIrrRO2ET(i, j, k), &
                                    RegIrrDP(i, j, k) + RegIrrRO2DP(i, j, k), RegIrrET(i, j, k), RegIrrRO(i, j, k), &
                                    RegIrrDP(i, j, k), RegIrrSL(i, j, k), RegIrrPSL(i, j, k), RegIrrDAP(i, j, k), RegIrrETG(i, j, k), &
                                    RegIrrDET(i, j, k), RegIrrETB(i, j, k), RegIrrDP1(i, j, k), RegIrrDP2(i, j, k), &
                                    RegIrrRO1(i, j, k), RegIrrRO2(i, j, k), RegIrrEWat(i, j, k), &
                                    RegIrrSF(i, j, k), RegIrrRO2DP(i, j, k), RegIrrRO2ET(i, j, k), RegIrrETtrans(i, j, k), RegIrrNIRmET(i, j, k)
                END IF
            END DO
        END DO
    END DO
    
    !Regional Crop Totals
    DO k = 1, 12
        DO i = StartYr, EndYr
            DO j = 0, 12
                IF (j .EQ. 0) THEN
                    WRITE(109 + 2 * k, 115) i, j, k, RegCropAc(i, j, k), RegCropPrecip(i, j, k), RegCropAP(i, j, k), RegCropSW(i, j, k), RegCropET(i, j, k) + RegCropSL(i, j, k) + RegCropRO2ET(i, j, k), &
                                    RegCropDP(i, j, k) + RegCropRO2DP(i, j, k), RegCropET(i, j, k), RegCropRO(i, j, k), &
                                    RegCropDP(i, j, k), RegCropSL(i, j, k), RegCropPSL(i, j, k), RegCropDAP(i, j, k), RegCropETG(i, j, k), &
                                    RegCropDET(i, j, k), RegCropETB(i, j, k), RegCropDP1(i, j, k), RegCropDP2(i, j, k), &
                                    RegCropRO1(i, j, k), RegCropRO2(i, j, k), RegCropEWat(i, j, k), &
                                    RegCropSF(i, j, k), RegCropRO2DP(i, j, k), RegCropRO2ET(i, j, k), RegCropETtrans(i, j, k), RegCropNIRmET(i, j, k)
                ELSE
                    WRITE(110 + 2 * k, 115) i, j, k, RegCropAc(i, j, k), RegCropPrecip(i, j, k), RegCropAP(i, j, k), RegCropSW(i, j, k), RegCropET(i, j, k) + RegCropSL(i, j, k) + RegCropRO2ET(i, j, k), &
                                    RegCropDP(i, j, k) + RegCropRO2DP(i, j, k), RegCropET(i, j, k), RegCropRO(i, j, k), &
                                    RegCropDP(i, j, k), RegCropSL(i, j, k), RegCropPSL(i, j, k), RegCropDAP(i, j, k), RegCropETG(i, j, k), &
                                    RegCropDET(i, j, k), RegCropETB(i, j, k), RegCropDP1(i, j, k), RegCropDP2(i, j, k), &
                                    RegCropRO1(i, j, k), RegCropRO2(i, j, k), RegCropEWat(i, j, k), &
                                    RegCropSF(i, j, k), RegCropRO2DP(i, j, k), RegCropRO2ET(i, j, k), RegCropETtrans(i, j, k), RegCropNIRmET(i, j, k)
                END IF
            END DO
        END DO
    END DO
            
    !Regional Irrigation and Crop Totals
    m = 1
    DO l = 1, 4
        DO k = 1, 12
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(199 + 2 * m, 117) i, j, l, k, RICAc(i, j, l, k), RICPrecip(i, j, l, k), RICAP(i, j, l, k), RICSW(i, j, l, k), RICET(i, j, l, k) + RICSL(i, j, l, k) + RICRO2ET(i, j, l, k), &
                                        RICDP(i, j, l, k) + RICRO2DP(i, j, l, k), RICET(i, j, l, k), RICRO(i, j, l, k), &
                                        RICDP(i, j, l, k), RICSL(i, j, l, k), RICPSL(i, j, l, k), RICDAP(i, j, l, k), RICETG(i, j, l, k), &
                                        RICDET(i, j, l, k), RICETB(i, j, l, k), RICDP1(i, j, l, k), RICDP2(i, j, l, k), &
                                        RICRO1(i, j, l, k), RICRO2(i, j, l, k), RICEWat(i, j, l, k), &
                                        RICSF(i, j, l, k), RICRO2DP(i, j, l, k), RICRO2ET(i, j, l, k), RICETtrans(i, j, l, k), RICNIRmET(i, j, l, k)
                    ELSE
                        WRITE(200 + 2 * m, 117) i, j, l, k, RICAc(i, j, l, k), RICPrecip(i, j, l, k), RICAP(i, j, l, k), RICSW(i, j, l, k), RICET(i, j, l, k) + RICSL(i, j, l, k) + RICRO2ET(i, j, l, k), &
                                        RICDP(i, j, l, k) + RICRO2DP(i, j, l, k), RICET(i, j, l, k), RICRO(i, j, l, k), &
                                        RICDP(i, j, l, k), RICSL(i, j, l, k), RICPSL(i, j, l, k), RICDAP(i, j, l, k), RICETG(i, j, l, k), &
                                        RICDET(i, j, l, k), RICETB(i, j, l, k), RICDP1(i, j, l, k), RICDP2(i, j, l, k), &
                                        RICRO1(i, j, l, k), RICRO2(i, j, l, k), RICEWat(i, j, l, k), &
                                        RICSF(i, j, l, k), RICRO2DP(i, j, l, k), RICRO2ET(i, j, l, k), RICETtrans(i, j, l, k), RICNIRmET(i, j, l, k)
                    END IF
                END DO
            END DO
            m = m + 1
        END DO
    END DO
!*******************************************************************************    
    !County Totals
    DO h = 0, 93
        IF (CntyFlag(h) .EQ. 1) THEN
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(300, 121) i, j, h, nCounty(h), cntyAc(i, j, h), cntyPrecip(i, j, h), cntyAP(i, j, h), cntySW(i, j, h), cntyET(i, j, h) + cntySL(i, j, h) + cntyRO2ET(i, j, h), &
                                        cntyDP(i, j, h) + cntyRO2DP(i, j, h), cntyET(i, j, h), cntyRO(i, j, h), &
                                        cntyDP(i, j, h), cntySL(i, j, h), cntyPSL(i, j, h), cntyDAP(i, j, h), cntyETG(i, j, h), &
                                        cntyDET(i, j, h), cntyETB(i, j, h), cntyDP1(i, j, h), cntyDP2(i, j, h), &
                                        cntyRO1(i, j, h), cntyRO2(i, j, h), cntyEWat(i, j, h), &
                                        cntySF(i, j, h), cntyRO2DP(i, j, h), cntyRO2ET(i, j, h), cntyETtrans(i, j, h), cntyNIRmET(i, j, h)
                    ELSE
                        WRITE(301, 121) i, j, h, nCounty(h), cntyAc(i, j, h), cntyPrecip(i, j, h), cntyAP(i, j, h), cntySW(i, j, h), cntyET(i, j, h) + cntySL(i, j, h) + cntyRO2ET(i, j, h), &
                                        cntyDP(i, j, h) + cntyRO2DP(i, j, h), cntyET(i, j, h), cntyRO(i, j, h), &
                                        cntyDP(i, j, h), cntySL(i, j, h), cntyPSL(i, j, h), cntyDAP(i, j, h), cntyETG(i, j, h), &
                                        cntyDET(i, j, h), cntyETB(i, j, h), cntyDP1(i, j, h), cntyDP2(i, j, h), &
                                        cntyRO1(i, j, h), cntyRO2(i, j, h), cntyEWat(i, j, h), &
                                        cntySF(i, j, h), cntyRO2DP(i, j, h), cntyRO2ET(i, j, h), cntyETtrans(i, j, h), cntyNIRmET(i, j, h)
                    END IF
                END DO
            END DO
        END IF
    END DO
    
    
    !County Irrigation Totals
    DO k = 1, 4
        DO h = 0, 93
            IF (CntyFlag(h) .EQ. 1) THEN
                DO i = StartYr, EndYr
                    DO j = 0, 12
                        IF (j .EQ. 0) THEN
                            WRITE(300 + 2 * k, 123) i, j, h, nCounty(h), k, cntyirrAc(i, j, h, k), cntyirrPrecip(i, j, h, k), cntyirrAP(i, j, h, k), cntyirrSW(i, j, h, k), cntyirrET(i, j, h, k) + cntyirrSL(i, j, h, k) + cntyirrRO2ET(i, j, h, k), &
                                            cntyirrDP(i, j, h, k) + cntyirrRO2DP(i, j, h, k), cntyirrET(i, j, h, k), cntyirrRO(i, j, h, k), &
                                            cntyirrDP(i, j, h, k), cntyirrSL(i, j, h, k), cntyirrPSL(i, j, h, k), cntyirrDAP(i, j, h, k), cntyirrETG(i, j, h, k), &
                                            cntyirrDET(i, j, h, k), cntyirrETB(i, j, h, k), cntyirrDP1(i, j, h, k), cntyirrDP2(i, j, h, k), &
                                            cntyirrRO1(i, j, h, k), cntyirrRO2(i, j, h, k), cntyirrEWat(i, j, h, k), &
                                            cntyirrSF(i, j, h, k), cntyirrRO2DP(i, j, h, k), cntyirrRO2ET(i, j, h, k), cntyirrETtrans(i, j, h, k), cntyirrNIRmET(i, j, h, k)
                        ELSE
                            WRITE(301 + 2 * k, 123) i, j, h, nCounty(h), k, cntyirrAc(i, j, h, k), cntyirrPrecip(i, j, h, k), cntyirrAP(i, j, h, k), cntyirrSW(i, j, h, k), cntyirrET(i, j, h, k) + cntyirrSL(i, j, h, k) + cntyirrRO2ET(i, j, h, k), &
                                            cntyirrDP(i, j, h, k) + cntyirrRO2DP(i, j, h, k), cntyirrET(i, j, h, k), cntyirrRO(i, j, h, k), &
                                            cntyirrDP(i, j, h, k), cntyirrSL(i, j, h, k), cntyirrPSL(i, j, h, k), cntyirrDAP(i, j, h, k), cntyirrETG(i, j, h, k), &
                                            cntyirrDET(i, j, h, k), cntyirrETB(i, j, h, k), cntyirrDP1(i, j, h, k), cntyirrDP2(i, j, h, k), &
                                            cntyirrRO1(i, j, h, k), cntyirrRO2(i, j, h, k), cntyirrEWat(i, j, h, k), &
                                            cntyirrSF(i, j, h, k), cntyirrRO2DP(i, j, h, k), cntyirrRO2ET(i, j, h, k), cntyirrETtrans(i, j, h, k), cntyirrNIRmET(i, j, h, k)
                        END IF
                    END DO
                END DO
            END IF
        END DO
    END DO
    
    !County Crop Totals
    DO k = 1, 12 
        DO h = 0, 93
            IF (CntyFlag(h) .EQ. 1) THEN
                DO i = StartYr, EndYr
                    DO j = 0, 12
                        IF (j .EQ. 0) THEN
                            WRITE(309 + 2 * k, 125) i, j, h, nCounty(h), k, cntycrpAc(i, j, h, k), cntycrpPrecip(i, j, h, k), cntycrpAP(i, j, h, k), cntycrpSW(i, j, h, k), cntycrpET(i, j, h, k) + cntycrpSL(i, j, h, k) + cntycrpRO2ET(i, j, h, k), &
                                            cntycrpDP(i, j, h, k) + cntycrpRO2DP(i, j, h, k), cntycrpET(i, j, h, k), cntycrpRO(i, j, h, k), &
                                            cntycrpDP(i, j, h, k), cntycrpSL(i, j, h, k), cntycrpPSL(i, j, h, k), cntycrpDAP(i, j, h, k), cntycrpETG(i, j, h, k), &
                                            cntycrpDET(i, j, h, k), cntycrpETB(i, j, h, k), cntycrpDP1(i, j, h, k), cntycrpDP2(i, j, h, k), &
                                            cntycrpRO1(i, j, h, k), cntycrpRO2(i, j, h, k), cntycrpEWat(i, j, h, k), &
                                            cntycrpSF(i, j, h, k), cntycrpRO2DP(i, j, h, k), cntycrpRO2ET(i, j, h, k), cntycrpETtrans(i, j, h, k), cntycrpNIRmET(i, j, h, k)
                        ELSE
                            WRITE(310 + 2 * k, 125) i, j, h, nCounty(h), k, cntycrpAc(i, j, h, k), cntycrpPrecip(i, j, h, k), cntycrpAP(i, j, h, k), cntycrpSW(i, j, h, k), cntycrpET(i, j, h, k) + cntycrpSL(i, j, h, k) + cntycrpRO2ET(i, j, h, k), &
                                            cntycrpDP(i, j, h, k) + cntycrpRO2DP(i, j, h, k), cntycrpET(i, j, h, k), cntycrpRO(i, j, h, k), &
                                            cntycrpDP(i, j, h, k), cntycrpSL(i, j, h, k), cntycrpPSL(i, j, h, k), cntycrpDAP(i, j, h, k), cntycrpETG(i, j, h, k), &
                                            cntycrpDET(i, j, h, k), cntycrpETB(i, j, h, k), cntycrpDP1(i, j, h, k), cntycrpDP2(i, j, h, k), &
                                            cntycrpRO1(i, j, h, k), cntycrpRO2(i, j, h, k), cntycrpEWat(i, j, h, k), &
                                            cntycrpSF(i, j, h, k), cntycrpRO2DP(i, j, h, k), cntycrpRO2ET(i, j, h, k), cntycrpETtrans(i, j, h, k), cntycrpNIRmET(i, j, h, k)
                        END IF
                    END DO
                END DO
            END IF
        END DO
    END DO
    
    !County Irrigation and Crop Totals
    m = 1
    DO l = 1, 4
        DO k = 1, 12
            DO h = 0, 93
                IF (CntyFlag(h) .EQ. 1) THEN
                    DO i = StartYr, EndYr
                        DO j = 0, 12
                            IF (j .EQ. 0) THEN
                                WRITE(399 + 2 * m, 127) i, j, h, nCounty(h), l, k, CICAc(i, j, h, l, k), CICPrecip(i, j, h, l, k), CICAP(i, j, h, l, k), CICSW(i, j, h, l, k), CICET(i, j, h, l, k) + CICSL(i, j, h, l, k) + CICRO2ET(i, j, h, l, k), &
                                                CICDP(i, j, h, l, k) + CICRO2DP(i, j, h, l , k), CICET(i, j, h, l, k), CICRO(i, j, h, l, k), &
                                                CICDP(i, j, h, l, k), CICSL(i, j, h, l, k), CICPSL(i, j, h, l, k), CICDAP(i, j, h, l, k), CICETG(i, j, h, l, k), &
                                                CICDET(i, j, h, l, k), CICETB(i, j, h, l, k), CICDP1(i, j, h, l, k), CICDP2(i, j, h, l, k), &
                                                CICRO1(i, j, h, l, k), CICRO2(i, j, h, l, k), CICEWat(i, j, h, l, k), &
                                                CICSF(i, j, h, l, k), CICRO2DP(i, j, h, l, k), CICRO2ET(i, j, h, l, k), CICETtrans(i, j, h, l, k), CICNIRmET(i, j, h, l, k)
                            ELSE
                                WRITE(400 + 2 * m, 127) i, j, h, nCounty(h), l, k, CICAc(i, j, h, l, k), CICPrecip(i, j, h, l, k), CICAP(i, j, h, l, k), CICSW(i, j, h, l, k), CICET(i, j, h, l, k) + CICSL(i, j, h, l, k) + CICRO2ET(i, j, h, l, k), &
                                                CICDP(i, j, h, l, k) + CICRO2DP(i, j, h, l , k), CICET(i, j, h, l, k), CICRO(i, j, h, l, k), &
                                                CICDP(i, j, h, l, k), CICSL(i, j, h, l, k), CICPSL(i, j, h, l, k), CICDAP(i, j, h, l, k), CICETG(i, j, h, l, k), &
                                                CICDET(i, j, h, l, k), CICETB(i, j, h, l, k), CICDP1(i, j, h, l, k), CICDP2(i, j, h, l, k), &
                                                CICRO1(i, j, h, l, k), CICRO2(i, j, h, l, k), CICEWat(i, j, h, l, k), &
                                                CICSF(i, j, h, l, k), CICRO2DP(i, j, h, l, k), CICRO2ET(i, j, h, l, k), CICETtrans(i, j, h, l, k), CICNIRmET(i, j, h, l, k)
                            END IF
                        END DO
                    END DO
                END IF
            END DO
            m = m + 1
        END DO
    END DO
!*******************************************************************************
    !Runoff Zone Totals
    DO h = 0, rozones
        DO i = StartYr, EndYr
            DO j = 0, 12
                IF (j .EQ. 0) THEN
                    WRITE(500, 131) i, j, h, ROZAc(i, j, h), ROZPrecip(i, j, h), ROZAP(i, j, h), ROZSW(i, j, h), ROZET(i, j, h) + ROZSL(i, j, h) + ROZRO2ET(i, j, h), &
                                    ROZDP(i, j, h) + ROZRO2DP(i, j, h), ROZET(i, j, h), ROZRO(i, j, h), &
                                    ROZDP(i, j, h), ROZSL(i, j, h), ROZPSL(i, j, h), ROZDAP(i, j, h), ROZETG(i, j, h), &
                                    ROZDET(i, j, h), ROZETB(i, j, h), ROZDP1(i, j, h), ROZDP2(i, j, h), &
                                    ROZRO1(i, j, h), ROZRO2(i, j, h), ROZEWat(i, j, h), &
                                    ROZSF(i, j, h), ROZRO2DP(i, j, h), ROZRO2ET(i, j, h), ROZETtrans(i, j, h), ROZNIRmET(i, j, h)
                ELSE
                    WRITE(501, 131) i, j, h, ROZAc(i, j, h), ROZPrecip(i, j, h), ROZAP(i, j, h), ROZSW(i, j, h), ROZET(i, j, h) + ROZSL(i, j, h) + ROZRO2ET(i, j, h), &
                                    ROZDP(i, j, h) + ROZRO2DP(i, j, h), ROZET(i, j, h), ROZRO(i, j, h), &
                                    ROZDP(i, j, h), ROZSL(i, j, h), ROZPSL(i, j, h), ROZDAP(i, j, h), ROZETG(i, j, h), &
                                    ROZDET(i, j, h), ROZETB(i, j, h), ROZDP1(i, j, h), ROZDP2(i, j, h), &
                                    ROZRO1(i, j, h), ROZRO2(i, j, h), ROZEWat(i, j, h), &
                                    ROZSF(i, j, h), ROZRO2DP(i, j, h), ROZRO2ET(i, j, h), ROZETtrans(i, j, h), ROZNIRmET(i, j, h)
                END IF
            END DO
        END DO
    END DO
    
    !Runoff Zone Irrigation Type Totals
    DO k = 1, 4
        DO h = 0, rozones
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(500 + 2 * k, 133) i, j, h, k, ROZirrAc(i, j, h, k), ROZirrPrecip(i, j, h, k), ROZirrAP(i, j, h, k), ROZirrSW(i, j, h, k), ROZirrET(i, j, h, k) + ROZirrSL(i, j, h, k) + ROZirrRO2ET(i, j, h, k), &
                                        ROZirrDP(i, j, h, k) + ROZirrRO2DP(i, j, h, k), ROZirrET(i, j, h, k), ROZirrRO(i, j, h, k), &
                                        ROZirrDP(i, j, h, k), ROZirrSL(i, j, h, k), ROZirrPSL(i, j, h, k), ROZirrDAP(i, j, h, k), ROZirrETG(i, j, h, k), &
                                        ROZirrDET(i, j, h, k), ROZirrETB(i, j, h, k), ROZirrDP1(i, j, h, k), ROZirrDP2(i, j, h, k), &
                                        ROZirrRO1(i, j, h, k), ROZirrRO2(i, j, h, k), ROZirrEWat(i, j, h, k), &
                                        ROZirrSF(i, j, h, k), ROZirrRO2DP(i, j, h, k), ROZirrRO2ET(i, j, h, k), ROZirrETtrans(i, j, h, k), ROZirrNIRmET(i, j, h, k)
                    ELSE
                        WRITE(501 + 2 * k, 133) i, j, h, k, ROZirrAc(i, j, h, k), ROZirrPrecip(i, j, h, k), ROZirrAP(i, j, h, k), ROZirrSW(i, j, h, k), ROZirrET(i, j, h, k) + ROZirrSL(i, j, h, k) + ROZirrRO2ET(i, j, h, k), &
                                        ROZirrDP(i, j, h, k) + ROZirrRO2DP(i, j, h, k), ROZirrET(i, j, h, k), ROZirrRO(i, j, h, k), &
                                        ROZirrDP(i, j, h, k), ROZirrSL(i, j, h, k), ROZirrPSL(i, j, h, k), ROZirrDAP(i, j, h, k), ROZirrETG(i, j, h, k), &
                                        ROZirrDET(i, j, h, k), ROZirrETB(i, j, h, k), ROZirrDP1(i, j, h, k), ROZirrDP2(i, j, h, k), &
                                        ROZirrRO1(i, j, h, k), ROZirrRO2(i, j, h, k), ROZirrEWat(i, j, h, k), &
                                        ROZirrSF(i, j, h, k), ROZirrRO2DP(i, j, h, k), ROZirrRO2ET(i, j, h, k), ROZirrETtrans(i, j, h, k), ROZirrNIRmET(i, j, h, k)
                    END IF
                END DO
            END DO
        END DO
    END DO
    
    !Runoff Zone Crop Totals
    DO k = 1, 12
        DO h = 0, rozones
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(509 + 2 * k, 135) i, j, h, k, ROZcrpAc(i, j, h, k), ROZcrpPrecip(i, j, h, k), ROZcrpAP(i, j, h, k), ROZcrpSW(i, j, h, k), ROZcrpET(i, j, h, k) + ROZcrpSL(i, j, h, k) + ROZcrpRO2ET(i, j, h, k), &
                                        ROZcrpDP(i, j, h, k) + ROZcrpRO2DP(i, j, h, k), ROZcrpET(i, j, h, k), ROZcrpRO(i, j, h, k), &
                                        ROZcrpDP(i, j, h, k), ROZcrpSL(i, j, h, k), ROZcrpPSL(i, j, h, k), ROZcrpDAP(i, j, h, k), ROZcrpETG(i, j, h, k), &
                                        ROZcrpDET(i, j, h, k), ROZcrpETB(i, j, h, k), ROZcrpDP1(i, j, h, k), ROZcrpDP2(i, j, h, k), &
                                        ROZcrpRO1(i, j, h, k), ROZcrpRO2(i, j, h, k), ROZcrpEWat(i, j, h, k), &
                                        ROZcrpSF(i, j, h, k), ROZcrpRO2DP(i, j, h, k), ROZcrpRO2ET(i, j, h, k), ROZcrpETtrans(i, j, h, k), ROZcrpNIRmET(i, j, h, k)
                    ELSE
                        WRITE(510 + 2 * k, 135) i, j, h, k, ROZcrpAc(i, j, h, k), ROZcrpPrecip(i, j, h, k), ROZcrpAP(i, j, h, k), ROZcrpSW(i, j, h, k), ROZcrpET(i, j, h, k) + ROZcrpSL(i, j, h, k) + ROZcrpRO2ET(i, j, h, k), &
                                        ROZcrpDP(i, j, h, k) + ROZcrpRO2DP(i, j, h, k), ROZcrpET(i, j, h, k), ROZcrpRO(i, j, h, k), &
                                        ROZcrpDP(i, j, h, k), ROZcrpSL(i, j, h, k), ROZcrpPSL(i, j, h, k), ROZcrpDAP(i, j, h, k), ROZcrpETG(i, j, h, k), &
                                        ROZcrpDET(i, j, h, k), ROZcrpETB(i, j, h, k), ROZcrpDP1(i, j, h, k), ROZcrpDP2(i, j, h, k), &
                                        ROZcrpRO1(i, j, h, k), ROZcrpRO2(i, j, h, k), ROZcrpEWat(i, j, h, k), &
                                        ROZcrpSF(i, j, h, k), ROZcrpRO2DP(i, j, h, k), ROZcrpRO2ET(i, j, h, k), ROZcrpETtrans(i, j, h, k), ROZcrpNIRmET(i, j, h, k)
                    END IF
                END DO
            END DO
        END DO
    END DO
            
    !Runoff Zone Irrigation type and Crop Totals
    m = 1
    DO l = 1, 4
        DO k = 1, 12
            DO h = 0, rozones
                DO i = StartYr, EndYr
                    DO j = 0, 12
                        IF (j .EQ. 0) THEN
                            WRITE(599 + 2 * m, 137) i, j, h, l, k, ROZICAc(i, j, h, l, k), ROZICPrecip(i, j, h, l, k), ROZICAP(i, j, h, l, k), ROZICSW(i, j, h, l, k), ROZICET(i, j, h, l, k) + ROZICSL(i, j, h, l, k) + ROZICRO2ET(i, j, h, l, k), & 
                                            ROZICDP(i, j, h, l, k) + ROZICRO2DP(i, j, h, l, k), ROZICET(i, j, h, l, k), ROZICRO(i, j, h, l, k), &
                                            ROZICDP(i, j, h, l, k), ROZICSL(i, j, h, l, k), ROZICPSL(i, j, h, l, k), ROZICDAP(i, j, h, l, k), ROZICETG(i, j, h, l, k), &
                                            ROZICDET(i, j, h, l, k), ROZICETB(i, j, h, l, k), ROZICDP1(i, j, h, l, k), ROZICDP2(i, j, h, l, k), &
                                            ROZICRO1(i, j, h, l, k), ROZICRO2(i, j, h, l, k), ROZICEWat(i, j, h, l, k), &
                                            ROZICSF(i, j, h, l, k), ROZICRO2DP(i, j, h, l, k), ROZICRO2ET(i, j, h, l, k), ROZICETtrans(i, j, h, l, k), ROZICNIRmET(i, j, h, l, k)
                        ELSE
                            WRITE(600 + 2 * m, 137) i, j, h, l, k, ROZICAc(i, j, h, l, k), ROZICPrecip(i, j, h, l, k), ROZICAP(i, j, h, l, k), ROZICSW(i, j, h, l, k), ROZICET(i, j, h, l, k) + ROZICSL(i, j, h, l, k) + ROZICRO2ET(i, j, h, l, k), & 
                                            ROZICDP(i, j, h, l, k) + ROZICRO2DP(i, j, h, l, k), ROZICET(i, j, h, l, k), ROZICRO(i, j, h, l, k), &
                                            ROZICDP(i, j, h, l, k), ROZICSL(i, j, h, l, k), ROZICPSL(i, j, h, l, k), ROZICDAP(i, j, h, l, k), ROZICETG(i, j, h, l, k), &
                                            ROZICDET(i, j, h, l, k), ROZICETB(i, j, h, l, k), ROZICDP1(i, j, h, l, k), ROZICDP2(i, j, h, l, k), &
                                            ROZICRO1(i, j, h, l, k), ROZICRO2(i, j, h, l, k), ROZICEWat(i, j, h, l, k), &
                                            ROZICSF(i, j, h, l, k), ROZICRO2DP(i, j, h, l, k), ROZICRO2ET(i, j, h, l, k), ROZICETtrans(i, j, h, l, k), ROZICNIRmET(i, j, h, l, k)
                        END IF
                    END DO
                END DO
            END DO
            m = m + 1
        END DO
    END DO
!*******************************************************************************
    !Coefficient Zone Totals
    DO h = 0, Cozones
        DO i = StartYr, EndYr
            DO j = 0, 12
                IF (j .EQ. 0) THEN
                    WRITE(700, 141) i, j, h, CoEZAc(i, j, h), CoEZPrecip(i, j, h), CoEZAP(i, j, h), CoEZSW(i, j, h), CoEZET(i, j, h) + CoEZSL(i, j, h) + CoEZRO2ET(i, j, h), &
                                    CoEZDP(i, j, h) + CoEZRO2DP(i, j, h), CoEZET(i, j, h), CoEZRO(i, j, h), &
                                    CoEZDP(i, j, h), CoEZSL(i, j, h), CoEZPSL(i, j, h), CoEZDAP(i, j, h), CoEZETG(i, j, h), &
                                    CoEZDET(i, j, h), CoEZETB(i, j, h), CoEZDP1(i, j, h), CoEZDP2(i, j, h), &
                                    CoEZRO1(i, j, h), CoEZRO2(i, j, h), CoEZEWat(i, j, h), &
                                    CoEZSF(i, j, h), CoEZRO2DP(i, j, h), CoEZRO2ET(i, j, h), CoEZETtrans(i, j, h), CoEZNIRmET(i, j, h)
                ELSE
                    WRITE(171, 141) i, j, h, CoEZAc(i, j, h), CoEZPrecip(i, j, h), CoEZAP(i, j, h), CoEZSW(i, j, h), CoEZET(i, j, h) + CoEZSL(i, j, h) + CoEZRO2ET(i, j, h), &
                                    CoEZDP(i, j, h) + CoEZRO2DP(i, j, h), CoEZET(i, j, h), CoEZRO(i, j, h), &
                                    CoEZDP(i, j, h), CoEZSL(i, j, h), CoEZPSL(i, j, h), CoEZDAP(i, j, h), CoEZETG(i, j, h), &
                                    CoEZDET(i, j, h), CoEZETB(i, j, h), CoEZDP1(i, j, h), CoEZDP2(i, j, h), &
                                    CoEZRO1(i, j, h), CoEZRO2(i, j, h), CoEZEWat(i, j, h), &
                                    CoEZSF(i, j, h), CoEZRO2DP(i, j, h), CoEZRO2ET(i, j, h), CoEZETtrans(i, j, h), CoEZNIRmET(i, j, h)
                END IF
            END DO
        END DO
    END DO
    
    !Coefficient Zone Irrigation Totals
    DO k = 1, 4
        DO h = 0, Cozones
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(700 + 2 * k, 143) i, j, h, k, CoEZirrAc(i, j, h, k), CoEZirrPrecip(i, j, h, k), CoEZirrAP(i, j, h, k), CoEZirrSW(i, j, h, k), CoEZirrET(i, j, h, k) + CoEZirrSL(i, j, h, k) + CoEZirrRO2ET(i, j, h, k), &
                                        CoEZirrDP(i, j, h, k) + CoEZirrRO2DP(i, j, h, k), CoEZirrET(i, j, h, k), CoEZirrRO(i, j, h, k), &
                                        CoEZirrDP(i, j, h, k), CoEZirrSL(i, j, h, k), CoEZirrPSL(i, j, h, k), CoEZirrDAP(i, j, h, k), CoEZirrETG(i, j, h, k), &
                                        CoEZirrDET(i, j, h, k), CoEZirrETB(i, j, h, k), CoEZirrDP1(i, j, h, k), CoEZirrDP2(i, j, h, k), &
                                        CoEZirrRO1(i, j, h, k), CoEZirrRO2(i, j, h, k), CoEZirrEWat(i, j, h, k), &
                                        CoEZirrSF(i, j, h, k), CoEZirrRO2DP(i, j, h, k), CoEZirrRO2ET(i, j, h, k), CoEZirrETtrans(i, j, h, k), CoEZirrNIRmET(i, j, h, k)
                    ELSE
                        WRITE(701 + 2 * k, 143) i, j, h, k, CoEZirrAc(i, j, h, k), CoEZirrPrecip(i, j, h, k), CoEZirrAP(i, j, h, k), CoEZirrSW(i, j, h, k), CoEZirrET(i, j, h, k) + CoEZirrSL(i, j, h, k) + CoEZirrRO2ET(i, j, h, k), &
                                        CoEZirrDP(i, j, h, k) + CoEZirrRO2DP(i, j, h, k), CoEZirrET(i, j, h, k), CoEZirrRO(i, j, h, k), &
                                        CoEZirrDP(i, j, h, k), CoEZirrSL(i, j, h, k), CoEZirrPSL(i, j, h, k), CoEZirrDAP(i, j, h, k), CoEZirrETG(i, j, h, k), &
                                        CoEZirrDET(i, j, h, k), CoEZirrETB(i, j, h, k), CoEZirrDP1(i, j, h, k), CoEZirrDP2(i, j, h, k), &
                                        CoEZirrRO1(i, j, h, k), CoEZirrRO2(i, j, h, k), CoEZirrEWat(i, j, h, k), &
                                        CoEZirrSF(i, j, h, k), CoEZirrRO2DP(i, j, h, k), CoEZirrRO2ET(i, j, h, k), CoEZirrETtrans(i, j, h, k), CoEZirrNIRmET(i, j, h, k)
                    END IF
                END DO
            END DO
        END DO
    END DO
    
    !Coefficient Zone Crop Totals
    DO k = 1, 12
        DO h = 0, Cozones
            DO i = StartYr, EndYr
                DO j = 0, 12
                    IF (j .EQ. 0) THEN
                        WRITE(709 + 2 * k, 145) i, j, h, k, CoEZcrpAc(i, j, h, k), CoEZcrpPrecip(i, j, h, k), CoEZcrpAP(i, j, h, k), CoEZcrpSW(i, j, h, k), CoEZcrpET(i, j, h, k) + CoEZcrpSL(i, j, h, k) + CoEZcrpRO2ET(i, j, h, k), &
                                        CoEZcrpDP(i, j, h, k) + CoEZcrpRO2DP(i, j, h, k), CoEZcrpET(i, j, h, k), CoEZcrpRO(i, j, h, k), &
                                        CoEZcrpDP(i, j, h, k), CoEZcrpSL(i, j, h, k), CoEZcrpPSL(i, j, h, k), CoEZcrpDAP(i, j, h, k), CoEZcrpETG(i, j, h, k), &
                                        CoEZcrpDET(i, j, h, k), CoEZcrpETB(i, j, h, k), CoEZcrpDP1(i, j, h, k), CoEZcrpDP2(i, j, h, k), &
                                        CoEZcrpRO1(i, j, h, k), CoEZcrpRO2(i, j, h, k), CoEZcrpEWat(i, j, h, k), &
                                        CoEZcrpSF(i, j, h, k), CoEZcrpRO2DP(i, j, h, k), CoEZcrpRO2ET(i, j, h, k), CoEZcrpETtrans(i, j, h, k), CoEZcrpNIRmET(i, j, h, k)
                    ELSE
                        WRITE(710 + 2 * k, 145) i, j, h, k, CoEZcrpAc(i, j, h, k), CoEZcrpPrecip(i, j, h, k), CoEZcrpAP(i, j, h, k), CoEZcrpSW(i, j, h, k), CoEZcrpET(i, j, h, k) + CoEZcrpSL(i, j, h, k) + CoEZcrpRO2ET(i, j, h, k), &
                                        CoEZcrpDP(i, j, h, k) + CoEZcrpRO2DP(i, j, h, k), CoEZcrpET(i, j, h, k), CoEZcrpRO(i, j, h, k), &
                                        CoEZcrpDP(i, j, h, k), CoEZcrpSL(i, j, h, k), CoEZcrpPSL(i, j, h, k), CoEZcrpDAP(i, j, h, k), CoEZcrpETG(i, j, h, k), &
                                        CoEZcrpDET(i, j, h, k), CoEZcrpETB(i, j, h, k), CoEZcrpDP1(i, j, h, k), CoEZcrpDP2(i, j, h, k), &
                                        CoEZcrpRO1(i, j, h, k), CoEZcrpRO2(i, j, h, k), CoEZcrpEWat(i, j, h, k), &
                                        CoEZcrpSF(i, j, h, k), CoEZcrpRO2DP(i, j, h, k), CoEZcrpRO2ET(i, j, h, k), CoEZcrpETtrans(i, j, h, k), CoEZcrpNIRmET(i, j, h, k)
                    END IF
                END DO
            END DO
        END DO
    END DO
            
    !Regional Irrigation and Crop Totals
    m = 1
    DO l = 1, 4
        DO k = 1, 12
            DO h = 0, Cozones
                DO i = StartYr, EndYr
                    DO j = 0, 12
                        IF (j .EQ. 0) THEN
                            WRITE(799 + 2 * m, 147) i, j, h, l, k, CoEZICAc(i, j, h, l, k), CoEZICPrecip(i, j, h, l, k), CoEZICAP(i, j, h, l, k), CoEZICSW(i, j, h, l, k), CoEZICET(i, j, h, l, k) + CoEZICSL(i, j, h, l, k) + CoEZICRO2ET(i, j, h, l, k), & 
                                            CoEZICDP(i, j, h, l, k) + CoEZICRO2DP(i, j, h, l, k), CoEZICET(i, j, h, l, k), CoEZICRO(i, j, h, l, k), &
                                            CoEZICDP(i, j, h, l, k), CoEZICSL(i, j, h, l, k), CoEZICPSL(i, j, h, l, k), CoEZICDAP(i, j, h, l, k), CoEZICETG(i, j, h, l, k), &
                                            CoEZICDET(i, j, h, l, k), CoEZICETB(i, j, h, l, k), CoEZICDP1(i, j, h, l, k), CoEZICDP2(i, j, h, l, k), &
                                            CoEZICRO1(i, j, h, l, k), CoEZICRO2(i, j, h, l, k), CoEZICEWat(i, j, h, l, k), &
                                            CoEZICSF(i, j, h, l, k), CoEZICRO2DP(i, j, h, l, k), CoEZICRO2ET(i, j, h, l, k), CoEZICETtrans(i, j, h, l, k), CoEZICNIRmET(i, j, h, l, k)
                        ELSE
                            WRITE(800 + 2 * m, 147) i, j, h, l, k, CoEZICAc(i, j, h, l, k), CoEZICPrecip(i, j, h, l, k), CoEZICAP(i, j, h, l, k), CoEZICSW(i, j, h, l, k), CoEZICET(i, j, h, l, k) + CoEZICSL(i, j, h, l, k) + CoEZICRO2ET(i, j, h, l, k), & 
                                            CoEZICDP(i, j, h, l, k) + CoEZICRO2DP(i, j, h, l, k), CoEZICET(i, j, h, l, k), CoEZICRO(i, j, h, l, k), &
                                            CoEZICDP(i, j, h, l, k), CoEZICSL(i, j, h, l, k), CoEZICPSL(i, j, h, l, k), CoEZICDAP(i, j, h, l, k), CoEZICETG(i, j, h, l, k), &
                                            CoEZICDET(i, j, h, l, k), CoEZICETB(i, j, h, l, k), CoEZICDP1(i, j, h, l, k), CoEZICDP2(i, j, h, l, k), &
                                            CoEZICRO1(i, j, h, l, k), CoEZICRO2(i, j, h, l, k), CoEZICEWat(i, j, h, l, k), &
                                            CoEZICSF(i, j, h, l, k), CoEZICRO2DP(i, j, h, l, k), CoEZICRO2ET(i, j, h, l, k), CoEZICETtrans(i, j, h, l, k), CoEZICNIRmET(i, j, h, l, k)
                        END IF
                    END DO
                END DO
            END DO
            m = m + 1
        END DO
    END DO
!*******************************************************************************

100 FORMAT (I7, 2X, I5, 2(2X, I3), 2X, I2, 2(2X, I3), 2X, F6.2, 18(2X, F14.8))

110 FORMAT ('Year, Month, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
111 FORMAT (I5, ", ", I2, 25(", ", F9.0))
112 FORMAT ('Year, Month, Irr_Type, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')  
113 FORMAT (I5, ", ", I2, ", ", I2, 25(", ", F9.0))
114 FORMAT ('Year, Month, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET') 
115 FORMAT (I5, ", ", I2, ", ", I3, 25(", ", F9.0))
116 FORMAT ('Year, Month, Irr_Type, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET') 
117 FORMAT (I5, ", ", I2, ", ", I2, ", ", I3, 25(", ", F9.0))
    
120 FORMAT ('Year, Month, County, Countyn, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
121 FORMAT (I5, ", ", I2, ", ", I3, ", ", A25, 25(", ", F9.0))
122 FORMAT ('Year, Month, County, Countyn, Irr_Type, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
123 FORMAT (I5, ", ", I2, ", ", I3, ", ", A25, ", ", I2, 25(", ", F9.0))
124 FORMAT ('Year, Month, County, Countyn, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
125 FORMAT (I5, ", ", I2, ", ", I3, ", ", A25, ", ", I3, 25(", ", F9.0))
126 FORMAT ('Year, Month, County, Countyn, Irr_Type, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
127 FORMAT (I5, ", ", I2, ", ", I3, ", ", A25, ", ", I2, ", ", I3, 25(", ", F9.0))

130 FORMAT ('Year, Month, RO_Zone, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
131 FORMAT (I5, ", ", I2, ", ", I3, 25(", ", F9.0))
132 FORMAT ('Year, Month, RO_Zone, Irr_Type, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
133 FORMAT (I5, ", ", I2, ", ", I3, ", ", I2, 25(", ", F9.0))
134 FORMAT ('Year, Month, RO_Zone, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
135 FORMAT (I5, ", ", I2, ", ", I3, ", ", I3, 25(", ", F9.0))
136 FORMAT ('Year, Month, RO_Zone, Irr_Type, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
137 FORMAT (I5, ", ", I2, ", ", I3, ", ", I2, ", ", I3, 25(", ", F9.0))

140 FORMAT ('Year, Month, Coeff_Zone, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
141 FORMAT (I5, ", ", I2, ", ", I3, 25(", ", F9.0))
142 FORMAT ('Year, Month, Coeff_Zone, Irr_Type, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
143 FORMAT (I5, ", ", I2, ", ", I3, ", ", I2, 25(", ", F9.0))
144 FORMAT ('Year, Month, Coeff_Zone, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
145 FORMAT (I5, ", ", I2, ", ", I3, ", ", I3, 25(", ", F9.0))
146 FORMAT ('Year, Month, Coeff_Zone, Irr_Type, crop, Acres, Precip, GW_Pumping, SW_Del, ET, DP, ET_dir, RO_dir, DP_dir, SL, Post_SL, DAP, ETGain, DET, ETbase, DP1, ' &
            'DP2, RO1, RO2, EWat, SF, RO_to_DP, RO_to_ET, ET_Transfer, NIR_minus_ET')
147 FORMAT (I5, ", ", I2, ", ", I3, ", ", I2, ", ", I3, 25(", ", F9.0))
    
    
END PROGRAM WSPP_Report
!************************************************************************

SUBROUTINE init
    USE PARAM
    IMPLICIT NONE
    
    ! Regional Totals
    RegionPrecip = 0.
    RegionAc = 0.
    RegionAP = 0.
    RegionSW = 0.
    RegionET = 0.
    RegionRO = 0.
    RegionDP = 0.
    RegionSL = 0.
    RegionPSL = 0.
    RegionDAP = 0.
    RegionETG = 0.
    RegionDET = 0.
    RegionETB = 0.
    RegionDP1 = 0.
    RegionDP2 = 0.
    RegionRO1 = 0.
    RegionRO2 = 0.
    RegionEWAT = 0.
    RegionSF = 0.
    RegionRO2DP = 0.
    RegionRO2ET = 0.
    RegionETTrans = 0.
    RegionNIRmET = 0.

    ! Regional Irrigation Totals
    RegIrrPrecip = 0.
    RegIrrAc = 0.
    RegIrrAP = 0.
    RegIrrSW = 0.
    RegIrrET = 0.
    RegIrrRO = 0.
    RegIrrDP = 0.
    RegIrrSL = 0.
    RegIrrPSL = 0.
    RegIrrDAP = 0.
    RegIrrETG = 0.
    RegIrrDET = 0.
    RegIrrETB = 0.
    RegIrrDP1 = 0.
    RegIrrDP2 = 0.
    RegIrrRO1 = 0.
    RegIrrRO2 = 0.
    RegIrrEWAT = 0.
    RegIrrSF = 0.
    RegIrrRO2DP = 0.
    RegIrrRO2ET = 0.
    RegIrrETTrans = 0.
    RegIrrNIRmET = 0.

    ! Regional Crop Totals
    RegCropPrecip = 0.
    RegCropAc = 0.
    RegCropAP = 0.
    RegCropSW = 0.
    RegCropET = 0.
    RegCropRO = 0.
    RegCropDP = 0.
    RegCropSL = 0.
    RegCropPSL = 0.
    RegCropDAP = 0.
    RegCropETG = 0.
    RegCropDET = 0.
    RegCropETB = 0.
    RegCropDP1 = 0.
    RegCropDP2 = 0.
    RegCropRO1 = 0.
    RegCropRO2 = 0.
    RegCropEWAT = 0.
    RegCropSF = 0.
    RegCropRO2DP = 0.
    RegCropRO2ET = 0.
    RegCropETTrans = 0.
    RegCropNIRmET = 0.
    
    !Regional crop and irrigation Totals
    RICPrecip = 0.
    RICAc = 0.
    RICAP = 0.
    RICSW = 0.
    RICET = 0.
    RICRO = 0.
    RICDP = 0.
    RICSL = 0.
    RICPSL = 0.
    RICDAP = 0.
    RICETG = 0.
    RICDET = 0.
    RICETB = 0.
    RICDP1 = 0.
    RICDP2 = 0.
    RICRO1 = 0.
    RICRO2 = 0.
    RICEWAT = 0.
    RICSF = 0.
    RICRO2DP = 0.
    RICRO2ET = 0.
    RICETTrans = 0.
    RICNIRmET = 0.
    
    ! County Totals
    cntyPrecip = 0.
    cntyAc = 0.
    cntyAP = 0.
    cntySW = 0.
    cntyET = 0.
    cntyRO = 0.
    cntyDP = 0.
    cntySL = 0.
    cntyPSL = 0.
    cntyDAP = 0.
    cntyETG = 0.
    cntyDET = 0.
    cntyETB = 0.
    cntyDP1 = 0.
    cntyDP2 = 0.
    cntyRO1 = 0.
    cntyRO2 = 0.
    cntyEWAT = 0.
    cntySF = 0.
    cntyRO2DP = 0.
    cntyRO2ET = 0.
    cntyETTrans = 0.
    cntyNIRmET = 0.
    
    ! County Irrigation Totals
    cntyirrPrecip = 0.
    cntyirrAc = 0.
    cntyirrAP = 0.
    cntyirrSW = 0.
    cntyirrET = 0.
    cntyirrRO = 0.
    cntyirrDP = 0.
    cntyirrSL = 0.
    cntyirrPSL = 0.
    cntyirrDAP = 0.
    cntyirrETG = 0.
    cntyirrDET = 0.
    cntyirrETB = 0.
    cntyirrDP1 = 0.
    cntyirrDP2 = 0.
    cntyirrRO1 = 0.
    cntyirrRO2 = 0.
    cntyirrEWAT = 0.
    cntyirrSF = 0.
    cntyirrRO2DP = 0.
    cntyirrRO2ET = 0.
    cntyirrETTrans = 0.
    cntyirrNIRmET = 0.
    
    ! County Crop Totals
    cntycrpPrecip = 0.
    cntycrpAc = 0.
    cntycrpAP = 0.
    cntycrpSW = 0.
    cntycrpET = 0.
    cntycrpRO = 0.
    cntycrpDP = 0.
    cntycrpSL = 0.
    cntycrpPSL = 0.
    cntycrpDAP = 0.
    cntycrpETG = 0.
    cntycrpDET = 0.
    cntycrpETB = 0.
    cntycrpDP1 = 0.
    cntycrpDP2 = 0.
    cntycrpRO1 = 0.
    cntycrpRO2 = 0.
    cntycrpEWAT = 0.
    cntycrpSF = 0.
    cntycrpRO2DP = 0.
    cntycrpRO2ET = 0.
    cntycrpETTrans = 0.
    cntycrpNIRmET = 0.
    
    ! County Irrigation and Crop Totals
    CICPrecip = 0.
    CICAc = 0.
    CICAP = 0.
    CICSW = 0.
    CICET = 0.
    CICRO = 0.
    CICDP = 0.
    CICSL = 0.
    CICPSL = 0.
    CICDAP = 0.
    CICETG = 0.
    CICDET = 0.
    CICETB = 0.
    CICDP1 = 0.
    CICDP2 = 0.
    CICRO1 = 0.
    CICRO2 = 0.
    CICEWAT = 0.
    CICSF = 0.
    CICRO2DP = 0.
    CICRO2ET = 0.
    CICETTrans = 0.
    CICNIRmET = 0.
    
    !Runoff Zone totals
    ROZPrecip = 0.
    ROZAc = 0.
    ROZAP = 0.
    ROZSW = 0.
    ROZET = 0.
    ROZRO = 0.
    ROZDP = 0.
    ROZSL = 0.
    ROZPSL = 0.
    ROZDAP = 0.
    ROZETG = 0.
    ROZDET = 0.
    ROZETB = 0.
    ROZDP1 = 0.
    ROZDP2 = 0.
    ROZRO1 = 0.
    ROZRO2 = 0.
    ROZEWAT = 0.
    ROZSF = 0.
    ROZRO2DP = 0.
    ROZRO2ET = 0.
    ROZETTrans = 0.
    ROZNIRmET = 0.
    
    !Runoff Zone Irrigation totals
    ROZirrPrecip = 0.
    ROZirrAc = 0.
    ROZirrAP = 0.
    ROZirrSW = 0.
    ROZirrET = 0.
    ROZirrRO = 0.
    ROZirrDP = 0.
    ROZirrSL = 0.
    ROZirrPSL = 0.
    ROZirrDAP = 0.
    ROZirrETG = 0.
    ROZirrDET = 0.
    ROZirrETB = 0.
    ROZirrDP1 = 0.
    ROZirrDP2 = 0.
    ROZirrRO1 = 0.
    ROZirrRO2 = 0.
    ROZirrEWAT = 0.
    ROZirrSF = 0.
    ROZirrRO2DP = 0.
    ROZirrRO2ET = 0.
    ROZirrETTrans = 0.
    ROZirrNIRmET = 0.
    
    !Runoff Zone Crop Totals
    ROZcrpPrecip = 0.
    ROZcrpAc = 0.
    ROZcrpAP = 0.
    ROZcrpSW = 0.
    ROZcrpET = 0.
    ROZcrpRO = 0.
    ROZcrpDP = 0.
    ROZcrpSL = 0.
    ROZcrpPSL = 0.
    ROZcrpDAP = 0.
    ROZcrpETG = 0.
    ROZcrpDET = 0.
    ROZcrpETB = 0.
    ROZcrpDP1 = 0.
    ROZcrpDP2 = 0.
    ROZcrpRO1 = 0.
    ROZcrpRO2 = 0.
    ROZcrpEWAT = 0.
    ROZcrpSF = 0.
    ROZcrpRO2DP = 0.
    ROZcrpRO2ET = 0.
    ROZcrpETTrans = 0.
    ROZcrpNIRmET = 0.
    
    !Runoff Zone Irrigation and crop Totals
    ROZICPrecip = 0.
    ROZICAc = 0.
    ROZICAP = 0.
    ROZICSW = 0.
    ROZICET = 0.
    ROZICRO = 0.
    ROZICDP = 0.
    ROZICSL = 0.
    ROZICPSL = 0.
    ROZICDAP = 0.
    ROZICETG = 0.
    ROZICDET = 0.
    ROZICETB = 0.
    ROZICDP1 = 0.
    ROZICDP2 = 0.
    ROZICRO1 = 0.
    ROZICRO2 = 0.
    ROZICEWAT = 0.
    ROZICSF = 0.
    ROZICRO2DP = 0.
    ROZICRO2ET = 0.
    ROZICETTrans = 0.
    ROZICNIRmET = 0.
    
    !Coefficient Zone Totals
    CoEZPrecip = 0.
    CoEZAc = 0.
    CoEZAP = 0.
    CoEZSW = 0.
    CoEZET = 0.
    CoEZRO = 0.
    CoEZDP = 0.
    CoEZSL = 0.
    CoEZPSL = 0.
    CoEZDAP = 0.
    CoEZETG = 0.
    CoEZDET = 0.
    CoEZETB = 0.
    CoEZDP1 = 0.
    CoEZDP2 = 0.
    CoEZRO1 = 0.
    CoEZRO2 = 0.
    CoEZEWAT = 0.
    CoEZSF = 0.
    CoEZRO2DP = 0.
    CoEZRO2ET = 0.
    CoEZETTrans = 0.
    CoEZNIRmET = 0.
    
    !Coefficient Zone Irrgation Totals
    CoEZirrPrecip = 0.
    CoEZirrAc = 0.
    CoEZirrAP = 0.
    CoEZirrSW = 0.
    CoEZirrET = 0.
    CoEZirrRO = 0.
    CoEZirrDP = 0.
    CoEZirrSL = 0.
    CoEZirrPSL = 0.
    CoEZirrDAP = 0.
    CoEZirrETG = 0.
    CoEZirrDET = 0.
    CoEZirrETB = 0.
    CoEZirrDP1 = 0.
    CoEZirrDP2 = 0.
    CoEZirrRO1 = 0.
    CoEZirrRO2 = 0.
    CoEZirrEWAT = 0.
    CoEZirrSF = 0.
    CoEZirrRO2DP = 0.
    CoEZirrRO2ET = 0.
    CoEZirrETTrans = 0.
    CoEZirrNIRmET = 0.
    
    !Coefficient Zone Crop Totals
    CoEZcrpPrecip = 0.
    CoEZcrpAc = 0.
    CoEZcrpAP = 0.
    CoEZcrpSW = 0.
    CoEZcrpET = 0.
    CoEZcrpRO = 0.
    CoEZcrpDP = 0.
    CoEZcrpSL = 0.
    CoEZcrpPSL = 0.
    CoEZcrpDAP = 0.
    CoEZcrpETG = 0.
    CoEZcrpDET = 0.
    CoEZcrpETB = 0.
    CoEZcrpDP1 = 0.
    CoEZcrpDP2 = 0.
    CoEZcrpRO1 = 0.
    CoEZcrpRO2 = 0.
    CoEZcrpEWAT = 0.
    CoEZcrpSF = 0.
    CoEZcrpRO2DP = 0.
    CoEZcrpRO2ET = 0.
    CoEZcrpETTrans = 0.
    CoEZcrpNIRmET = 0.
    
    !Coefficient Zone Irrigaiton and Crop Totals
    CoEZICPrecip = 0.
    CoEZICAc = 0.
    CoEZICAP = 0.
    CoEZICSW = 0.
    CoEZICET = 0.
    CoEZICRO = 0.
    CoEZICDP = 0.
    CoEZICSL = 0.
    CoEZICPSL = 0.
    CoEZICDAP = 0.
    CoEZICETG = 0.
    CoEZICDET = 0.
    CoEZICETB = 0.
    CoEZICDP1 = 0.
    CoEZICDP2 = 0.
    CoEZICRO1 = 0.
    CoEZICRO2 = 0.
    CoEZICEWAT = 0.
    CoEZICSF = 0.
    CoEZICRO2DP = 0.
    CoEZICRO2ET = 0.
    CoEZICETTrans = 0.
    CoEZICNIRmET = 0.
    
END SUBROUTINE init
!********************************************************************
SUBROUTINE YRTXT(CalcYr, YRtx)

!   The Subroutine YRTXT covert a four digit Integer into four separate	
!   one character strings and then concatinate them into a four 
!   character string.

	USE Param
    
    CHARACTER*4 YRtx
    INTEGER CalcYr
    INTEGER N1, N2, N3, N4

	N1 = (CalcYr-MOD(CalcYr,1000))/1000
	N2 = (CalcYr-MOD(CalcYr,100))/100-10*N1
	N3 = (MOD(CalcYr,100)-MOD(CalcYr,10))/10
	N4 = MOD(CalcYr,10)

	YRtx = CHAR(N1+48)//CHAR(N2+48)//CHAR(N3+48)//CHAR(N4+48)

END SUBROUTINE YRTXT
!********************************************************************