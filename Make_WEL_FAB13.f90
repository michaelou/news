!********************************************************************************
!
!  Make_WEL  - Program creates .WEL file for input into ground water model
!
!  Initial Development:  Feb 2008
!	By:  Marc Groff - TFG
!
!	  Revised:	5 Oct 2010
!	  Subject:	Revised to work with new DNR dataset and 1/2 mile input grid
!
!     Revised:  October 2011
!     Subject:  Converted to monthly values - IM
!********************************************************************************
!   
!   ActiveCellCol           The column location of a given cell (cell)
!   ActiveCellRow           The row location of a given cell (cell)
!   Cell                    The cell being simulated
!   CellMI                  Monthly Municipal and Industrial pumping (month)
!   CellPump                Monthly Irrigation pumping values (month)
!   DaysInMonth             The number days in a given month (month)
!   EndYr                   The last year being simulated
!   Header                  Dummy varialbe to read header rows
!   INDIR                   The directory location of the input files
!   IrrCellPumpAF           Irrigation pumping values (cell, year, month)
!   IrrPumpYr               Year value holder for irrigation pumping
!   LinesPerStressPeriod    Number of months that have pumping values in a cell (Year, Month)
!   MuniCellPumpAF          Municipal and Industrial pumping values (cell, year, month)
!   ncell                   number of cells being simulated
!   ncol                    number of columns in the grid
!   nrow                    number of rows in the grid
!   OUTDIR                  The directory location of the output files
!   ProcessMo               Month counter
!   ProcessYr               Year counter
!   StartYr                 The first year being simulated
!   SwcYr                   The year where the results switch from monthly to annual
!
!********************************************************************************
MODULE PARAM

    INTEGER, PARAMETER :: startyr = 2013
    INTEGER, PARAMETER :: endyr = 2041
    INTEGER, PARAMETER :: ncell = 49725
    
    DATA    MISP /0/
    DATA    Muni /1/
    DATA    SwcYr /2012/
        
    DATA    Layer /1/
    DATA    ncols /255/
    DATA    csize /640/
    
    DATA OUTDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\'/
    DATA INDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\'/
    DATA MIDIR /'W:\CNEB\CNEB\MI\'/
    DATA MISCDIR /'W:\CNEB\CNEB\MISC\'/
    
    !DATA OUTDIR2 /'A:\DNR\CNEB\CNEB\Run006np_new\Results_Ann\'/
    
    CHARACTER*4     YRT, mYRT
    CHARACTER*100   OUTDIR, pmpfile, INDIR, MIDIR, MISCDIR
    CHARACTER*20    MIfold, MIfile, MISCfold, MISCfile
    
    INTEGER     LPSP(12), DaysInMonth(12)
    INTEGER     Cell, Yr, iyear, MLPSP
    INTEGER     startmo, endmo, col, row, ncols
    INTEGER     i, n
    INTEGER     MISP, SwcYr, DIY, csize
    
    REAL    CellPump(0 : 12), IrrCellp(ncell, 0 : 12), MuniCellp(ncell, 0 : 12), MiscCellp(ncell, 0 : 12)
    REAL    pvol(0 : 12), Totp, TPy

    DATA    DaysInMonth /30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4/

END MODULE PARAM

!********************************************************************************
PROGRAM UNW_Make_WEL

    USE PARAM
    IMPLICIT NONE
    
    OPEN (10, FILE = TRIM(OUTDIR)//'Pmp_values.txt', STATUS = 'UNKNOWN')
    WRITE (10, 100)
    
    DO iyear = startyr, endyr
        WRITE (6, *) iyear
        WRITE (YRT, '(I4)') iyear
        
        IF (iyear .LT. 2012) THEN
            WRITE (mYRT, '(I4)') iyear
        ELSE
            WRITE (mYRT, '(I4)') 2012
        END IF
    
        
        IF (MOD(iyear, 4) .EQ. 0) THEN
            DaysInMonth(2) = 30.4
            DIY = 365
        ELSE
            DaysInMonth(2) = 30.4
            DIY = 365
        END IF
        
        startmo = 1
        endmo = 12
        
        IrrCellp = 0.
        !Read in Annual Irrigation Pumping from WSPP
        OPEN (1, FILE = TRIM(OUTDIR)//'pump\pump'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
        DO WHILE (.NOT. EOF(1))
            READ (1, *) Cell, Yr, CellPump(:)
            
            IF ((Yr .LT. StartYr) .OR. (Yr .GT. Endyr)) GOTO 13
            IrrCellp (Cell, :) = CellPump(:)
13      END DO
        CLOSE (1)
        
        MuniCellp = 0.
        IF (Muni .EQ. 1) THEN
            OPEN (13, FILE = TRIM(INDIR)//'MIpump.txt', STATUS = 'OLD')
            DO WHILE (.NOT. EOF(13))
                READ (13, *) MIfold, MIFile
                
                OPEN (14, FILE = TRIM(MIDIR)//TRIM(MIfold)//'\'//TRIM(MIFile)//'2012.txt', STATUS = 'OLD')
                READ (14, *)
                DO WHILE (.NOT. EOF(14))
                    READ (14, *) cell, yr, pvol(:)
                    MuniCellp(cell, :) = MuniCellp(cell, :) + pvol(:)
                END DO
                Close(14)
            END DO
            CLOSE(13)
        END IF
        
        MiscCellp = 0.
        IF (MISP .EQ. 1) THEN
        !Read in Annual Miscellaneou Pumping
            OPEN (13, FILE = TRIM(INDIR)//'Pumping\Pumpwell.txt', STATUS = 'OLD')
            DO WHILE (.NOT. EOF(13))
                READ (13, *) pmpfile
            
                OPEN (14, FILE = TRIM(MISCDIR)//'Miscpmp\'//TRIM(pmpfile)//TRIM(mYRT)//'.txt', STATUS = 'OLD')
                READ (14, *)
                DO WHILE (.NOT. EOF(14))
                    READ (14, *) cell, yr, pvol(:)
                    MiscCellp(Cell, :) = pvol(:)
                END DO
                CLOSE (14)
            END DO
            CLOSE(13)
        END IF
        
        LPSP = 0
        DO i = 1, ncell
            DO n = 1, 12
                IF (iyear .LT. SwcYr) THEN
                    IF (IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n) .GT. 0.) THEN
                        LPSP(1) = LPSP(1) + 1
                        GOTO 14
                    END IF
                ELSE
                    IF (IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n) .GT. 0.) LPSP(n) = LPSP(n) + 1
                END IF
            END DO
14      END DO
        
        MLPSP = MAXVAL(LPSP)
        
        OPEN (20, FILE = TRIM(OUTDIR)//'WEL\CNEB'//TRIM(YRT)//'.WEL', STATUS = 'UNKNOWN')
        WRITE (20, 120) MLPSP
        
        OPEN (30, FILE = TRIM(OUTDIR)//'Well_chk\Well_chk'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        WRITE (30, 130)
        
        IF (iyear .LT. SwcYr) THEN
            WRITE (20, 120) LPSP(1)
            DO i = 1, ncell
                col = MOD(i, ncols)
                IF (col .EQ. 0) col = ncols
                
                row = (i - col) / ncols + 1
                
                Totp = SUM(IrrCellp(i, 1 : 12)) + SUM(MiscCellp(i, 1 : 12)) + SUM(MuniCellp(i, 1 : 12))
                WRITE (10, 101) i, iyear, MAX(Totp, 0.) / csize *12
                
                IF (Totp .GT. 0.) THEN
                    WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DIY)
                    WRITE (30, 131) Cell, Layer, row, col, iyear, MISP * 0, DIY, Totp / DIY, Totp * 43560 / DIY
                END IF
            END DO
        ELSE
            DO n = StartMo, EndMo
            
                WRITE (20, 120) LPSP(n)
                DO i = 1, ncell
                
                    col = MOD(i, ncols)
                    IF (col .EQ. 0) col = ncols
                    
                    row = (i - col) / ncols + 1
                
                    Totp = IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n)
                
                    IF (Totp .GT. 0.) THEN
                        WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DaysInMonth(n))
                    
                        WRITE (30, 131) Cell, Layer, row, col, iyear, n, DaysInMonth(n), Totp / DaysInMonth(n), &
                                            Totp * 43560 / DaysInMonth(n)
                    END IF
                    
                    IF (n .EQ. 1) THEN
                        TPy = SUM(IrrCellp(i, 1 : 12)) + SUM(MiscCellp(i, 1 : 12)) + SUM(MuniCellp(i, 1 : 12))
                        WRITE (10, 101) i, iyear, MAX(TPy, 0.) / csize * 12
                    END IF
                
                END DO
            END DO
        END IF
        
        CLOSE (20)
        CLOSE (30)
        
    END DO

!199 FORMAT('# MODFLOW 2000 Well Package')
!200 FORMAT('PARAMETER  0  0')
!201 FORMAT(2I10) !Header for WEL file (File 20)
120 FORMAT(2I10)	                !Lines in the stress period (File 20)
121	FORMAT(3I10, F10.2)             !Layer, Row, Col, Pumpage FT^3/Day in stress period (File 20)
130	FORMAT ('TFGSeqNum, Layer, QtrCellRow, QtrCellCol, Year, Month, NumberOfDaysInPeriod, QtrCell Pumpage (AF/Day), QtrCell Pumpage (FT^3/Day)')            !Header (File 30)
131	FORMAT (I5, ', ', I1, ', ', 2(I3, ', '), I4, ', ', 2(I3, ', '), F12.7, ', ', F12.2)         !Output for WEL_Check.txt (File 30)
100 FORMAT ('Cell, Year, Pumping_AF')
101 FORMAT (I6, ',', I5, ',', F6.2)

    WRITE(6,*) 'Program Successfully Completed'

END PROGRAM UNW_Make_WEL
!*********************************************************************************
