!  Compile_Wel.f90 
!
!  FUNCTIONS:
!  Compile_Wel - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Compile_Wel
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

PROGRAM Compile_Wel

    IMPLICIT NONE
   
    INTEGER, PARAMETER :: startyr = 2013
    INTEGER, PARAMETER :: endyr = 2039
   
    DATA OUTDIR /'A:\DNR\CNEB\1000-TFG\CNEB\FAB2013_STA001\Results\'/
    
    DATA SwcYr /1986, 2000/
   
    CHARACTER*4     YRT
    CHARACTER*100   OUTDIR
    CHARACTER*50    Line
   
    INTEGER     iyear, MLSP(startyr : endyr), SwcYr(2), flag40, icase
    INTEGER     MLPSP1, MLPSP2, MLPSP3
   
    REAL    pump
    
!    flag40 = 1
    
    DO iyear = startyr, endyr
        WRITE(YRT, '(I4)') iyear
        OPEN (iyear, FILE = TRIM(OUTDIR)//'WEL\CNEB'//TRIM(YRT)//'.WEL', STATUS = 'OLD')
        READ (iyear, *) MLSP(iyear)
    END DO
    
!    MLPSP1 = MAXVAL(MLSP(startyr : SwcYr(1) - 1))
!    MLPSP2 = MAXVAL(MLSP(SwcYr(1) : SwcYr(2) -1))
!    MLPSP3 = MAXVAL(MLSP(SwcYr(2) : endyr))
    MLPSP3 = MAXVAL(MLSP(startyr : endyr))
    
!    OPEN (1, FILE = TRIM(OUTDIR)//'CNEB40.WEL', STATUS = 'UNKNOWN')
!    WRITE (1, 100)
!    WRITE (1, 101)
!    WRITE (1, 102) MLPSP1, 40
    
!    OPEN (2, FILE = TRIM(OUTDIR)//'CNEB86.WEL', STATUS = 'UNKNOWN')
!    WRITE (2, 100)
!    WRITE (2, 101)
!    WRITE (2, 102) MLPSP2, 40
    
    OPEN (3, FILE = TRIM(OUTDIR)//'CNEB_FAB13_Static001.WEL', STATUS = 'UNKNOWN')
    WRITE (3, 100)
    WRITE (3, 101)
    WRITE (3, 102) MLPSP3, 40
    
    DO iyear = startyr, endyr
1       WRITE (6, *) iyear
        
        IF (iyear .LT. SwcYr(1)) THEN
            icase = 1
        ELSE IF ((iyear .GE. SwcYr(1)) .AND. (iyear .LT. SwcYr(2))) THEN
            icase = 2
        ELSE 
            icase = 3
        END IF
        icase = 3
        
        
        DO WHILE (.NOT. EOF(iyear))
            READ (iyear, 103) Line
            WRITE (icase, 103) Line
        END DO
        
    END DO
    
    CLOSE (1)
    CLOSE (2)
    CLOSE (3)
    
    DO iyear = startyr, endyr
        CLOSE (iyear)
    END DO
    
    
100 FORMAT ('#MODFLOW2000 Recharge Package')
101 FORMAT ('PARAMETER  0  0')
102 FORMAT (2I10)
103 FORMAT (A50)
    
END PROGRAM Compile_Wel

