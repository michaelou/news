!*****************************************************************************
!
!    
!*****************************************************************************
MODULE PARAM

!   Input Variables
    INTEGER, PARAMETER ::   EndYr = 2041
    INTEGER, PARAMETER ::   ncell = 49725
    INTEGER, PARAMETER ::   ncols = 255
    INTEGER, PARAMETER ::   nrows = 195
    INTEGER, PARAMETER ::   StartYr = 2013
    INTEGER, PARAMETER ::   rzone = 37
    INTEGER, PARAMETER ::   nzone = 5
    
    DATA csize /640/
    DATA SwcYr /1986/
    
!   Is there Canal Recharge Data 1.Yes 0.No
    DATA CnlRch /0/
    
!   Is there Misc Recharge Data 1. Yes 0.No
    DATA MscRch /0/
    
    DATA INDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\'/
	DATA OUTDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\'/
    DATA CNLDIR /'W:\CNEB\CNEB\Canal\'/
    
    CHARACTER*100   INDIR, OUTDIR, CNLDIR
    CHARACTER*4     YRT
    CHARACTER*12    rchfile
    
    INTEGER     fcol, pcol, cell, CoefZone(ncell), ROZone(ncell), zone, m
    INTEGER     iyear, startmon, endmon, yr, mon
    INTEGER     zz, irow, icol, linenum, k, SwcYr, DIY
    
    REAL    MiToGauge(ncell), LPM(0 : rzone), DryETadj(nzone), IrrETadj(nzone), NIRadjFactor(nzone)
    REAL    AEgadj(nzone), Fslgw(nzone), DryETtoRO(nzone), AEsadj(nzone), Fslsw(nzone), PertoRchg(nzone)
    
    REAL    IrrPrecRch(ncell, 0 : 12), IrrPrecRO(ncell, 0 : 12), CellRCH(nrows, ncols)
    REAL    DP(0 : 12), RO(0 : 12), Canalrch(ncell, 0 : 12), cnlr(0 : 12)
    REAL    Miscrch(ncell, 0 : 12), mrch(0 : 12), DaysInMonth(12)

    DATA DaysInMonth /30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4/
    
END MODULE PARAM

!*****************************************************************************
PROGRAM Make_RCH
    
    USE PARAM
    IMPLICIT NONE
    
    fcol = ncols / 10
    pcol = MOD(ncols, 10)
    
    !Populate Cells with Location Variables
    WRITE (6,*) 'READING CELL LOCATION'
    OPEN (8, FILE = TRIM(INDIR)//'Cellloc.txt', STATUS = 'OLD')
    READ(8, *)
    DO WHILE (.NOT. EOF(8))
        READ (8, *) Cell, CoefZone(Cell), ROZone(cell), MiToGauge(cell)
    END DO
15  CLOSE (8)
    
    !Populate Runoff Zone Coefs
	WRITE (6, *) 'Populating Runoff Zone Coefs'
	OPEN(9, File = TRIM(INDIR)//'ROZoneCoef.txt', STATUS = 'OLD')
	DO WHILE (.NOT. EOF(9))
		READ (9, *) Zone, LPM(Zone)
	END DO  
	Close (9)

    !LOAD CELL COEFFICIENTS
    WRITE(6, *) 'Reading Coefficient File Information'
    OPEN (11, FILE = TRIM(INDIR)//'CoefTest.txt', STATUS='UNKNOWN')
    READ (11, *)
    DO WHILE (.NOT. EOF(11))
        READ (11, *) m, DryETAdj(m), IrrETadj(m), NIRadjFactor(m), AEgadj(m), Fslgw(m), &
                            DryETtoRO(m), AEsadj(m), Fslsw(m), PertoRchg(m)
    END DO 
    CLOSE (11)
    
    OPEN (10, FILE = TRIM(OUTDIR)//'Rch_values.txt', STATUS = 'UNKNOWN')
    WRITE (10, 100)
    
    DO iyear = startyr, endyr
        WRITE (YRT, '(I4)') iyear
        WRITE (6, *) iyear
        
        IF (iyear .LT. SwcYr) THEN
            startmon = 1
            endmon = 1
        ELSE
            startmon = 1
            endmon = 12
        END IF
        
        IF ( MOD(iyear, 4) .EQ. 0) THEN
            DaysInMonth(2) = 30.4
            DIY = 365
        ELSE
            DaysInMonth(2) = 30.4
            DIY = 365
        END IF
        
        IrrPrecRch = 0.
        IrrPrecRO = 0.
        
        OPEN (1, FILE = TRIM(OUTDIR)//'WSPP_OUT\WSPPOut'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
        DO WHILE (.NOT. EOF(1))
            READ (1, *) Cell, yr, DP(:), RO(:)
            IrrPrecRch(Cell, :) = IrrPrecRch(Cell, :) + DP(:)
            IrrPrecRO(Cell, :) = IrrPrecRO(Cell, :) + RO(:)
        END DO
        CLOSE(1)
        
        canalrch = 0.
        IF (CnlRch .EQ. 1) THEN
            OPEN (2, FILE = TRIM(INDIR)//'Stella\CanalRchg\RchFiles.txt.', STATUS = 'OLD')
            DO WHILE (.NOT. EOF(2))
                READ (2, *) Rchfile
                
                OPEN (1, FILE = TRIM(CNLDIR)//'\'//TRIM(rchfile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (1, *)
                DO WHILE (.NOT. EOF(1))
                    READ (1, *)  yr, Cell, cnlr(:)
                    canalrch(cell, :) = canalrch(cell, :) + cnlr(:)
                END DO
                CLOSE (1)
            END DO
            CLOSE (2)
        END IF
        
        miscrch = 0.
        IF (mscrch .EQ. 1) THEN
            OPEN (1, FILE = TRIM(INDIR)//'Pumping\pumprch.txt', STATUS = 'OLD')
            DO WHILE (.NOT. EOF(1))
                READ (1, *) rchfile
                
                OPEN (2, FILE = TRIM(OUTDIR)//'Miscrch\'//TRIM(rchfile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (2, *)
                DO WHILE (.NOT. EOF(2))
                    READ(2, *) Cell, yr, mrch(:)
                    miscrch(Cell, :) = miscrch(Cell, :) + mrch(:)
                END DO
                CLOSE(2)
            END DO
            CLOSE(1)
        END IF
        
        OPEN (20, FILE = TRIM(OUTDIR)//'RCH\CNEB'//TRIM(YRT)//'.RCH', STATUS = 'UNKNOWN')
        OPEN (30, FILE = TRIM(OUTDIR)//'IrrPrec\IrrPrec'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        OPEN (40, FILE = TRIM(OUTDIR)//'Canal\Canal'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        OPEN (50, FILE = TRIM(OUTDIR)//'SF\SF'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        OPEN (60, FILE = TRIM(OUTDIR)//'ROBal\ROBal'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        OPEN (70, FILE = TRIM(OUTDIR)//'AnnRCH\AnnRCH'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        
        WRITE (30, 131)
        WRITE (40, 141)
        WRITE (50, 151)
        WRITE (60, 161)
        WRITE (70, 171)
        
        DO Mon = startmon, endmon
            CellRch = 0.
            WRITE (6, *) mon
            CALL popcellrch
            
            WRITE(20, 122)
            WRITE(20, 123)
            
            zz = 1
            DO irow = 1, nrows
                IF(zz .GT. ncell) GOTO 16
                
                icol = 1
                DO Linenum = 1, fcol
                    DO k = 0, 8
                        IF (zz .GT. ncell) THEN
                            WRITE (20, *)
                            GOTO 16
                        END IF
                        
                        IF (Cellrch(irow, icol + k) .EQ. 0.) THEN
                            WRITE (20, 204) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 10.) THEN
                            WRITE (20, 215) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 100.) THEN
                            WRITE (20, 216) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 1000.) THEN
                            WRITE (20, 217) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 10000.) THEN
                            WRITE (20, 218) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE
                            WRITE (20, 219) Cellrch(irow, icol + k)
                            zz = zz + 1
                        END IF
                    END DO
                    
                    IF(zz .GT. ncell) THEN
                        WRITE (20, *)
                        GOTO 16
                    END IF
                    
                    k = 9
                    IF (Cellrch(irow, icol + k) .EQ. 0.) THEN
                        WRITE (20, 206) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 10.) THEN
                        WRITE (20, 225) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 100.) THEN
                        WRITE (20, 226) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 1000.) THEN
                        WRITE (20, 227) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 10000.) THEN
                        WRITE (20, 228) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 229) Cellrch(irow, icol + k)
                        zz = zz + 1
                    END IF
                    icol = icol + 10
                END DO
                
                DO k = 1, pcol - 1
                    IF (zz .GT. ncell) THEN
                        WRITE(20, *)
                        GOTO 16
                    END IF
                    
                    IF (Cellrch(irow, (fcol * 10) + k) .EQ. 0.) THEN
                        WRITE (20, 204) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 10.) THEN
                        WRITE (20, 215) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 100.) THEN
                        WRITE (20, 216) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 1000.) THEN
                        WRITE (20, 217) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 10000.) THEN
                        WRITE (20, 218) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 219) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    END IF
                END DO
                
                IF(zz .GT. ncell) THEN
                    IF (pcol .NE. 0) WRITE(20, *)
                    GOTO 16
                END IF
                
                IF (pcol .NE. 0) THEN
                    IF (Cellrch(irow, ncols) .EQ. 0.) THEN
                        WRITE (20, 206) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 10.) THEN
                        WRITE (20, 225) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 100.) THEN
                        WRITE (20, 226) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 1000.) THEN
                        WRITE (20, 227) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 10000.) THEN
                        WRITE (20, 228) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 229) Cellrch(irow, ncols)
                        zz = zz + 1
                    END IF
                END IF
16          END DO
        END DO
        
        CLOSE (20)
        CLOSE (30)
        CLOSE (40)
        CLOSE (50)
        CLOSE (60)
        CLOSE (70)
        
    END DO
    
    CLOSE (10)
    
122 FORMAT ('         1         1')
123 FORMAT ('INTERNAL 1.0 (free) 0')
    
204 FORMAT (F3.1, ' ', \) 
206 FORMAT (F3.1)
   
215 FORMAT (F14.12, ' ', \)
216 FORMAT (F15.12, ' ', \)
217 FORMAT (F16.12, ' ', \)
218 FORMAT (F17.12, ' ', \)
219 FORMAT (F18.12, ' ', \)
   
225 FORMAT (F14.12)
226 FORMAT (F15.12)
227 FORMAT (F16.12)
228 FORMAT (F17.12)
229 FORMAT (F18.12)
   
131 FORMAT ( 'Cell, ROZone, Row, Col, Year, Month, NumberOfDaysInMonth, MonthlyIrrPrecipDP_AF, MonthlyTransDP_AF, MonthlyCanalRchg_AF, ' &
             'Misc Pump Recharge, LPM, MiToGauge, PerToRchg, TotalMonthlyDP_FTperDAY, MonthlyIrrPrecipRchg_FTperDAY') !Header (File 30)
141 FORMAT ( 'Cell, QtrCellRow, QtrCellCol, Year, Month, NumberOfDaysInMonth, QtrCellCanalRchg_AFPerMonth, QtrCellCanalRchg_FTPerDay')
151	FORMAT ( 'Cell, ROZone, Row, Col, Year, Month, NumberOfDaysInMonth, AnnualSF_AF, AnnualCellRO_AF, LPM, MiToGauge, MonthlySF_AFperDAY') !Header (File 50)
161 FORMAT ( 'Cell, Year, Month, FieldRO_AF, TransRchg_AF, SF_AF, ET_AF')
171 FORMAT ( 'Cell, Year, Month, Total_Recharge, Direct_RCH, EOF_RCH, Canal_RCH, Misc_Pump_RCH, Stream_Flow, RO_Transfer_ET, Direct_RO')
100 FORMAT ( 'Cell, Year, Rch')
 
    

END PROGRAM Make_RCH
!*****************************************************************************
SUBROUTINE popcellrch

    USE PARAM
    IMPLICIT NONE
    
    DOUBLE PRECISION TotRch
    
    INTEGER     row, col
    
    REAL        LF
    REAL        SF
    REAL        RO2DP
    REAL        RO2ET
    
    DO Cell = 1, ncell
        col = MOD(Cell, ncols)
        IF (col .EQ. 0) col = ncols
        row = (Cell - col) / ncols + 1
        
        IF (MiToGauge(cell) .EQ. 0.) THEN
            LF = 0.5
        ELSE
            LF = MIN(1 - EXP(-LPM(ROZone(cell)) * MiToGauge(Cell)), 1.0)
        END IF
        
        IF (mon .EQ. 1) THEN
            
            SF = SUM(IrrPrecRO(cell, 1 : 12)) * (1 - LF)
            RO2DP = SUM(IrrPrecRO(cell, 1 : 12)) * LF * PerToRchg(coefzone(cell))
            RO2ET = SUM(IrrPrecRO(cell, 1 : 12)) * LF * PerToRchg(coefzone(cell))
            
            TotRch = SUM(IrrPrecRch(cell, 1 : 12)) + RO2DP + SUM(Canalrch(Cell, 1 : 12)) + SUM(Miscrch(cell, 1 : 12))
            
            IF (TotRch .GT. 0.) THEN
                WRITE (70, 173) Cell, iyear, totrch, SUM(IrrPrecRch(cell, 1 : 12)), RO2DP, SUM(Canalrch(cell, 1 : 12)), &
                                    SUM(Miscrch(cell, 1 : 12)), SF, RO2ET, SUM(IrrPrecRO(cell, 1 : 12))
                
                IF (iyear .LT. SwcYr) THEN
                    Cellrch(row, col) = TotRch / csize / DIY
                END IF
                
            END IF
            
            WRITE (10, 101) Cell, iyear, MAX(TotRch, 0.) / csize * 12
            
        END IF
        
        SF = IrrPrecRO(Cell, mon) * (1 - LF)
        RO2DP = IrrPrecRO(Cell, mon) * LF * PerToRchg(coefzone(cell))
        RO2ET = IrrPrecRO(Cell, mon) * LF * (1 -PerToRchg(coefzone(cell)))
        
        TotRch = IrrPrecRch(Cell, mon) + RO2DP + Canalrch(Cell, Mon) + Miscrch(Cell, Mon)
        
        IF (TotRch .GT. 0.) THEN
            WRITE (70, 172) Cell, iyear, mon, TotRch, IrrPrecRch(Cell, mon), RO2DP, Canalrch(cell, mon), Miscrch(cell, mon), &
                                SF, RO2ET, IrrPrecRO(Cell, mon)
            
            IF (iyear .GE. SwcYr) THEN
                Cellrch(row, col) = TotRch / csize / DaysInMonth(mon)
            END IF
            
            WRITE (30, 132) Cell, Rozone(cell), row, col, iyear, mon, DaysInMonth(mon), &
                                IrrPrecRch(cell, mon), RO2DP, Canalrch(cell, mon), miscrch(cell, mon), &
                                LPM(ROZone(cell)), MiToGauge(cell), PerToRchg(Coefzone(cell)), Cellrch(row, col), &
                                Cellrch(row, col) - Canalrch(cell, mon) / csize / DaysInMonth(mon)
            
            IF (Canalrch(cell, mon) .GT. 0.) THEN
                WRITE(40, 142) Cell, row, col, iyear, mon, DaysInMonth(mon), Canalrch(cell, mon), &
                                    Canalrch(cell, mon) / csize / DaysInMonth(mon)
            END IF
            WRITE (50, 152) Cell, Rozone(cell), row, col, iyear, mon, DaysInMonth(mon), SF, &
                                IrrPrecRO(cell, mon), LPM(ROZone(cell)), MiToGauge(cell), SF / DaysInMonth(mon)
        END IF
        
        IF (IrrPrecRO(cell, mon) .GT. 0.) THEN
            WRITE (60, 162) Cell, iyear, mon, IrrPrecRO(Cell, mon), RO2DP, SF, RO2ET
        END IF
        
    END DO

101 FORMAT(I6,',', I5,',', F6.2)
132	FORMAT(I6,',', I4,',', 2(I3,','), I4,',', 2(I2,','), 3(F7.2,','), F4.2,',',F6.2,',',F5.2,',', F14.10,',',F14.10)
142 FORMAT(I6,', ',2(I3,', '),I4,', ',2(I2,', '),F16.10,', ',F14.12)
152 FORMAT(I6,',', I4,',', 2(I3,','), I4,',', 2(I2,','), F4.2,',',F8.2,',',F4.2,',',F6.2,',',F14.10)
162 FORMAT(I6,',',I4,',', I2, ',',3(F8.2,','),F8.2)
172 FORMAT(I6, ', ', I4, ', ', I2, 8(', ', F8.2))  
173 FORMAT(I6, ', ', I4, ',  0', 8(', ', F8.2))  

END SUBROUTINE popcellrch
!*****************************************************************************