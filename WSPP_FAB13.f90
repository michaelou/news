!********************************************************************************
!
!  UNW_WSPP  - Conjunctive Management Water Supply Partitioning Program
!
!  Initial Development:  Mar 2011
!	By:  Marc Groff - TFG
!	Summary:  Based on COHYST CCMM WSPP program, but uses CROPSIM results directly
!
!  Further Development: Sep 2011 - Feb 2012
!   By: Isaac Mortensen - TFG
!   Summary:    WSPP was updated to remove the curve coefficients, Change the structure of the
!               land use files, distribute ET gain, employ the use of monthly output cropsim data,
!               enhance the ease of use and streamline the changes.
!
!   July 2012   IM -TFG
!   Updated the format to run on an annual basis
!
!********************************************************************************
!
!********************************************************************************************
MODULE Param
    INTEGER, PARAMETER :: startyr = 2013
    INTEGER, PARAMETER :: endyr = 2041
    INTEGER, PARAMETER :: ncell = 49725
    INTEGER, PARAMETER :: nzone = 5
    INTEGER, PARAMETER :: rzone = 10
    
    DATA maxRO /0.8/
    DATA minRO /0.2/
    
    DATA INDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\'/
    DATA OUTDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\'/
    DATA LUDIR /'W:\CNEB\CNEB\LandUse\2016Static\'/
    DATA WBPDIR /'W:\CNEB\CNEB\Gridded_CropSim\Run005\Full_Grid\'/
    
    !0 - irrigation included; 1 - dryland only
    DATA DPonly /0/
    
    !0 - all irrigation; 1 - no GWP or COP
    DATA SWonly /1/
    DATA LandUse /'CNEBSTALU'/
    
    CHARACTER*100    INDIR, OUTDIR, LUDIR, WBPDIR
    CHARACTER*4     YRT, CYRT
    CHARACTER*12    Crop(12), LandUse
    
    INTEGER     CropCode(12)
    INTEGER     Cell, CoefZone(ncell), ROZone(ncell), zone
    INTEGER     iyear, yr, CalcYr, pflag, iyr, cyr(startyr : endyr)
    INTEGER     SWDcell, COPCell, CODcell, GWPcell
    INTEGER     CSCell, CSYear, CSCrop
    INTEGER     DPonly, SWonly
    
    INTEGER     i, j, k, m
    
    REAL    MiToGauge, minRO, maxRO
    REAL    DryETAdj(nzone), IrrETadj(nzone), NIRadjFactor(nzone), Fslgw(nzone)
    REAL    DryETtoRO(nzone), Fslsw(nzone), PertoRchg(nzone), AEgadj(nzone), AEsadj(nzone), DPadj(nzone), ROadj(nzone), CMsplit(nzone)
    REAL    SWDann(ncell, 0 : 12), SWDcrp(ncell, 12, 0 : 12)
    REAL    GWPann(ncell, 0 : 12), GWPcrp(ncell, 12, 0 : 12)
    REAL    CODann(ncell, 0 : 12), CODcrp(ncell, 12, 0 : 12)
    REAL    COPann(ncell, 0 : 12), COPcrp(ncell, 12, 0 : 12)
    REAL    DryAc(12), GWAc(12), SWAc(12), COAc(12)

    REAL    CellRO(4, 12, 0 : 12), CellDP(4, 12, 0 : 12), CellAP(4, 12, 0 : 12)
    REAL    CellET, CellSL, CellSW, CellDAP, CellPSL, CellETGain, CellDET, CellETbase
    REAL    CellDP1, CellDP2, CellDP3, CellRO1, CellRO2, CellRO3, CellEWAT, CellNIRmET
    
    REAL    DryDP(12, 0 : 12), DryRO(12, 0 : 12), DryET(12, 0 : 12)
    REAL    IrrDP(12, 0 : 12), IrrRO(12, 0 : 12), IrrET(12, 0 : 12), NIRCS(12, 0 : 12)
    
    REAL    ETMaxdry(0 : 12), ETMAXadjdry(0 : 12), NIRmET(0 : 12)
    REAL    RO1dry(0 : 12), DP1dry(0 : 12), RO2dry(0 : 12), DP2dry(0 : 12)
    REAL    CellETTrans, ETTrans(0 : 12)
    
    REAL    ET(0 : 12), SL(0 : 12), DAP(0 : 12), PSL(0 : 12), ETGain(0 : 12), DET1(0 : 12), DET2(0 : 12)
    REAL    ETbase(0 : 12), DP1Irr(0 : 12), DP2(0 : 12), DP3(0 : 12), RO1Irr(0 : 12), RO2(0 : 12)
    REAL    RO3(0 : 12), EWat(0 : 12)
    
    REAL    TotCellDP(0 : 12), TotCellRO(0 : 12), TotCellAP(0 : 12)
    REAL    AEsw, AEgw, AEs(startyr : endyr), AEg(startyr : endyr)
    
    DATA CROP / 'Corn', 'SugarBeets', 'EdibleBeans', 'Alfalfa', 'WinterWheat', 'Potatoes', &
                'Milo', 'Sunflower', 'SoyBeans', 'SmallGrain', 'Fallow', 'Past'/

END MODULE Param
    
    !**********************************************************************************	
PROGRAM UNW_WSPP

    USE Param
    IMPLICIT NONE
    REAL    dummy (0 : 12)
    
    dummy = 0.
    
    !Populate Cell Location Variables
    WRITE(6,*) 'Reading CellLoc'
    OPEN (20, File = TRIM(INDIR)//'Cellloc.txt', STATUS = 'OLD')
    READ (20,*) 		
    DO WHILE (.NOT. EOF(20))
    	READ (20,*) Cell, CoefZone(Cell), ROZone(Cell), MiToGauge
    END DO  
    CLOSE(20)
    
    !Load Cell Coefficients
    WRITE(6,*) 'Reading CoefFile Info'
    OPEN (11, File = TRIM(INDIR)//'CoefTest.txt', STATUS = 'UNKNOWN')
    READ (11, *)
    DO WHILE (.NOT. EOF(11))
    	READ (11, *, END = 2) m, DryETAdj(m), IrrETadj(m), NIRadjFactor(m), AEgadj(m), Fslgw(m), &
                        DryETtoRO(m), AEsadj(m), Fslsw(m), PertoRchg(m), DPadj(m), ROadj(m), CMsplit(m)
2   END DO 
    CLOSE(11)
    
    !Load Application Efficiencies
    WRITE (6, *) 'Reading Application Efficiencies'
    OPEN (11, FILE = TRIM(INDIR)//'AE.txt', STATUS = 'OLD')
    READ (11, *)
    DO WHILE (.NOT. EOF(11))
        READ (11, *) yr, AEgw, AEsw
        IF (yr .LT. startyr) GOTO 1
        IF (yr .GT. endyr) GOTO 1
        AEg(yr) = AEgw
        AEs(yr) = AEsw
1   END DO
    CLOSE (11)
    
    !Get call years
    WRITE (6, *) 'Get WBP relationship years'
    OPEN (11, FILE = TRIM(INDIR)//'CallYear.csv', STATUS = 'OLD')
    READ (11, *)
    DO WHILE (.NOT. EOF(11))
        READ (11, *) yr, iyr
        IF ((yr .LT. startyr) .OR. (yr .GT. endyr)) GOTO 3
        cyr(yr) = iyr
3   END DO
    CLOSE (11)

    DO iyear = StartYr, EndYr
        
        WRITE (YRT, '(I4)') iyear
        WRITE (cYRT, '(I4)') cyr(iyear)
        WRITE (6, *) iyear, cyr(iyear)
        
        SWDcell = 0
        CODcell = 0
        COPcell = 0
        GWPcell = 0
        
        SWDann = 0.
        GWPann = 0.
        COPann = 0.
        CODann = 0.
        SWDcrp = 0.
        GWPcrp = 0.
        COPcrp = 0.
        CODcrp = 0.
        
        AEgw = AEg(iyear) * AEgadj(Coefzone(cell))
        AEsw = AEs(iyear) * AEgadj(Coefzone(cell))
        
        !OPEN OUTPUT FILES
        OPEN (12, File = TRIM(OUTDIR)//'WSPP_Out\WSPPOut'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        WRITE(12, 120)
        
        OPEN (13, FILE = TRIM(OUTDIR)//'Pump\Pump'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        WRITE(13, 130)

        !Routine opens all Cropsim Input files and reads header row from each file
        CALL OpenCSFiles

        !Open Landuse file which serves as the control file
        OPEN (20, File = TRIM(LUDIR)//TRIM(LandUse)//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (20, *) 

        !Open output file
        OPEN (1, File = TRIM(OUTDIR)//'RAW\RAW_WSPP'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
        WRITE (1, 100)
            
       IF (DPonly .EQ. 0) THEN
            
            !Open Surface Water Deliveries
            OPEN (8, File = TRIM(OUTDIR)//'SWD\SWD'//TRIM(YRT)//'.txt', STATUS = 'OLD')
            READ (8, *)
            DO WHILE (.NOT. EOF(8))
                READ (8, 202, END = 50) SWDcell, yr, SWDann(SWDcell, :), (SWDcrp(SWDcell, i, :), i = 1, 12)
50          END DO
            CLOSE (8)
        
            !OPEN Ground Water Pumping
            OPEN (9, FILE = TRIM(OUTDIR)//'GWP\GWP'//TRIM(YRT)//'.txt', STATUS = 'OLD')
            READ (9, *)
            DO WHILE (.NOT. EOF(9))
                READ (9, 202, END = 51) GWPcell, yr, GWPann(GWPcell, :), (GWPcrp(GWPcell, i, :), i = 1, 12)
51          END DO
            CLOSE (9)
        
            !OPEN Co-mingled SW Deliveries
            OPEN (10, FILE = TRIM(OUTDIR)//'COD\COD'//TRIM(YRT)//'.txt', STATUS = 'OLD')
            READ (10, *)
            DO WHILE (.NOT. EOF(10))
                READ (10, 202, END = 52) CODcell, yr, CODann(CODcell, :), (CODcrp(CODcell, i, :), i = 1, 12)
52          END DO
            CLOSE (10)
        
            !OPEN Co-mingled GW Pumping
            OPEN (11, FILE = TRIM(OUTDIR)//'COP\COP'//TRIM(YRT)//'.txt', STATUS = 'OLD')
            READ (11, *)
            DO WHILE (.NOT. EOF(11))
                READ (11, 202, END = 53) COPcell, yr, COPann(COPcell, :), (COPcrp(COPcell, i, :), i = 1, 12)
53          END DO
            CLOSE (11)
        ELSE
            SWDCell = 0.
            GWPcell = 0.
            COPcell = 0.
            CODcell = 0.
        END IF
        
        IF (SWonly .EQ. 1) THEN
            COPann = 0.
            COPcell = 0.
            COPcrp = 0.
            GWPann = 0.
            GWPcell = 0.
            GWPcrp = 0.
        END IF
            
        DO WHILE (.NOT. EOF(20))

            Read (20, 299) Cell, CalcYr, DryAc, GWAc, SWAc, COAc
            
            IF (SWonly .EQ. 1) THEN
                DryAc = DryAc + GWAc
                GWAc = 0.
            END IF
    
            IF (cell .EQ. 5236) THEN
                CONTINUE
            END IF
            
            IF (CalcYr .LT. StartYr) GOTO 2000
            IF (CalcYr .GT. EndYr)	GOTO 2000
            
		    WRITE(6,*) 'SEARCHING CELL, YEAR', Cell, CalcYr

1000	    Call ReadCSFiles
		    IF (.NOT. EOF(301)) THEN
			    IF (CSCell .NE. Cell) GOTO 1000
    			IF ((CSYear .NE. CalcYr) .AND. (CSYear .NE. cyr(iyear))) GOTO 1000
	    	ELSE
		    	WRITE(6,*) 'CROPSIM file order error'
			    STOP
            END IF
        
	    	Call InitValues
        
		    zone = CoefZone(Cell)

            DO j = 1, 12
                
                !Dryland
                Pflag = 1
                IF (DryAc(j) .GT. 0.) THEN
                    Call CalcDryCrop(j) 
                    
                    DO k = 0, 12
                        CellRO(1, j, k) = (RO1dry(k) + RO2dry(k)) / 12 * DryAc(j)
                        CellDP(1, j, k) = (DP1dry(k) + DP2dry(k)) / 12 * DryAc(j)
                        CellAP(1, j, k) = 0.
                        
                        CellET = ETMAXadjdry(k) / 12 * DryAc(j)
                        CellSL = 0.
                        CellSW = 0.
                        
                        CellDAP = 0.
                        CellPSL = 0.
                        CellETGain = 0.
                        CellDET = (ETMaxdry(k) - ETMAXadjdry(k)) / 12 * DryAc(j)
                        CellETbase = ETMAXadjdry(k) / 12 * DryAc(j)
                        CellDP1 = DP1dry(k) / 12 * DryAc(j)
                        CellDP2 = DP2dry(k) / 12 * DryAc(j)
                        CellRO1 = RO1dry(k) / 12 * DryAc(j)
                        CellRO2 = RO2dry(k) / 12 * DryAc(j)
                        CellEwat = 0.
                        CellETTrans = ETTrans(k) / 12 * DryAc(j)
                        CellNIRmET = NIRmET(k) / 12 * DryAc(j)
                        
                        WRITE(1, 101) Cell, iyear, k, j, Pflag, CoefZone(Cell), ROZone(Cell), DryAc(j), CellAP(Pflag, j, k), &
                                        CellSW, CellET, CellRO(Pflag, j, k), CellDP(Pflag, j, k), CellSL, CellPSL, CellDAP, CellETGain, &
                                        CellDET, CellETbase, CellDP1, CellDP2, CellRO1, CellRO2, CellEWat, CellETTrans, CellNIRmET
                        
                    END DO
                END IF
                
                !Groundwater Pumping
                Pflag = 2
                IF (GWAc(j) .GT. 0.) THEN
                    IF (DryAc(j) .EQ. 0.) THEN
                        Call CalcDryCrop(j)
                    END IF
                    
                    Call CalcIrrCrop(j, dummy, GWPcrp(Cell, j, :) * 12)
                    
                    DO k = 0, 12
                        CellRO(2, j, k) = (RO2(k) + RO1irr(k)) / 12 * GWAc(j)
                        CellDP(2, j, k) = (DP2(k) + DP1irr(k)) / 12 * GWAc(j)
                        CellAP(2, j, k) = GWPcrp(Cell, j, k) * GWAc(j)
                        
                        CellET = ET(k) / 12 * GWAc(j)
                        CellSL = SL(k) / 12 * GWAc(j)
                        CellSW = 0.
                        
                        CellDAP = DAP(k) / 12 * GWAc(j)
                        CellPSL = PSL(k) / 12 * GWAc(j)
                        CellETGain = ETGain(k) / 12 * GWAc(j)
                        CellDET = (DET1(k) + DET2(k)) / 12 * GWAc(j)
                        CellETbase = ETBase(k) / 12 * GWAc(j)
                        CellDP1 = DP1irr(k) / 12 * GWAc(j)
                        CellDP2 = DP2(k) / 12 * GWAc(j)
                        CellRO1 = RO1irr(k) / 12 * GWAc(j)
                        CellRO2 = RO2(k) / 12 * GWAc(j)
                        CellEWat = EWat(k) / 12 * GWAc(j)
                        CellETTrans = ETTrans(k) / 12 * GWAc(j)
                        CellNIRmET = NIRmET(k) / 12 * GWAc(j)
                        
                        WRITE(1, 101) Cell, iyear, k, j, Pflag, CoefZone(Cell), ROZone(Cell), GWAc(j), CellAP(Pflag, j, k), &
                                        CellSW, CellET, CellRO(Pflag, j, k), CellDP(Pflag, j, k), CellSL, CellPSL, CellDAP, CellETGain, &
                                        CellDET, CellETbase, CellDP1, CellDP2, CellRO1, CellRO2, CellEWat, CellETTrans, CellNIRmET
                    END DO
                END IF
                
                !Surface Water Deliveries
                Pflag = 3
                IF(SWAc(j) .GT. 0.) THEN
                    IF (DryAc(j) .EQ. 0.) THEN
                        CALL CalcDryCrop(j)
                    END IF
                    
                    
                    Call CalcIrrCrop(j, SWDcrp(Cell, j, :) * 12, dummy)
                    
                    DO k = 0, 12
                        CellRO(3, j, k) = (RO2(k) + RO1irr(k)) / 12 * SWAc(j)
                        CellDP(3, j, k) = (DP2(k) + DP1irr(k)) / 12 * SWAc(j)
                        CellAP(3, j, k) = 0.
                        
                        CellET = ET(k) / 12 * SWAc(j)
                        CellSL = SL(k) / 12 * SWAc(j)
                        CellSW = SWDcrp(Cell, j, k) * SWAc(j)
                        
                        CellDAP = DAP(k) / 12 * SWAc(j) 
                        CellPSL = PSL(k) / 12 * SWAc(j)
                        CellETGain = ETGain(k) / 12 * SWAc(j)
                        CellDET = (DET2(k) + DET2(k)) / 12 * SWAc(j)
                        CellETbase = ETbase(k) / 12 * SWAc(j)
                        CellDP1 = DP1irr(k) / 12 * SWAc(j)
                        CellDP2 = DP2(k) / 12 * SWAc(j)
                        CellRO1 = RO1irr(k) / 12 * SWAc(j)
                        CellRO2 = RO2(k) / 12 * SWAc(j)
                        CellEWat = EWat(k) / 12 * SWAc(j)
                        CellETTrans = ETTrans(k) / 12 * SWAc(j)
                        CellNIRmET = NIRmET(k) / 12 * SWAc(j)
                        
                        WRITE(1, 101) Cell, iyear, k, j, Pflag, CoefZone(Cell), ROZone(Cell), SWAc(j), CellAP(Pflag, j, k), &
                                        CellSW, CellET, CellRO(Pflag, j, k), CellDP(Pflag, j, k), CellSL, CellPSL, CellDAP, CellETGain, &
                                        CellDET, CellETbase, CellDP1, CellDP2, CellRO1, CellRO2, CellEWat, CellETTrans, CellNIRmET
                    END DO
                END IF
            
                !Co-mingled Lands
                Pflag = 4
                IF (COAc(j) .GT. 0.) THEN
                    IF (DryAc(j) .EQ. 0.) THEN
                        Call CalcDryCrop(j)
                    END IF
                    
                    Call CalcIrrCrop(j, CODcrp(Cell, j, :) * 12, COPcrp(Cell, j, :) * 12)
                    
                    DO k = 0, 12
                        CellRO(4, j, k) = (RO2(k) + RO1irr(k)) / 12 * COAc(j)
                        CellDP(4, j, k) = (DP2(k) + DP1irr(k)) / 12 * COAc(j)
                        CellAP(4, j, k) = COPcrp(Cell,j, k) * COAc(j)
                        
                        CellET = ET(k) / 12 * COAc(j)
                        CellSL = SL(k) / 12 * COAc(j)
                        CellSW = CODcrp(Cell, j, k) * COAc(j)
                        
                        CellDAP = DAP(k) / 12 * COAc(j)
                        CellPSL = PSL(k) / 12 * COAc(j)
                        CellETGain = ETGain(k) / 12 * COAc(j)
                        CellDET = (DET1(k) + DET2(k)) / 12 * COAc(j)
                        CellETbase = ETbase(k) / 12 * COAc(j)
                        CellDP1 = DP1irr(k) / 12 * COAc(j)
                        CellDP2 = DP2(k) / 12 * COAc(j)
                        CellRO1 = RO1irr(k) / 12 * COAc(j)
                        CellRO2 = RO2(k) / 12 * COAc(j)
                        CellEWat = EWat(k) / 12 * COAc(j)
                        CellETTrans = ETTrans(k) / 12 * COAc(j)
                        CellNIRmET = NIRmET(k) / 12 * COAc(j)
                        
                        WRITE(1, 101) Cell, iyear, k, j, Pflag, CoefZone(Cell), ROZone(Cell), COAc(j), CellAP(Pflag, j, k), &
                                        CellSW, CellET, CellRO(Pflag, j, k), CellDP(Pflag, j, k), CellSL, CellPSL, CellDAP, CellETGain, &
                                        CellDET, CellETbase, CellDP1, CellDP2, CellRO1, CellRO2, CellEWat, CellETTrans, CellNIRmET
                    END DO
                END IF
            END DO
        
            DO m = 1, 4
                DO j = 1, 12
                    DO k = 1, 12
                        
                        TotCellDP(k) = TotCellDP(k) + CellDP(m, j, k)
                        TotCellRO(k) = TotCellRO(k) + CellRO(m, j, k)
                        TotCellAP(k) = TotCellAP(k) + CellAP(m, j, k)
                    END DO
                END DO
            END DO
            
            TotCellDP(0) = SUM(TotCellDP(1 : 12))
            TotCellRO(0) = SUM(TotCellRO(1 : 12))
            TotCellAP(0) = SUM(TotCellAP(1 : 12))
            
            WRITE (12, 121) Cell, iyear, TotCellDP(:), TotCellRO(:)
            WRITE (13, 131) Cell, iyear, TotCellAP(:)
2000        CONTINUE            
        END DO
        
        CLOSE(1)
        CLOSE(8)
        CLOSE(9)
        CLOSE(10)
        CLOSE(11)
        CLOSE(12)
        CLOSE(13)
        CLOSE(20)
        CALL CLOSECSfiles
    
    END DO

100 FORMAT('Cell, Year, Month, Crop, Irr_Type, Coeff_Zone, RO_Zone, Acres, GW_Pumping, SW_Delivery, Evapotranspiration, Runoff, Deep_Percolation, ' &
            'Surface_Loss, PSL_Irr, Delta_Irr, ET_Gain, Delta_ET, ET_base, DP1, DP2, RO1, RO2, Excess_Water, ETTrans, CS_NIR_minus_ET')
101 FORMAT(I7, ", ", I5, 2(", ", I3), ", ", I2, 2(", ", I3), ", ", F6.2, 18(", ", F14.8))
                        
120 FORMAT('Cell, Year, Ann_DP_AF, Jan_DP_AF, Feb_DP_AF, Mar_DP_AF, Apr_DP_AF, May_DP_AF, Jun_DP_AF, Jul_DP_AF, Aug_DP_AF, Sep_DP_AF, Oct_DP_AF, Nov_DP_AF, Dec_DP_AF, ' &
           'Ann_RO_AF, Jan_RO_AF, Feb_RO_AF, Mar_RO_AF, Apr_RO_AF, May_RO_AF, Jun_RO_AF, Jul_RO_AF, Aug_RO_AF, Sep_RO_AF, Oct_RO_AF, Nov_RO_AF, Dec_RO_AF')
121 FORMAT(I7, ',', I5, 26(',', F9.2))
    
130 FORMAT('Cell, Year, Ann_AP_AF, Jan_AP_AF, Feb_AP_AF, Mar_AP_AF, Apr_AP_AF, May_AP_AF, Jun_AP_AF, Jul_AP_AF, Aug_AP_AF, Sep_AP_AF, Oct_AP_AF, Nov_AP_AF, Dec_AP_AF')   
131 FORMAT(I7, ',', I5, 13(',', F9.2))

202 FORMAT(I7, X, I5, 169(',', F14.8))
299 FORMAT(I6, 2X, I5, 48(2X, F6.2))
    
END PROGRAM UNW_WSPP

!**********************************************************************
SUBROUTINE OpenCSFiles

!   There are numerous input files associated with WSPP.  A large majority of these programs
!   are associated the gridded output of the CropSim program.  The following items are 
!   necessary in both the monthly and annual time steps: Deep Percolation, Field Runoff,
!   Evapotranspiration, and Net Irrigation Requirement.  OpenCSFiles, opens each type of 
!   input file for each of the crops that are supported by the program (see list above).
!   Finally, the first line of each file is read, so as to bypass the header line.  No
!   files are present for the irrigated fallow.

	USE Param
    IMPLICIT NONE
    character*100   crystal
    
    INTEGER     nn
        
    DO nn = 1, 12
        
        OPEN (300 + nn, FILE = TRIM(WBPDIR)//'DP\'//TRIM(Crop(nn))//'-DryDP'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
        OPEN (400 + nn, FILE = TRIM(WBPDIR)//'RO\'//TRIM(Crop(nn))//'-DryRO'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
        OPEN (500 + nn, FILE = TRIM(WBPDIR)//'ET\'//TRIM(Crop(nn))//'-DryET'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
                
        READ (300 + nn, *)
        READ (400 + nn, *)
        READ (500 + nn, *)
        
        IF (nn .NE. 11) THEN
            OPEN (320 + nn, FILE = TRIM(WBPDIR)//'DP\'//TRIM(Crop(nn))//'-IrrDP'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
            OPEN (420 + nn, FILE = TRIM(WBPDIR)//'RO\'//TRIM(Crop(nn))//'-IrrRO'//TRIM(cYRT)//'.txt', STATUS = 'OLD')    
            OPEN (520 + nn, FILE = TRIM(WBPDIR)//'ET\'//TRIM(Crop(nn))//'-IrrET'//TRIM(cYRT)//'.txt', STATUS = 'OLD')       
            OPEN (620 + nn, FILE = TRIM(WBPDIR)//'NIR\'//TRIM(Crop(nn))//'-NIR'//TRIM(cYRT)//'.txt', STATUS = 'OLD')
            
            READ (320 + nn, *)
            READ (420 + nn, *)
            READ (520 + nn, *)
            READ (620 + nn, *)
        END IF
        
    END DO

END SUBROUTINE OpenCSFiles

!**********************************************************************
SUBROUTINE ReadCSFiles

!   ReadCSFiles reads in the annual and monthly timestep values for each cell, year combinations
!   from each of the input files

	USE Param
    IMPLICIT NONE

	INTEGER nn
	
	DO nn = 1, 12
	
	    READ(300 + nn, 300) CSCell, CSYear, CSCrop, (DryDP(nn, i), i = 0, 12)  !i=Month , 0=Annual Value	!!Dry Values
		READ(400 + nn, 300) CSCell, CSYear, CSCrop, (DryRO(nn, i), i = 0, 12)  
        READ(500 + nn, 300) CSCell, CSYear, CSCrop, (DryET(nn, i), i = 0, 12)  
        
        IF (nn .NE. 11) THEN    !Not Fallow
            
            READ(320 + nn, 300) CSCell, CSYear, CSCrop, (IrrDP(nn, i), i = 0, 12)   !Irrigated Values
            READ(420 + nn, 300) CSCell, CSYear, CSCrop, (IrrRO(nn, i), i = 0, 12)
            READ(520 + nn, 300) CSCell, CSYear, CSCrop, (IrrET(nn, i), i = 0, 12)
            READ(620 + nn, 300) CSCell, CSYear, CSCrop, (NIRCS(nn, i), i = 0, 12)
            
		END IF
    END DO !n
    
    IF (CSCell .EQ. 1614) THEN
        CONTINUE
    END IF

!300 Format statement based on program Grid_MONfiles_xx-TT.f90, format line 490
300	FORMAT (I6, 2X, I4, 1X, I3, 2X, F7.2, 12(2X, F7.2))

END SUBROUTINE ReadCSFiles
!********************************************************************
SUBROUTINE CLOSECSfiles
    USE Param
    IMPLICIT NONE
    
    INTEGER     nn
    
    DO nn = 1, 12
        CLOSE (300 + nn)
        CLOSE (400 + nn)
        CLOSE (500 + nn)
                
        IF (nn .NE. 11) THEN
            CLOSE (320 + nn)
            CLOSE (420 + nn)
            CLOSE (520 + nn)
            CLOSE (620 + nn)
        END IF
    END DO
    
END SUBROUTINE CloseCSfiles
!********************************************************************
SUBROUTINE InitValues

!   InitCellTotalsAndAcValues resets the cumulating values to zero for each cell year combination
!   so as to avoid unwanted data transfer.  The change in the format of the Landuse file renders 
!   the need to initialize acrage values unnessary.

	USE Param
	IMPLICIT NONE
		
	TotCellAP = 0.
	TotCellRO = 0.
	TotCellDP = 0.
        
	CellAP = 0.
    CellRO = 0.
	CellDP = 0.
   
END SUBROUTINE InitValues

!********************************************************************
SUBROUTINE CalcDryCrop (CropCS)

!   The CalcDryCrop subroutine reduces the idealized dryland cropping conditions
!   simulated in CropSim.  ET amounts are reduced to non idealized conditions by
!   a factor specified by the coefficient zone.  The reduction in ET is then divided
!   proportionally between Runoff and Deep Percolation.  The results are passed back
!   to the main code while the variable values needed to run the calculations are
!   written to an output file.

!   CropCS      This is the CropCode(WSPP INDEX) passed from the main program, or
!               the CropSim Code value
!   n           A counter for monthly time steps.

	USE Param
    IMPLICIT NONE
    
	INTEGER	CropCS, n
    
    ETMAXdry = DryET(CropCS, :)
    ETMAXadjdry = ETMAXdry * DryETadj(zone)
    
    NIRmET = 0. - ETMAXdry
    
    RO1dry = DryRO(CropCS, :) * ROadj(zone)
    RO2dry = (ETMAXdry - ETMAXadjdry) * DryETtoRO(zone)
    
    DP1dry = DryDP(CropCS, :) * DPadj(zone)
    DP2dry = (ETMAXdry - ETMAXadjdry) * (1 - DryETtoRO(zone))
    
    ETTrans = DryDP(CropCS, :) * (1 - DPadj(zone)) + DryRO(CropCS, :) * (1 - ROadj(zone))
	    
    !Sum monthly value to obtain annual value
    ETMAXdry(0) = SUM(ETMAXdry(1 : 12))
    ETMAXadjdry(0) = SUM(ETMAXadjdry(1 : 12))
    RO1dry(0) = SUM(RO1dry(1 : 12))
    DP1dry(0) = SUM(DP1dry(1 : 12))
    RO2dry(0) = SUM(RO2dry(1 : 12))
    DP2dry(0) = SUM(DP2dry(1 : 12))
    ETTrans(0) = SUM(ETTrans(1 : 12))
        
END SUBROUTINE CalcDryCrop

!********************************************************************
SUBROUTINE CalcIrrCrop (xx, AppSW, AppGW)

    USE Param
    IMPLICIT NONE

    INTEGER n, xx
        
    REAL    AppGW(0 : 12)
    REAL    AppSW(0 : 12)
    REAL    AppWat(0 : 12)
    REAL    GIRFactor

    REAL	RODPwt(12)
    REAL    ISETirrMax
    REAL    ISETdryMax
    REAL    ISPSL, ISAPWAT
    REAL    Fsl_gw
    REAL    RO2b(0 : 12), DP2b(0 : 12)
    
    REAL    ETMAX(0 : 12), ETMAXadj(0 : 12)
    REAL    NIR(0 : 12), ETGain1(0 : 12), CU(0 : 12)
    REAL    CIR, CIR1, GIR, GIR1, Beta, Beta1
    REAL    ETBase1(0 : 12), ET1(0 : 12)
    REAL    SWGIR, GWGIR, GWGD
    REAL    AEcomp, TNIR1, TNIR2, TDSW1, VDSW1, VDSW2, A1
    
    SWGIR = 1 / 0.75
    IF (AEgw .LE. 0.75) THEN
        GWGIR = 1 / 0.85
    ELSE IF (AEgw .GE. 0.85) THEN
        GWGIR = 1 / 0.95
    ELSE
        GWGD = 0.85 + (0.95 - 0.85) * ((AEgw - 0.75)/(0.85 - 0.75))
        GWGIR = 1 / GWGD
    END IF
    
    ISETirrMax = 0.
    ISETdryMax = 0.
    ISPSL = 0.
    ISApWat = 0.
    
    IF (appSW(0) .GT. 0.) THEN
        IF (appGW(0) .GT. 0.) THEN
            GIRFactor = GWGIR * AppGW(0) / (AppGW(0) + AppSW(0)) + SWGIR * (1 - AppGW(0) / (AppGW(0) + AppSW(0)))
        ELSE
            GIRFactor = SWGIR
        END IF
    ELSE
        GIRFactor = GWGIR
    END IF
    
    RO1irr = IrrRO(xx, :) * ROadj(zone)
    DP1irr = IRRDP(xx, :) * DPadj(zone)
    ETTrans = IrrRO(xx, :) * (1 - ROadj(zone)) + IrrDP(xx, :) * (1 - DPadj(zone))
    ETMAX = IrrET(xx, :)
    NIR = NIRCS(xx, :)
    AppWat = AppGW + AppSW
    
    RO1irr(0) = SUM(RO1irr(1 : 12))
    DP1irr(0) = SUM(DP1irr(1 : 12))
    ETMAX(0) = SUM(ETMAX(1 : 12))
    NIR(0) = SUM(NIR(1 : 12))
    ETTrans(0) = SUM(ETTrans(1 : 12))
    
    !Computing the CropSim Water Balance minus the precipitation
    
    
    AEcomp = (AEgw * (AppGW(0) / (AppGW(0) + AppSW(0))) + AEsw * (AppSW(0) / (AppGW(0) + AppSW(0))))
    !WHat are these for?
    Tnir1 = NIR(0) / AEcomp
    !What are these for?
    Tnir2 = AppWat(0) / NIRadjFactor(zone)
    TDSW1 = NIR(0) - ETMAX(0) - RO1irr(0) - DP1irr(0) - ETTrans(0)
    NIRmET = NIR - ETMAX
    
    DO n = 1, 12
        IF (RO1irr(n) + DP1irr(n) .LE. 0.) THEN
            RODPwt(n) = DryETtoRO(zone)
        ELSE
            RODPwt(n) = MIN(MAX(RO1irr(n) / (RO1irr(n) + DP1irr(n)), minRO), maxRO)
        END IF
        
        IF ((NIR(n) .LE. 0.) .AND. (AppWat(n) .LE. 0.)) THEN
            
            ETGain1(n) = 0.
            DAP(n) = 0.
            RO2(n) = 0.
            DP2(n) = 0.
            
        ELSE IF ((NIR(n) .LE. 0.) .AND. (AppWat(n) .GT. 0.)) THEN

            ETGain1(n) = 0.
            DAP(n) = 0.
            RO3(n) = (PSL(n) - ETGain(n)) * RODPwt(n)
            DP3(n) = PSL(n) - ETGain(n) - RO2(n)
                        
        ELSE
            DAP(n) = NIR(n) / AEcomp - AppWat(n)
           
            IF (ETMAX(n) .GT. ETMAXdry(n)) THEN
                ISETirrMax = ISETirrMax + ETMAX(n)
                ISETdryMax = ISETdryMax + ETMAXdry(n)
                ISApWat = ISApWat + AppWat(n)
            ELSE
                ETGain1(n) = 0.
            END IF
        END IF
    END DO

    CIR1 = MAX(ISETirrMax - ISETdryMax, 0.0001)
    GIR1 = NIR(0) / AEcomp
    Beta1 = CIR1 / GIR1
    
    IF (ISApWat .LT. GIR1) THEN
        ETGain1(0) = MAX(MIN(CIR1 * (1 - (1 - ISApWat / GIR1)**(1 / Beta1)), ISApWat), 0.)
    ELSE
        ETGain1(0) = MAX(ISETIrrMax - ISETdryMax, 0.)
    END IF
    
    IF (ISETirrMax - ISETdryMax .GT. 0.001) THEN        
        CALL DistETGain(RODPwt, ETGain1(0), ETMax, ETMaxdry, AppWat, ETGain1(1 : 12), RO2, DP2, ETbase1, ET1)
    ELSE
        ETGain1 = 0.
        ETbase1 = ETMax
        ET1 = ETMAX
    END IF
    
    ETGain1(0) = SUM(ETGain1(1 : 12))
    ETbase1(0) = SUM(ETbase1(1 : 12))
    DET1 = ETMAX - (ETGain1 + ETbase1)
    
    DET1(0) = SUM(DET1(1 : 12))
    ET1(0) = SUM(ET1(1 : 12))
    !We can delete a1
    a1 = DET1(0) + ET1(0)

    ET = ET1 * IrrETadj(zone)
    DET2 = ET1 * (1 - IrrETadj(zone))
    
    SL = Fslsw(zone) * AppSW + Fslgw(zone) * AppGW
    PSL = AppSW * (1 - Fslsw(zone)) + AppGW * (1 - Fslgw(zone))
    
    Ewat = AppWat - SL - ET - NIRmET
    
    
    DO n = 1, 12
        
        IF (ETGain1(n) .GT. 0) THEN
            ETGain(n) = ETGain1(n) - ETGain1(n)/ET1(n) * DET2(n)
            ETbase(n) = ETBase1(n) - (1 - ETGain1(n) / ET1(n)) * DET2(n)
        ELSE
            ETbase(n) = ETbase1(n) - DET2(n)
            ETGain(n) = 0.
        END IF

        RO2(n) = EWat(n) * RODPwt(n)
        DP2(n) = EWat(n) * (1 - RODPwt(n))
        
    END DO
    
    RO1irr(0) = SUM(RO1irr(1 : 12))
    DP1irr(0) = SUM(DP1irr(1 : 12))
    RO2(0) = SUM(RO2(1 : 12))
    DP2(0) = SUM(DP2(1 : 12))
    ETGain(0) = SUM(ETGain(1 : 12))
    ETbase(0) = SUM(ETbase(1 : 12))
    DET1(0) = SUM(DET1(1 : 12))
    DET2(0) = SUM(DET2(1 : 12))
    ET(0) = SUM(ET(1 : 12))
    ETTrans(0) = SUM(ETTrans(1 : 12))
    EWAT(0) = SUM(EWAT(1 : 12))
    
    VDSW2 = AppWat(0) - ET(0) - RO2(0) - DP2(0) - SL(0)
    VDSW1 = AppWat(0) - ET(0) - RO2(0) - DP2(0) - SL(0) - RO1irr(0) - DP1irr(0) - ETTrans(0)
    IF (ABS(VDSW2 - NIRmET(0)) .GT. 0.0001) THEN
        CONTINUE
    END IF
    IF (ABS(VDSW1 - TDSW1) .GT. 0.0001) THEN
        CONTINUE
    END IF
    
END SUBROUTINE CalcIrrCrop
!********************************************************************
SUBROUTINE DistETGain(wtRODP, GainET, MaxETirr, MaxETdry, IrrPSL, GainETmon, RO2a, DP2a, ETb, ET2)
    USE Param
    IMPLICIT NONE

    INTEGER flag(12)
    INTEGER n
    
    REAL    ET2(0 : 12), ETb(0 : 12)
    REAL    GainET, GainETmon(12)
    REAL    IrrPSL(0 : 12)
    REAL    MaxETirr(0 : 12), MaxETdry(0 : 12)
    REAL	wtRODP(12)
    REAL    RO2a(0 : 12), DP2a(0 : 12)
    
    REAL    ETProp(12)
    REAL    ETG
    REAL    irrETsum
    REAL    dryETsum
    REAL    irrPLSsum
    
    flag = 1
    ETG = GainET
        
    DO n = 1, 12
        IF (IrrPSL(n) .LE. 0.) flag(n) = 0
        IF (MaxETirr(n) .LE. MaxETdry(n)) flag(n) = 0
    END DO
    
15  irrETsum = 0.
    dryETsum = 0.
    
    DO n = 1, 12
        IF (flag(n) .EQ. 1) THEN
            irrETsum = irrETsum + MaxETirr(n)
            dryETsum = dryETsum + MaxETdry(n)
        END IF
    END DO
    
    DO n = 1, 12
        IF (flag(n) .EQ. 1) THEN
            ETprop(n) = (MaxETirr(n) - MaxETdry(n)) / (irrETsum - dryETsum)
        ELSE
            ETprop(n) = 0.
        END IF
    END DO
    
    DO n = 1, 12
        IF (flag(n) .EQ. 1) THEN
                GainETmon(n) = ETG * ETprop(n)
            IF (GainETmon(n) .GT. IrrPSL(n)) THEN
                GainETmon(n) = IrrPSL(n)
                flag(n) = 0
                ETG = ETG - IrrPSL(n)
                GOTO 15
            END IF
        END IF
    END DO

    ETG = sum(GainETmon)
    
    IF (ETG .LT. GainET) THEN
        flag = 1
        ETG = GainET - ETG
        IF (ETG .GT. 0.00001) THEN
            DO n = 1, 12
                IF (IrrPSL(n) .LE. 0.) flag(n) = 0
                IF (MaxETirr(n) .GT. MaxETdry(n)) flag(n) = 0
            END DO
        
16          irrPLSsum = 0.
        
            DO n = 1, 12
                If (flag(n) .EQ. 1) THEN
                    irrPLSsum = irrPLSsum + IrrPSL(n)
                END IF
            END DO
        
            DO n = 1, 12
                IF (flag(n) .EQ. 1) THEN
                    GainETmon(n) = ETG * IrrPSL(n) / irrPLSsum
                    IF (GainETmon(n) .GT. IrrPSL(n)) THEN
                        GainETmon(n) = IrrPSL(n)
                        flag(n) = 0
                        ETG = ETG - IrrPSL(n)
                        GOTO 16
                    END IF
                END IF
            END DO
        END IF
    END IF
    
    ETG = sum(GainETmon)
    
    IF (ETG .LT. GainET) THEN
        flag = 1
        ETG = GainET - ETG
        IF (ETG .GT. 0.00001) THEN
            DO n = 1, 12
                IF (IrrPSL(n) .GT. 0) flag(n) = 0
                IF (MaxETdry(n) .LE. MaxETirr(n)) flag(n) = 0
            END DO
        
            irrETsum = 0.
        
            DO n = 1, 12
                IF (flag(n) .EQ. 1) THEN
                    irrETsum = IrrETsum + MaxETirr(n)
                END IF
            END DO
        
            DO n = 1, 12
                IF (flag(n) .EQ. 1) THEN
                    GainETmon(n) = ETG * MaxETirr(n) / irrETsum
                    RO2a(n) = MAX((IrrPSL(n) - GainETmon(n) * IrrETAdj(zone)) * wtRODP(n), 0.)
                    DP2a(n) = RO2a(n) * (1 - wtRODP(n)) / wtRODP(n)
                END IF
            END DO
        END IF
    END IF
    
    
    DO n = 1, 12
        IF (ISNAN(GainETmon(n))) THEN
            CONTINUE
        END IF
        IF (IrrPSL(n) .LE. 0) THEN
            ET2(n) = GainETmon(n) + MaxETIrr(n)
            ETb(n) = MaxETIrr(n)
        ELSE
            IF(MaxETIrr(n) .GT. MaxETdry(n)) THEN
                ET2(n) = GainETmon(n) + MaxETdry(n)
                ETb(n) = MaxETdry(n)
            ELSE
                ET2(n) = GainETmon(n) + MaxETIrr(n)
                ETb(n) = MaxETIrr(n)
            END IF
        END IF        
    END DO
    
END SUBROUTINE DistETGain
!********************************************************************