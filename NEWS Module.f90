MODULE PARAM

    INTEGER, PARAMETER :: startyr = 2013
    INTEGER, PARAMETER :: endyr = 2041
    INTEGER, PARAMETER :: nzones = 5
    INTEGER, PARAMETER :: rzones = 37
    INTEGER, PARAMETER :: ncell = 49725
    
    DATA LUDIR /'W:\CNEB\CNEB\LandUse\2016Static\'/
    DATA INDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Inputs\'/
    DATA WBPDIR /'W:\CNEB\CNEB\Gridded_CropSim\Run005\Full_Grid\'/
    DATA OUTDIR /'W:\CNEB\CNEB\CNEB\FAB2016_STA001np\Results\'/
    
    DATA LandUse /'CNEBSTALU'/
    
    CHARACTER*100   LUDIR, OUTDIR, INDIR, WBPDIR
    CHARACTER*4     YRT, cYRT
    CHARACTER*12    Crop(12), LandUse
    
    INTEGER     iyear, yr, iyr, cyr(startyr : endYr)
    INTEGER     cell, coefzone(ncell), ROZone, MiToGauge
    INTEGER     i, j, m
    
    REAL    DryETadj(nzones), IrrETadj(nzones), NIRadjFactor(nzones), AEgadj(nzones), Fslgw(nzones)
    REAL    DryETtoRO(nzones), AEsadj(nzones), Fslsw(nzones), PertoRchg(nzones), DPadj(nzones), ROadj(nzones), CMsplit(nzones)
    REAL    DryAc(12), GWAc(12), SWAc(12), COAc(12)
    REAL    NIRCS(ncell, 12, 0 : 12), NIR(0 : 12)
    REAL    SWD(12, 0 : 12), COD(12, 0 : 12), GWP(12, 0 : 12), COP(12, 0 : 12)
    REAL    SWDCell (0 : 12), GWPCell(0 : 12), CODCell(0 : 12), COPCell(0 : 12)
    REAL    AEsw, AEgw, AEs(startyr : endyr), AEg(startyr : endyr)

    DATA CROP / 'Corn', 'SugarBeets', 'EdibleBeans', 'Alfalfa', 'WinterWheat', 'Potatoes', &
                'Milo', 'Sunflower', 'SoyBeans', 'SmallGrain', 'Fallow', 'Past'/

END MODULE PARAM