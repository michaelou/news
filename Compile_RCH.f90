!  Compile_RCH.f90 
!
!  FUNCTIONS:
!  Compile_RCH - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Compile_RCH
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

PROGRAM Compile_RCH
    IMPLICIT NONE
    
    INTEGER, PARAMETER :: endyr = 2038
    INTEGER, PARAMETER :: startyr = 2013
    
    DATA OUTDIR /'A:\DNR\CNEB\CNEB\EXT002np\Results\'/
    
    CHARACTER*50    OUTDIR
    CHARACTER*4     YRT
    CHARACTER*1     Line(175)
    
    INTEGER     iyear, icase
!    INTEGER     flag40
    INTEGER     SwcYr(2)
    
    DATA SwcYr/1986, 2000/
    
!    flag40 = 1
    
!    OPEN (1, FILE = TRIM(OUTDIR)//'CNEB40.RCH', STATUS = 'UNKNOWN')
!    WRITE (1, 100)
!    WRITE (1, 101)
!    WRITE (1, 102)
    
!    OPEN (2, FILE = TRIM(OUTDIR)//'CNEB86.RCH', STATUS = 'UNKNOWN')
!    WRITE (2, 100)
!    WRITE (2, 101)
!    WRITE (2, 102)
    
    OPEN (3, FILE = TRIM(OUTDIR)//'CNEB2012np.RCH', STATUS = 'UNKNOWN')
    WRITE (3, 100)
    WRITE (3, 101)
    WRITE (3, 102)
    
    DO iyear = startyr, endyr
        WRITE(6, *) iyear
        WRITE(YRT, '(I4)') iyear
        
1       IF (iyear .LT. SwcYr(1)) THEN
            icase = 1
        ELSE IF ((iyear .GE. SwcYr(1)) .AND. (iyear .LT. SwcYr(2))) THEN
            icase = 2
        ELSE 
            icase = 3
        END IF
        icase = 3
        
        OPEN (10, FILE = TRIM(OUTDIR)//'RCH\CNEB'//TRIM(YRT)//'.RCH', STATUS = 'OLD')
        
        DO WHILE (.NOT. EOF(10))
            READ (10, '(175A1)') Line(:)
            WRITE (icase, '(175A1)') Line(:)
        END DO
        CLOSE (10)
!        IF (Flag40 .EQ. 1) THEN
!            Flag40 = 0
!            GOTO 1
!        END IF
    END DO
        
100 FORMAT ('#MODFLOW2000 Recharge Package')	!Header for RCH file (File 20)
101 FORMAT ('PARAMETER  0')
102 FORMAT ('         3        40') !Header for RCH file (File 20)

END PROGRAM Compile_RCH

